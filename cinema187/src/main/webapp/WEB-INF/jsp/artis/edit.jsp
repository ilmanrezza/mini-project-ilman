<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Edit Artis</h1>
	<form action="#" method="get" id="form-artis-edit">
		<table>
			<tr>
				<td>Kode Artis</td>
				<td><input type="text" id="kodeArtis" name="kodeArtis" value = "${artisModel.kodeArtis}"/></td>
			</tr>
			<tr>
				<td>Nama Artis</td>
				<td><input type="text" id="namaArtis" name="namaArtis" value = "${artisModel.namaArtis}"/></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td><input type="text" id="jenisKelamin" name="jenisKelamin" value = "${artisModel.jenisKelamin}"/></td>
			</tr>
			<tr>
				<td>Bayaran</td>
				<td><input type="text" id="bayaran" name="bayaran" value = "${artisModel.bayaran}"/></td>
			</tr>
			<tr>
				<td>Award</td>
				<td><input type="text" id="award" name="award" value = "${artisModel.award}"/></td>
			</tr>
			<tr>
				<td>Negara</td>
				<td><input type="text" id="negara" name="negara" value = "${artisModel.negara}"/></td>
			</tr>
			
		</table>
		<button type="submit" onclick="validasi();">Simpan</button> 
	</form>
</div>

<script type = "text/javascript">
	function validasi() {
		var kodeArtis = document.getElementById("kodeArtis");
		if (kodeArtis.value == "") {
			kodeArtis.style.borderColor = "red";
		} else {
			kodeArtis.style.borderColor = "gray";
		}
		
		var namaArtis = document.getElementById("namaArtis");
		if (namaArtis.value == "") {
			namaArtis.style.borderColor = "red";
		} else {
			namaArtis.style.borderColor = "gray";
		}
		
		var jenisKelamin = document.getElementById("jenisKelamin");
		if (jenisKelamin.value == "") {
			jenisKelamin.style.borderColor = "red";
		} else {
			jenisKelamin.style.borderColor = "gray";
		}
		
		var bayaran = document.getElementById("bayaran");
		if (bayaran.value == "") {
			bayaran.style.borderColor = "red";
		} else {
			bayaran.style.borderColor = "gray";
		}
		
		var award = document.getElementById("award");
		if (award.value == "") {
			award.style.borderColor = "red";
		} else {
			award.style.borderColor = "gray";
		}

		
		var negara = document.getElementById("negara");
		if (negara.value == "") {
			negara.style.borderColor = "red";
		} else {
			negara.style.borderColor = "gray";
		}

	}
</script>