<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Tambah Divisi</h1>
	<form action="#" method="get" id="form-divisi-add">
		<table>
		<!-- 	<tr>
				<td>ID Divisi</td>
				<td><input type="text" id="idDivisi" name="idDivisi"/></td>
			</tr> -->
			
			<tr>
				<td>Kode Divisi</td>
				<td><input type="text" id="kodeDivisi" name="kodeDivisi"/></td>
			</tr>
			<tr>
				<td>Nama Divisi</td>
				<td><input type="text" id="namaDivisi" name="namaDivisi"/></td>
			</tr>			
		</table>
		<button type="submit" onclick="validasi();">Simpan</button> 
	</form>
</div>

<script type = "text/javascript">
	function validasi() {
		var idDivisi = document.getElementById("idDivisi");
		if (idDivisi.value == "") {
			idDivisi.style.borderColor = "red";
		} else {
			idDivisi.style.borderColor = "gray";
		}
		
		var kodeDivisi = document.getElementById("kodeDivisi");
		if (kodeDivisi.value == "") {
			kodeDivisi.style.borderColor = "red";
		} else {
			kodeDivisi.style.borderColor = "gray";
		}
		
		var namaDivisi = document.getElementById("namaDivisi");
		if (namaDivisi.value == "") {
			namaDivisi.style.borderColor = "red";
		} else {
			namaDivisi.style.borderColor = "gray";
		}

	}
</script>