<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Artis</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah
		Artis</button>

	<div>
		<table class="table" id="tbl-artis">
			<tr>
				<td>No</td>
				<td>Kode Artis</td>
				<td>Nama Artis</td>
				<td>Jenis Kelamin</td>
				<td>Bayaran</td>
				<td>Award</td>
				<td>Negara</td>
				<td>Aksi</td>
			</tr>
			<tbody id="list-artis">

			</tbody>
		</table>
	</div>


</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Artis</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	loadListArtis();

	function loadListArtis() {
		$.ajax({
			url : 'artis/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-artis').html(result);
			}
		});
	}
	
	//document ready itu setelah halamannya dipanggil
	$(document).ready(function() {

		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'artis/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah

		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit', '#form-artis-add', function() {
			$.ajax({
				url : 'artis/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {

					$('#modal-input').modal('hide');
					alert("data telah ditambah!");

				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah
		
		//fungsi ajax untuk popup edit
		$('#list-artis').on('click','#btn-edit',function(){
			var kodeArtis = $(this).val();
			$.ajax({
				url : 'artis/edit.html',
				type : 'get',
				dataType : 'html',
				data:{kodeArtis:kodeArtis},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit

		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-artis-edit',function(){
			
			$.ajax({
				url:'artis/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah diubah!");
					loadListArtis();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update

		//fungsi ajax untuk popup hapus
		$('#list-artis').on('click','#btn-delete',function(){
			var kodeArtis = $(this).val();
			$.ajax({
				url : 'artis/remove.html',
				type : 'get',
				dataType : 'html',
				data:{kodeArtis:kodeArtis},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup hapus
		
		//fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-artis-delete',function(){
			
			$.ajax({
				url:'artis/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihapus!");
					loadListArtis();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete

		//fungsi ajax untuk popup lihat
		$('#list-artis').on('click','#btn-detail',function(){
			var kodeArtis = $(this).val();
			$.ajax({
				url : 'artis/detail.html',
				type : 'get',
				dataType : 'html',
				data:{kodeArtis:kodeArtis},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup lihat

		
	});
	//akhir dokumen ready
</script>
