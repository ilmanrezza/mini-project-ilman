<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${testimonyModel}" var="testimonyModel" varStatus="number">
	<tr>
		<td>${testimonyModel.title}</td>
		<td></td>
		<td>

			
			<div class="btn-group">
    			<a type="button" href="#" class="glyphicon glyphicon-menu-hamburger" data-toggle="dropdown" style="color:black;"> </a>
   			 	<div class="dropdown-menu">
      			<button class="dropdown-item" value="${testimonyModel.id}" id="btn-edit" style="border:none; background-color : white;">Edit</button><br>
     			<button class="dropdown-item" value="${testimonyModel.id}" id="btn-delete" style="border:none; background-color : white;">Delete</button>
    			</div>
  			</div>
		</td>
		
	</tr>
</c:forEach>