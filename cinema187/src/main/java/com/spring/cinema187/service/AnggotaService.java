package com.spring.cinema187.service;

import java.util.List;

import com.spring.cinema187.model.AnggotaModel;

public interface AnggotaService {

	public void create(AnggotaModel anggotaModel);

	public List<AnggotaModel> searchAll();

}
