package com.spring.cinema187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.cinema187.dao.ProduserDao;
import com.spring.cinema187.model.ProduserModel;
import com.spring.cinema187.service.ProduserService;

@Service
@Transactional
public class ProduserServiceImpl implements ProduserService {

	@Autowired
	private ProduserDao produserDao;

	@Override
	public void create(ProduserModel produserModel) {
		// TODO Auto-generated method stub
		this.produserDao.create(produserModel);
	}

	@Override
	public List<ProduserModel> searchAll() {
		// TODO Auto-generated method stub
		return this.produserDao.searchAll();
	}

	@Override
	public ProduserModel searchKode(String kodeProduser) {
		// TODO Auto-generated method stub
		return this.produserDao.searchKode(kodeProduser);
	}

	@Override
	public void update(ProduserModel produserModel) {
		// TODO Auto-generated method stub
		this.produserDao.update(produserModel);

	}

	@Override
	public void delete(ProduserModel produserModel) {
		// TODO Auto-generated method stub
		this.produserDao.delete(produserModel);

	}

}
