package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.LokasiDao;
import com.spring.cinema187.model.LokasiModel;

@Repository
public class LokasiDaoImpl implements LokasiDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<LokasiModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<LokasiModel> lokasiModelList = new ArrayList<LokasiModel>();
		String query = " from LokasiModel ";
		lokasiModelList = session.createQuery(query).list();
		
		return lokasiModelList;
	}
}
