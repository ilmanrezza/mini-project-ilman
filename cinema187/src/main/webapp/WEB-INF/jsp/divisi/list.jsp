<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${divisiModelList}" var="divisiModel" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${divisiModel.idDivisi}</td>
		<td>${divisiModel.kodeDivisi}</td>
		<td>${divisiModel.namaDivisi}</td>
				
		<td>
			<button type="button" class="btn btn-warning" value="${divisiModel.kodeDivisi}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${divisiModel.kodeDivisi}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${divisiModel.kodeDivisi}" id="btn-delete">Hapus</button>
		</td>
	</tr>
</c:forEach>