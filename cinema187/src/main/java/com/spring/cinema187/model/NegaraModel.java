package com.spring.cinema187.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_NEGARA") //nama tabel
public class NegaraModel {
	private String kodeNegara; //nama variabel
	private String namaNegara;

//	Join Tabel Ke tabel Genre
	private String kodeGenre; //pk
	private GenreModel genreModel;
	
//	Join Tabel Ke Tabel Bahasa
	private Integer idBahasa;
	private BahasaModel bahasaModel;
	
	@Id
	@Column(name="KD_NEGARA")
	public String getKodeNegara() {
		return kodeNegara;
	}
	public void setKodeNegara(String kodeNegara) {
		this.kodeNegara = kodeNegara;
	}
	
	@Column(name="NM_NEGARA")
	public String getNamaNegara() {
		return namaNegara;
	}
	public void setNamaNegara(String namaNegara) {
		this.namaNegara = namaNegara;
	}
	
	@Column(name = "KD_GENRE")
	public String getKodeGenre() {
		return kodeGenre;
	}
	public void setKodeGenre(String kodeGenre) {
		this.kodeGenre = kodeGenre;
	}
	
	@ManyToOne
	@JoinColumn(name="KD_GENRE", nullable=true, updatable=false, insertable=false)
	public GenreModel getGenreModel() {
		return genreModel;
	}
	public void setGenreModel(GenreModel genreModel) {
		this.genreModel = genreModel;
	}
	
	@Column(name="ID_BAHASA")
	public Integer getIdBahasa() {
		return idBahasa;
	}
	public void setIdBahasa(Integer idBahasa) {
		this.idBahasa = idBahasa;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_BAHASA", nullable=true, updatable=false, insertable=false)	
	public BahasaModel getBahasaModel() {
		return bahasaModel;
	}
	public void setBahasaModel(BahasaModel bahasaModel) {
		this.bahasaModel = bahasaModel;
	}


	
}
