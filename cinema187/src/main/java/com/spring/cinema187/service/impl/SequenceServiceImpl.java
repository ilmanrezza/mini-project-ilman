package com.spring.cinema187.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.cinema187.dao.SequenceDao;
import com.spring.cinema187.service.SequenceService;

@Service
@Transactional
public class SequenceServiceImpl implements SequenceService {

	@Autowired
	private SequenceDao sequenceDao;
	
	@Override
	public Integer searchForIdMahasiswaSeq(String sequenceName) {
		// TODO Auto-generated method stub
		return this.sequenceDao.searchForIdMahasiswaSeq(sequenceName);
	}

}
