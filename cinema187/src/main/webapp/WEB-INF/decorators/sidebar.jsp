
<nav class="navbar" style="height: 100%; position: fixed;"
	role="navigation">
	<aside class="main-sidebar">

		<section class="sidebar">
			<ul class="sidebar-menu">
				<li class="header">Smart POS</li>
					
				<li>
					<a href="#"><i class="fa fa-bars"></i><span>Master</span><span class="pull-right-container"> </span></a>
					<ul class="treeview-menu">
						<c:choose>
							<c:when test="${kodeRole=='ROLE_ADMIN'}">
								<li>
									<a href="${contextName}/mahasiswa.html" class="menu-item"><i class="fa fa-truck"></i>Mahasiswa</a>
								</li>
							</c:when>
						</c:choose>
						
						
						<li>
							<a href="${contextName}/fakultas.html" class="menu-item"><i class="fa fa-truck"></i>Fakultas</a>
						</li>
						<li>
							<a href="${contextName}/negara.html" class="menu-item"><i class="fa fa-truck"></i>Negara</a> 
						</li>
						<li>
							<a href="${contextName}/artis.html" class="menu-item"><i class="fa fa-truck"></i>Artis</a> 
						</li>
						<li>
							<a href="${contextName}/produser.html" class="menu-item"><i class="fa fa-truck"></i>Produser</a> 
						</li>
						<li>
							<a href="${contextName}/genre.html" class="menu-item"><i class="fa fa-truck"></i>Genre</a> 
						</li>
						<li>
							<a href="${contextName}/belajar.html" class="menu-item"><i class="fa fa-truck"></i>Belajar</a> 
						</li>
						<li>
							<a href="${contextName}/film.html" class="menu-item"><i class="fa fa-truck"></i>Film</a> 
						</li>
						<li>
							<a href="${contextName}/divisi.html" class="menu-item"><i class="fa fa-truck"></i>Divisi</a> 
						</li>
						<li>
							<a href="${contextName}/anggota.html" class="menu-item"><i class="fa fa-truck"></i>Anggota</a> 
						</li>
						<li>
							<a href="${contextName}/ajax.html" class="menu-item"><i class="fa fa-truck"></i>ajax</a> 
						</li>
						<li>
							<a href="${contextName}/bootcamptes.html" class="menu-item"><i class="fa fa-truck"></i>Bootcamp Test Type</a> 
						</li>
						
						<li>
							<a href="${contextName}/category.html" class="menu-item"><i class="fa fa-truck"></i>Category</a> 
						</li>
						
							<li>
							<a href="${contextName}/testimony.html" class="menu-item"><i class="fa fa-truck"></i>Testimony</a> 
						</li>
						
						</li>
						
							<li>
							<a href="${contextName}/question.html" class="menu-item"><i class="fa fa-truck"></i>Question</a> 
						</li>
						
					</ul>
				</li>	
				
				<li>
					<a href="#"><i class="fa fa-bars"></i> <span>Transaksi</span><span class="pull-right"></span></a>
					<ul class="treeview-menu">
						<li>
							<a href="#"><i class="fa fa-calculator"></i><span>Kasir</span><span class="pull-right-container"></span></a>
							<ul class="treeview-menu">
								<li>
									<a href="${contextName}/kasir_header.html" class="menu-item"><i class="fa fa-calculator"></i>Kasir</a>
								</li>
								<li>
									<a href="${contextName}/kasir_detail.html" class="menu-item"><i class="fa fa-list-alt"></i>Detail</a>
								</li>
							</ul>
						</li>
						
						<li>
							<a href="#"><i class="fa fa-shopping-cart"></i><span>Jasa</span><span class="pull-right-container"></span></a>
							<ul class="treeview-menu">
								<li>
									<a href="${contextName}/jasa_header.html" class="menu-item"><i class="fa fa-shopping-cart"></i>Jasa</a>
								</li>
								<li>
									<a href="${contextName}/jasa_detail.html" class="menu-item"><i class="fa fa-list-alt"></i>Detail</a>
								</li>
							</ul>
						</li>
						
						<li>
							<a href="#"><i class="fa fa-shopping-basket"></i><span>Gudang</span><span class="pull-right-container"></span></a>
							<ul class="treeview-menu">
								<li>
									<a href="${contextName}/gudang_header.html" class="menu-item"><i class="fa fa-shopping-basket"></i>Gudang</a>
								</li>
								<li>
									<a href="${contextName}/gudang_detail.html" class="menu-item"><i class="fa fa-list-alt"></i>Detail</a>
								</li>
							</ul>
						</li>
						
						<li>
							<a href="#"><i class="fa fa-mail-reply"></i><span>Retur</span><span class="pull-right-container"></span></a>
							<ul class="treeview-menu">
								<li>
									<a href="${contextName}/retur_header.html" class="menu-item"><i class="fa fa-mail-reply"></i>Retur</a>
								</li>
								<li>
									<a href="${contextName}/retur_detail.html" class="menu-item"><i class="fa fa-list-alt"></i>Detail</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>	
			</ul>
		</section>
		<!-- /.sidebar -->
	</aside>
</nav>