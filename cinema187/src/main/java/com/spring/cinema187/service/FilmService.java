package com.spring.cinema187.service;

import java.util.List;

import com.spring.cinema187.model.FilmModel;

public interface FilmService {

	public void create(FilmModel filmModel);

	public List<FilmModel> searchAll();

	public FilmModel searchKode(String kodeFilm);

	public void update(FilmModel filmModel);

	public void delete(FilmModel filmModel);


}
