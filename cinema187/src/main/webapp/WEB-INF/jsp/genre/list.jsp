<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${genreModelList}" var="genreModel" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${genreModel.kodeGenre}</td>
		<td>${genreModel.namaGenre}</td>
		<td>

			
			<div class="btn-group">
    			<a type="button" href="#" class="glyphicon glyphicon-menu-hamburger" data-toggle="dropdown" style="color:black;"> </a>
   			 	<div class="dropdown-menu">
   			 	<button class="dropdown-item" href="#" value="${genreModel.kodeGenre}" id="btn-detail">Lihat</button><br>
      			<button class="dropdown-item" href="#" value="${genreModel.kodeGenre}" id="btn-edit">Edit</button><br>
     			<button class="dropdown-item" href="#" value="${genreModel.kodeGenre}" id="btn-delete">Delete</button>
    			</div>
  			</div>
		</td>
		
	</tr>
</c:forEach>