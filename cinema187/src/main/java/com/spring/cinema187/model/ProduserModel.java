package com.spring.cinema187.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_PRODUSER")

public class ProduserModel {
	private String kodeProduser;
	private String namaProduser;
	private String international;
	
	@Id
	@Column(name="KD_PRODUSER")
	public String getKodeProduser() {
		return kodeProduser;
	}
	public void setKodeProduser(String kodeProduser) {
		this.kodeProduser = kodeProduser;
	}
	
	@Column(name="NM_PRODUSER")
	public String getNamaProduser() {
		return namaProduser;
	}
	public void setNamaProduser(String namaProduser) {
		this.namaProduser = namaProduser;
	}
	
	@Column(name="INTERNATIONAL")
	public String getInternational() {
		return international;
	}
	public void setInternational(String international) {
		this.international = international;
	}
	

}
