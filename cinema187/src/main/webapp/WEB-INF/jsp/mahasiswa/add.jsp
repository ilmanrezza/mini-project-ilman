<h4>Halaman Tambah Mahasiswa</h4>
${xsis}
<br/>
<div class="form-horizontal">
	<form action="#" method="get" id="form-mahasiswa-add">
	
		<div class="form-group">
			<label class="control-label col-md-3">NIM</label>
			<input type="text" class="form-input" name="nimMahasiswa_Disabled" id="nimMahasiswa_Disabled" value="${nimMahasiswaOtomatis}" disabled="disabled"/>
			<input type="hidden" class="form-input" name="nimMahasiswa" id="nimMahasiswa" value="${nimMahasiswaOtomatis}" />
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama</label>
			<input type="text" class="form-input" name="namaMahasiswa" id="namaMahasiswa"/>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Telepon</label>
			<select id="provider" onclick="pilihProvider();">
				<option value="0812">Telkomsel</option>
				<option value="0856">Indosat</option>
				<option value="0899">XL</option>
			</select>
			<input type="text" class="form-input" name="teleponMahasiswa" id="teleponMahasiswa"/>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Gender</label>
			<input type="radio" class="form-input" name="genderMahasiswa" id="pria" value="pria"/>Pria 
			<input type="radio" class="form-input" name="genderMahasiswa" id="wanita" value="wanita"/>Wanita
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Alamat</label>
			<textarea rows="5" cols="30" name="alamatMahasiswa" id="alamatMahasiswa"></textarea>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Tahun Lahir</label>
			<input type="text" class="form-input" name="tahunLahirMahasiswa" id="tahunLahirMahasiswa"/>
		</div>
		
		<div class="modal-footer">
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</form>
</div>

<script>
	function pilihProvider() {
		var provider = document.getElementById('provider');
		var teleponMahasiswa = document.getElementById('teleponMahasiswa');
		
		teleponMahasiswa.value = provider.value;
	}
</script>