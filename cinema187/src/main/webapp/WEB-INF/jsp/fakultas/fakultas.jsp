
<div class="panel" style="background: white;margin-top: 50px; min-height: 500px">
	<h1>Halaman Fakultas</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah</button>
	
	<div>
		<table class="table" id="tbl-fakultas">
			<tr>
				<td></td>
				<td></td>
				<td align="right">Cari kode / nama</td>
				<td>
					<form action="#" method="get" id="form-fakultas-search">
						<input type="text" id="keyword" name="keyword" size="20"/>
						<button type="submit">Cari</button>
					</form>
				</td>
			</tr>
			<tr>
				<td>No</td>
				<td>Kode Fakultas</td>
				<td>Nama Fakultas</td>
				<td>Aksi</td>
			</tr>
			<tr>
				<td></td>
				<td><form action="#" method="get" id="form-fakultas-search-kode">
						<input type="text" id="kodeFakultasKey" name="kodeFakultasKey" size="20"/>
						<br/>
						<button type="submit">Cari</button>
					</form>
				</td>
				<td>
					<form action="#" method="get" id="form-fakultas-search-nama">
						<input type="text" id="namaFakultasKey" name="namaFakultasKey" size="20"/>
						<br/>
						<button type="submit">Cari</button>
					</form>
				</td>
				<td></td>
			</tr>
			<tbody id="list-fakultas">
			
			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Fakultas</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script>
	
	loadListFakultas();
	
	function loadListFakultas() {
		$.ajax({
			url:'fakultas/cruds/list.html',
			type:'get',
			dataType:'html',
			success: function(result) {
				$('#list-fakultas').html(result);
			}
		});
	}
	
	//document ready itu setelah halamannya dipanggil
	$(document).ready(function(){
		
		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click',function(){
			$.ajax({
				url:'fakultas/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah
		
		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit','#form-fakultas-add',function(){
			$.ajax({
				url:'fakultas/cruds/create.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					
					if (result.jumlahDataFakultas > 0 ) {
						alert("nama fakultas "+result.namaFakultas+" sudah ada di DB");
					} else {
						$('#modal-input').modal('hide');
						alert("data telah ditambah!");
						loadListFakultas();
					}
					
					
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah
		
		//fungsi ajax untuk lihat
		$('#list-fakultas').on('click','#btn-detail',function(){
			var idFakultas = $(this).val();
			alert(idFakultas);
			$.ajax({
				url:'fakultas/cruds/detail.html',
				type:'get',
				dataType:'html',
				data:{idFakultas:idFakultas},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk lihat
		
		//fungsi ajax untuk ubah
		$('#list-fakultas').on('click','#btn-edit',function(){
			var idFakultas = $(this).val();
			$.ajax({
				url:'fakultas/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{idFakultas:idFakultas},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk ubah
		
		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-fakultas-edit',function(){
			
			$.ajax({
				url:'fakultas/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah diubah!");
					loadListFakultas();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
		
		//fungsi ajax untuk hapus
		$('#list-fakultas').on('click','#btn-delete',function(){
			var idFakultas = $(this).val();
			$.ajax({
				url:'fakultas/cruds/remove.html',
				type:'get',
				dataType:'html',
				data:{idFakultas:idFakultas},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk hapus
		
		//fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-fakultas-delete',function(){
			
			$.ajax({
				url:'fakultas/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihapus!");
					loadListFakultas();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
		//fungsi ajax untuk search nama
		$('#form-fakultas-search-nama').on('submit',function(){
			$.ajax({
				url:'fakultas/cruds/search/nama.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan nama!");
					$('#list-fakultas').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search nama
		
		//fungsi ajax untuk search kode
		$('#form-fakultas-search-kode').on('submit',function(){
			$.ajax({
				url:'fakultas/cruds/search/kode.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode!");
					$('#list-fakultas').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search kode
		
		//fungsi ajax untuk search kode / nama
		$('#form-fakultas-search').on('submit',function(){
			$.ajax({
				url:'fakultas/cruds/search.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode atau nama!");
					$('#list-fakultas').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search kode / nama
		
	});
	//akhir dokumen ready
</script>