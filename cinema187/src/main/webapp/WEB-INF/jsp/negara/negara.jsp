<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Negara</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah
		Negara</button>

	<div>
		<table class="table" id="tbl-negara">
			<tr>
				<td></td>
				<td></td>
				<td align="right">Cari kode / nama</td>
				<td>
					<form action="#" method="get" id="form-negara-search">
						<input type="text" id="keyword" name="keyword" size="20"/>
						<button type="submit">Cari</button>
					</form>
				</td>
			</tr>
			<tr>
				<td>1</td>
				<td>
					<form action="#" method="get" id="form-negara-search-kode">
						<input type="text" id="kodeNegara" name="kodeNegara"/>
						<button type="submit">Cari Kode Negara</button>
					</form></td> 
				<td>
					<form action="#" method="get" id="form-negara-search-nama">
						<input type="text" id="namaNegara" name="namaNegara"/>
						<button type="submit">Cari Nama Negara</button>
					</form></td>
				<td>4</td>
	
			</tr>
			<tr>
				<td>No</td>
				<td>Kode Negara</td>
				<td>Nama Negara</td>
				<td>Aksi</td>
			</tr>
			<tbody id="list-negara">

			</tbody>
		</table>
	</div>


</div>

<!-- syntax popup -->
<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Negara</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	loadListNegara();

	function loadListNegara() {
		$.ajax({
			url : 'negara/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-negara').html(result);
			}
		});
	}

	//document ready itu setelah halamannya dipanggil
	$(document).ready(function() {

		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'negara/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah

		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit', '#form-negara-add', function() {
			$.ajax({
				url : 'negara/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {

					$('#modal-input').modal('hide');
					alert("data telah ditambah!");

				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah
		
		//fungsi ajax untuk popup edit
		$('#list-negara').on('click','#btn-edit',function(){
			var kodeNegara = $(this).val();
			$.ajax({
				url : 'negara/edit.html',
				type : 'get',
				dataType : 'html',
				data:{kodeNegara:kodeNegara},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit

		//fungsi ajax untuk popup lihat
		$('#list-negara').on('click','#btn-detail',function(){
			var kodeNegara = $(this).val();
			$.ajax({
				url : 'negara/detail.html',
				type : 'get',
				dataType : 'html',
				data:{kodeNegara:kodeNegara},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup lihat

		//fungsi ajax untuk popup hapus
		$('#list-negara').on('click','#btn-delete',function(){
			var kodeNegara = $(this).val();
			$.ajax({
				url : 'negara/remove.html',
				type : 'get',
				dataType : 'html',
				data:{kodeNegara:kodeNegara},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup hapus

		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-negara-edit',function(){
			
			$.ajax({
				url:'negara/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah diubah!");
					loadListNegara();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update

		//fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-negara-delete',function(){
			
			$.ajax({
				url:'negara/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihapus!");
					loadListNegara();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete

		//fungsi ajax untuk search kode
		$('#form-negara-search-kode').on('submit',function(){
			$.ajax({
				url:'negara/search/kode.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode!");
					$('#list-negara').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search kode

		//fungsi ajax untuk search nama
		$('#form-negara-search-nama').on('submit',function(){
			$.ajax({
				url:'negara/search/nama.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan nama!");
					$('#list-negara').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search nama

		//fungsi ajax untuk search kode / nama
		$('#form-negara-search').on('submit',function(){
			$.ajax({
				url:'negara/search.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode atau nama!");
					$('#list-negara').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search kode / nama

		
	});
	//akhir dokumen ready
</script>
