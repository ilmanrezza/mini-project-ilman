<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Film</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah
		Film</button>

	<div>
		<table class="table" id="tbl-film">
			<tr>
				<td>No</td>
				<td>Kode Film</td>
				<td>Nama Film</td>
				<td>Genre</td>
				<td>Artis</td>
				<td>Produser</td>
				<td>Pendapatan</td>
				<td>Nominasi</td>
			</tr>
			<tbody id="list-film">

			</tbody>
		</table>
	</div>


</div>

<!-- syntax popup -->
<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Film</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	loadListFilm();

	function loadListFilm() {
		$.ajax({
			url : 'film/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-film').html(result);
			}
		});
	}

	//document ready itu setelah halamannya dipanggil
	$(document).ready(function() {

		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'film/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah

		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit', '#form-film-add', function() {
			$.ajax({
				url : 'film/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {

					$('#modal-input').modal('hide');
					alert("data telah ditambah!");

				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah

		//fungsi ajax untuk popup edit
		$('#list-film').on('click','#btn-edit',function(){
			var kodeFilm = $(this).val();
			$.ajax({
				url : 'film/edit.html',
				type : 'get',
				dataType : 'html',
				data:{kodeFilm:kodeFilm},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit

		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-film-edit',function(){
			
			$.ajax({
				url:'film/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah diubah!");
					loadListFilm();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update

		//fungsi ajax untuk popup hapus
		$('#list-film').on('click','#btn-delete',function(){
			var kodeFilm = $(this).val();
			$.ajax({
				url : 'film/remove.html',
				type : 'get',
				dataType : 'html',
				data:{kodeFilm:kodeFilm},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup hapus
		
		//fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-film-delete',function(){
			
			$.ajax({
				url:'film/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihapus!");
					loadListFilm();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
		//fungsi ajax untuk popup lihat
		$('#list-film').on('click','#btn-detail',function(){
			var kodeFilm = $(this).val();
			$.ajax({
				url : 'film/detail.html',
				type : 'get',
				dataType : 'html',
				data:{kodeFilm:kodeFilm},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup lihat

		
		
	});
	//akhir dokumen ready
</script>

