package com.spring.cinema187.service;

import java.util.List;

import com.spring.cinema187.model.FakultasModel;

public interface FakultasService {
	
	public void create(FakultasModel fakultasModel);
	public void update(FakultasModel fakultasModel);
	public void delete(FakultasModel fakultasModel);
	public List<FakultasModel> search(String kodeRole);
	public FakultasModel searchId(Integer idFakultas);
	public List<FakultasModel> searchNama(String namaFakultas);
	public List<FakultasModel> searchKode(String kodeFakultas);
	public List<FakultasModel> searchKodeOrNama(String kodeFakultas, String namaFakultas);
	public List<FakultasModel> searchKodeEqual(String kodeFakultas);
	public List<FakultasModel> searchNamaEqual(String namaFakultas);
	public void deleteTemporary(FakultasModel fakultasModel);
	
	/*public output nama(type input)*/
	/*sintax input tanpa ouput
	public void simpan(String kodeFakultas);
	public void ubahId(Integer idFakultas);
	public void ubahNama(String namaFakultas);
	public void ubahKode(String kodeFakultas);
	public void cari(String namaFakultas);
	public void createDua(Integer idFakultas, String kodeFakultas, String namaFakultas);*/
	
	/*sintax tanpa input dengan ouput
	SELECT * FROM M_FAKULTAS
	public FakultasModel selectAll();*/
	
	/*sintax input dan output
	SELECT * FROM M_FAKULTAS WHERE NAMA_FAKULTAS='';
	public FakultasModel searchNama(String namaFakultas);
	
	SELECT * FROM M_FAKULTAS WHERE NAMA_FAKULTAS='' And KODE_FAKULTAS='';
	public FakultasModel searchNamaAndKode(String namaFakultas, String kodeFakultas);
	
	SELECT * FROM M_FAKULTAS WHERE ID_FAKULTAS='' Or KODE_FAKULTAS='';
	public FakultasModel searchIdOrKode(Integer idFakultas, String kodeFakultas);*/
}
