package com.spring.cinema187.service;

import java.util.List;

import com.spring.cinema187.model.LokasiModel;

public interface LokasiService {
	
	public List<LokasiModel> search();
}
