<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Tambah Film</h1>
	<form action="#" method="get" id="form-film-add">
		<table>
			<tr>
				<td>Kode Film</td>
				<td><input type="text" id="kodeFilm" name="kodeFilm"/></td>
			</tr>
			<tr>
				<td>Nama Film</td>
				<td><input type="text" id="namaFilm"name="namaFilm"/></td>
			</tr>
			<tr>
				<td>Genre</td>
				<td><input type="text" id="genre"name="genre"/></td>
			</tr>
			<tr>
				<td>Artis</td>
				<td><input type="text" id="artis"name="artis"/></td>
			</tr>
			<tr>
				<td>Produser</td>
				<td><input type="text" id="produser"name="produser"/></td>
			</tr>
			<tr>
				<td>Pendapatan</td>
				<td><input type="text" id="pendapatan"name="pendapatan"/></td>
			</tr>
			<tr>
				<td>Nominasi</td>
				<td><input type="text" id="nominasi"name="nominasi"/></td>
			</tr>
		</table>
		<button type="submit" onclick="validasi();">Simpan</button> 
	</form>
</div>

<script type = "text/javascript">
	function validasi() {
		var kodeFilm = document.getElementById("kodeFilm");
		if (kodeFilm.value == "") {
			kodeFilm.style.borderColor = "red";
		} else {
			kodeFilm.style.borderColor = "gray";
		}
		
		var namaFilm = document.getElementById("namaFilm");
		if (namaFilm.value == "") {
			namaFilm.style.borderColor = "red";
		} else {
			namaFilm.style.borderColor = "gray";
		}
		
		var genre = document.getElementById("genre");
		if (genre.value == "") {
			genre.style.borderColor = "red";
		} else {
			genre.style.borderColor = "gray";
		}
	
		var artis = document.getElementById("artis");
		if (artis.value == "") {
			artis.style.borderColor = "red";
		} else {
			artis.style.borderColor = "gray";
		}
		
		var produser = document.getElementById("produser");
		if (produser.value == "") {
			produser.style.borderColor = "red";
		} else {
			produser.style.borderColor = "gray";
		}

		var pendapatan = document.getElementById("pendapatan");
		if (pendapatan.value == "") {
			pendapatan.style.borderColor = "red";
		} else {
			pendapatan.style.borderColor = "gray";
		}
		
		var nominasi = document.getElementById("nominasi");
		if (nominasi.value == "") {
			nominasi.style.borderColor = "red";
		} else {
			nominasi.style.borderColor = "gray";
		}
	}
</script>