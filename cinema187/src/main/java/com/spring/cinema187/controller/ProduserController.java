package com.spring.cinema187.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.cinema187.model.ProduserModel;
import com.spring.cinema187.model.ProduserModel;
import com.spring.cinema187.service.ProduserService;

@Controller
public class ProduserController {
	@Autowired
	private ProduserService produserService;

	@RequestMapping(value = "produser")
	public String produser() {
		String jsp = "produser/produser";
		return jsp;
	}
	
	@RequestMapping(value = "produser/add") // url
	public String produserAdd() {
		String jsp = "produser/add";
		return jsp;
	}
	
	@RequestMapping(value="produser/create")
	public String produserCreate(HttpServletRequest request) {
		String kodeProduser = request.getParameter("kodeProduser");
		String namaProduser = request.getParameter("namaProduser");
		String international = request.getParameter("international");

		ProduserModel produserModel = new ProduserModel(); //kantong / tempatnya
		produserModel.setKodeProduser(kodeProduser);
		produserModel.setNamaProduser(namaProduser);
		produserModel.setInternational(international);
		this.produserService.create(produserModel);
		
		String jsp = "produser/produser";
		return jsp;
	}
	
	@RequestMapping(value="produser/list")
	public String produserList(Model model) {
		List<ProduserModel> produserModelList = new ArrayList<ProduserModel>();
		produserModelList = this.produserService.searchAll();
		model.addAttribute("produserModelList", produserModelList); 
		String jsp = "produser/list";
		return jsp;

	}
	
	@RequestMapping(value="produser/edit")
	public String produserEdit(Model model, HttpServletRequest request) {
		String kodeProduser = request.getParameter("kodeProduser");
		ProduserModel produserModel = new ProduserModel();
		produserModel = this.produserService.searchKode(kodeProduser);
		model.addAttribute("produserModel", produserModel); 			//backend ke frontend, taro di edit.
		String kota = "Jakarta";
		Integer usia = 22;
		model.addAttribute("kota", kota);
		model.addAttribute("usia", usia);
		
		String jsp = "produser/edit";
		return jsp;
	}

	@RequestMapping(value="produser/update")
	public String produserUpdate(Model model, HttpServletRequest request) {
		String kodeProduser = request.getParameter("kodeProduser");
		String namaProduser = request.getParameter("namaProduser");
		String international = request.getParameter("international");
		
		ProduserModel produserModel = new ProduserModel();
		produserModel = this.produserService.searchKode(kodeProduser);
		produserModel.setNamaProduser(namaProduser); //set
		produserModel.setInternational(international);
		
		this.produserService.update(produserModel);
		
		String jsp = "produser/produser";
		return jsp;
	}
	
	@RequestMapping(value="produser/remove")
	public String produserRemove(Model model, HttpServletRequest request) {
		String kodeProduser = request.getParameter("kodeProduser");
		ProduserModel produserModel = new ProduserModel();
		produserModel = this.produserService.searchKode(kodeProduser);
		model.addAttribute("produserModel", produserModel);
		
		String jsp = "produser/remove";
		return jsp;
	}

	@RequestMapping(value="produser/delete")
	public String produserDelete(Model model, HttpServletRequest request) {
		String kodeProduser = request.getParameter("kodeProduser");
		String namaProduser = request.getParameter("namaProduser");
		String international = request.getParameter("international");
		
		ProduserModel produserModel = new ProduserModel();
		produserModel = this.produserService.searchKode(kodeProduser);
		
		this.produserService.delete(produserModel);
		
		String jsp = "produser/produser";
		return jsp;
	}

	@RequestMapping(value="produser/detail")
	public String produserDetail(Model model, HttpServletRequest request) {
		String kodeProduser = request.getParameter("kodeProduser");
		ProduserModel produserModel = new ProduserModel();
		produserModel = this.produserService.searchKode(kodeProduser);
		model.addAttribute("produserModel", produserModel);
		
		String jsp = "produser/detail";
		return jsp;
	}

	
}
