package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.ArtisDao;
import com.spring.cinema187.model.ArtisModel;

@Repository
public class ArtisDaoImpl implements ArtisDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(ArtisModel artisModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //set start query
		session.save(artisModel); //hibernate(framework) tabel (hql), kl di
		
	}

	@Override
	public List<ArtisModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<ArtisModel> artisModelList = new ArrayList<ArtisModel>();
		String query = "from ArtisModel";
		String kondisi = " ";
		artisModelList = session.createQuery(query + kondisi).list(); // hql: select from kl di sql
		return artisModelList;
	}

	@Override
	public ArtisModel searchKode(String kodeArtis) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //set start query
		
		ArtisModel artisModel = new ArtisModel();
		String query = "from ArtisModel";
		String kondisi = " where kodeArtis = '"+kodeArtis+"' "; //query	
		artisModel = (ArtisModel) session.createQuery(query + kondisi).uniqueResult(); //hql: select from kl di sql
		return artisModel;
	}

	@Override
	public void update(ArtisModel artisModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(artisModel);		
		
	}

	@Override
	public void delete(ArtisModel artisModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(artisModel);		
		
	}


	
}
