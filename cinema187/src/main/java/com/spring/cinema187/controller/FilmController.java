package com.spring.cinema187.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.cinema187.model.FilmModel;
import com.spring.cinema187.service.FilmService;

@Controller
public class FilmController {

	@Autowired
	private FilmService filmService;

	@RequestMapping(value = "film")
	public String film() {
		String jsp = "film/film";
		return jsp;
	}

	@RequestMapping(value = "film/add") // url
	public String filmAdd() {
		String jsp = "film/add";
		return jsp;
	}

	@RequestMapping(value = "film/create")
	public String filmCreate(HttpServletRequest request) {
		String kodeFilm = request.getParameter("kodeFilm");
		String namaFilm = request.getParameter("namaFilm");
		String genre = request.getParameter("genre");
		String artis = request.getParameter("artis");
		String produser = request.getParameter("produser");
		String pendapatan = String.valueOf(request.getParameter("pendapatan"));
		String nominasi = String.valueOf(request.getParameter("nominasi"));

		FilmModel filmModel = new FilmModel(); // kantong / tempatnya
		filmModel.setKodeFilm(kodeFilm);
		filmModel.setNamaFilm(namaFilm);
		filmModel.setGenre(genre);
		filmModel.setArtis(artis);
		filmModel.setProduser(produser);
		filmModel.setPendapatan(Integer.valueOf(pendapatan));
		filmModel.setNominasi(Integer.valueOf(nominasi));

		this.filmService.create(filmModel);

		String jsp = "film/film";
		return jsp;
	}

	@RequestMapping(value = "film/list")
	public String filmList(Model model) {
		List<FilmModel> filmModelList = new ArrayList<FilmModel>();
		filmModelList = this.filmService.searchAll();
		model.addAttribute("filmModelList", filmModelList);
		String jsp = "film/list";
		return jsp;

	}

	@RequestMapping(value = "film/edit")
	public String filmEdit(Model model, HttpServletRequest request) {
		String kodeFilm = request.getParameter("kodeFilm");
		FilmModel filmModel = new FilmModel();
		filmModel = this.filmService.searchKode(kodeFilm);
		model.addAttribute("filmModel", filmModel); // backend ke frontend, taro di edit.
		String kota = "Jakarta";
		Integer usia = 22;
		model.addAttribute("kota", kota);
		model.addAttribute("usia", usia);

		String jsp = "film/edit";
		return jsp;
	}

	@RequestMapping(value = "film/update")
	public String filmUpdate(Model model, HttpServletRequest request) {
		String kodeFilm = request.getParameter("kodeFilm");
		String namaFilm = request.getParameter("namaFilm");
		String genre = request.getParameter("genre");
		String artis = request.getParameter("artis");
		String produser = request.getParameter("produser");
		String pendapatan = String.valueOf(request.getParameter("pendapatan"));
		String nominasi = String.valueOf(request.getParameter("nominasi"));

		FilmModel filmModel = new FilmModel(); // kantong / tempatnya
		filmModel = this.filmService.searchKode(kodeFilm);
		filmModel.setNamaFilm(namaFilm);
		filmModel.setGenre(genre);
		filmModel.setArtis(artis);
		filmModel.setProduser(produser);
		filmModel.setPendapatan(Integer.valueOf(pendapatan));
		filmModel.setNominasi(Integer.valueOf(nominasi));

		this.filmService.update(filmModel);

		String jsp = "film/film";
		return jsp;
	}

	@RequestMapping(value = "film/remove")
	public String filmRemove(Model model, HttpServletRequest request) {
		String kodeFilm = request.getParameter("kodeFilm");
		FilmModel filmModel = new FilmModel();
		filmModel = this.filmService.searchKode(kodeFilm);
		model.addAttribute("filmModel", filmModel);

		String jsp = "film/remove";
		return jsp;
	}

	@RequestMapping(value = "film/delete")
	public String filmDelete(Model model, HttpServletRequest request) {
		String kodeFilm = request.getParameter("kodeFilm");
		String namaFilm = request.getParameter("namaFilm");
		String genre = request.getParameter("genre");
		String artis = request.getParameter("artis");
		String produser = request.getParameter("produser");
		String pendapatan = String.valueOf(request.getParameter("pendapatan"));
		String nominasi = String.valueOf(request.getParameter("nominasi"));

		FilmModel filmModel = new FilmModel(); // kantong / tempatnya
		filmModel = this.filmService.searchKode(kodeFilm);

		this.filmService.delete(filmModel);

		String jsp = "film/film";
		return jsp;
	}

	@RequestMapping(value="film/detail")
	public String filmDetail(Model model, HttpServletRequest request) {
		String kodeFilm = request.getParameter("kodeFilm");
		FilmModel filmModel = new FilmModel();
		filmModel = this.filmService.searchKode(kodeFilm);
		model.addAttribute("filmModel", filmModel);
		
		String jsp = "film/detail";
		return jsp;
	}

}
