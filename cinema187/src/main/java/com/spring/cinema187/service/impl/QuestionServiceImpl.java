package com.spring.cinema187.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.cinema187.dao.QuestionDao;
import com.spring.cinema187.model.QuestionModel;
import com.spring.cinema187.service.QuestionService;

@Service
@Transactional
public class QuestionServiceImpl implements QuestionService {

	@Autowired
	private QuestionDao questionDao;

	@Override
	public void create(QuestionModel questionModel) {
		// TODO Auto-generated method stub
		this.questionDao.create(questionModel);
	}

	@Override
	public void update(QuestionModel questionModel) {
		// TODO Auto-generated method stub
		this.questionDao.update(questionModel);
	}

	@Override
	public List<QuestionModel> searchAll() {
		// TODO Auto-generated method stub
		return this.questionDao.searchAll();
	}

	@Override
	public QuestionModel searchIdQuestion(Integer idQuestion) {
		// TODO Auto-generated method stub
		return this.questionDao.searchIdQuestion(idQuestion);
	}

	@Override
	public List<QuestionModel> searchQuestion(String question) {
		// TODO Auto-generated method stub
		return this.questionDao.searchQuestion(question);
	}

	@Override
	public void deleteTemporary(QuestionModel questionModel) {
		// TODO Auto-generated method stub
		this.questionDao.update(questionModel);
	}
	
}
