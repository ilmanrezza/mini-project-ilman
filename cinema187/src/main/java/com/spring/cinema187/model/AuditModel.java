package com.spring.cinema187.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity //menandakan entitas
@Table(name = "T_AUDIT_LOG") //harusnya M atau T_Artis utk tabel master
public class AuditModel {
		private Integer idLog;
		private String json_insert;
		private String json_before;
		private String json_after;
		private String json_delete;
		
		
		 // VARIABLE audit log
		private Integer idCreatedBy;
		private UserModel createdBy;
		private Date createdOn;
		// VARIABLE audit log
	
		
		
		@Id
		@Column(name="ID_LOG",length=11,nullable=false) //nama kolom
			@GeneratedValue(strategy=GenerationType.TABLE, generator="T_AUDIT_LOG") // utk buat nilai sequential
			@TableGenerator(name="T_AUDIT_LOG", table="M_SEQUENCE",
							pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_LOG",
							valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
		public Integer getIdLog() {
			return idLog;
		}
		public void setIdLog(Integer idLog) {
			this.idLog = idLog;
		} 
		
		@Column(name="JSON_INSERT")
		public String getJson_insert() {
			return json_insert;
		}
		
		public void setJson_insert(String json_insert) {
			this.json_insert = json_insert;
		}
		@Column(name="JSON_BEFORE")
		public String getJson_before() {
			return json_before;
		}
		public void setJson_before(String json_before) {
			this.json_before = json_before;
		}
		@Column(name="JSON_AFTER")
		public String getJson_after() {
			return json_after;
		}
		public void setJson_after(String json_after) {
			this.json_after = json_after;
		}
		@Column(name="JSON_DELETE")
		public String getJson_delete() {
			return json_delete;
		}
		public void setJson_delete(String json_delete) {
			this.json_delete = json_delete;
		}
		
		
		@Column(name="CREATED_BY",length=11,nullable=false)
		public Integer getIdCreatedBy() {
			return idCreatedBy;
		}
		
		public void setIdCreatedBy(Integer idCreatedBy) {
			this.idCreatedBy = idCreatedBy;
		}
		
		@ManyToOne
		@JoinColumn(name="CREATED_BY", nullable=false, updatable=false, insertable=false)
		public UserModel getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(UserModel createdBy) {
			this.createdBy = createdBy;
		}
		@Column(name="CREATED_ON",nullable=false)
		public Date getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}
}
