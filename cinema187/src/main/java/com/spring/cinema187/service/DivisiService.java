package com.spring.cinema187.service;

import java.util.List;

import com.spring.cinema187.model.DivisiModel;

public interface DivisiService {

	void create(DivisiModel divisiModel);

	public List<DivisiModel> searchAll();

	public DivisiModel searchKode(String idDivisi);

	public void update(DivisiModel divisiModel);
}
