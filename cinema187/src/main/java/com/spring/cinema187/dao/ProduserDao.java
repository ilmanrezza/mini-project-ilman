package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.ProduserModel;

public interface ProduserDao {
	public void create(ProduserModel produserModel);

	public List<ProduserModel> searchAll();

	public ProduserModel searchKode(String kodeProduser);

	public void update(ProduserModel produserModel);

	public void delete(ProduserModel produserModel);

}
