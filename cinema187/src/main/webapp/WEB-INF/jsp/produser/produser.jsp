<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Produser</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah
		Produser</button>

	<div>
		<table class="table" id="tbl-produser">
			<tr>
				<td>No</td>
				<td>Kode Produser</td>
				<td>Nama Produser</td>
				<td>International</td>
				<td>Aksi</td>
			</tr>
			<tbody id="list-produser">

			</tbody>
		</table>
	</div>


</div>

<!-- syntax popup -->
<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Produser</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	loadListProduser();

	function loadListProduser() {
		$.ajax({
			url : 'produser/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-produser').html(result);
			}
		});
	}

	//document ready itu setelah halamannya dipanggil
	$(document).ready(function() {

		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'produser/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah

		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit', '#form-produser-add', function() {
			$.ajax({
				url : 'produser/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {

					$('#modal-input').modal('hide');
					alert("data telah ditambah!");

				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah

		//fungsi ajax untuk popup edit
		$('#list-produser').on('click', '#btn-edit', function() {
			var kodeProduser = $(this).val();
			$.ajax({
				url : 'produser/edit.html',
				type : 'get',
				dataType : 'html',
				data : {
					kodeProduser : kodeProduser
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit

		//fungsi ajax untuk update
		$('#modal-input').on('submit', '#form-produser-edit', function() {

			$.ajax({
				url : 'produser/update.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("data telah diubah!");
					loadListProduser();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update

		//fungsi ajax untuk popup hapus
		$('#list-produser').on('click', '#btn-delete', function() {
			var kodeProduser = $(this).val();
			$.ajax({
				url : 'produser/remove.html',
				type : 'get',
				dataType : 'html',
				data : {
					kodeProduser : kodeProduser
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup hapus

		//fungsi ajax untuk delete
		$('#modal-input').on('submit', '#form-produser-delete', function() {

			$.ajax({
				url : 'produser/delete.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("data telah dihapus!");
					loadListProduser();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete

		//fungsi ajax untuk popup lihat
		$('#list-produser').on('click', '#btn-detail', function() {
			var kodeProduser = $(this).val();
			$.ajax({
				url : 'produser/detail.html',
				type : 'get',
				dataType : 'html',
				data : {
					kodeProduser : kodeProduser
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup lihat

	});
	//akhir dokumen ready
</script>
