package com.spring.cinema187.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.cinema187.dao.AuditDao;
import com.spring.cinema187.model.AuditModel;
import com.spring.cinema187.service.AuditService;
@Service
@Transactional
public class AuditServiceImpl implements AuditService {
	@Autowired
	private AuditDao auditDao;

	@Override
	public void create(AuditModel auditModel) {
		// TODO Auto-generated method stub
		this.auditDao.create(auditModel);
	}

	@Override
	public void update(AuditModel auditModel) {
		// TODO Auto-generated method stub
		this.auditDao.update(auditModel);
	}

	@Override
	public void delete(AuditModel auditModel) {
		// TODO Auto-generated method stub
		this.auditDao.delete(auditModel);
	}

	@Override
	public List<AuditModel> searchAll() {
		// TODO Auto-generated method stub
		return this.auditDao.searchAll();
	}

	@Override
	public void deleteTemporary(AuditModel auditModel) {
		// TODO Auto-generated method stub
		this.auditDao.update(auditModel);
	}
	
}
