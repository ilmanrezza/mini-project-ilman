package com.spring.cinema187.service;

import java.util.List;

import com.spring.cinema187.model.NegaraModel;


public interface NegaraService {
	
	public void create(NegaraModel negaraModel); // fungsi menyimpan data ke database

	public List<NegaraModel> searchAll();

	// pola = public output namaTransaksi(input);
	// select * from T_NEGARA where kodeNegara = ''
	public NegaraModel searchKode(String kodeNegara);

	public void update(NegaraModel negaraModel);

	public void delete(NegaraModel negaraModel);

	public List<NegaraModel> searchKodeLike(String kodeNegara);
	
	public List<NegaraModel> searchNamaLike(String namaNegara);

	public List<NegaraModel> searchKodeOrNama(String kodeNegara, String namaNegara);
	
	public Long countKode();


	// select * from T_NEGARA where namaNegara = ''
	//public NegaraModel searchNama(String namaNegara);

	// select * from T_NEGARA where namaNegara = '' and kodeNegara = ''
//	public NegaraModel searchNamaAndKode(String namaNegara, String kodeNegara);

	// select * from T_NEGARA where namaNegara = '' or kodeNegara = ''
//	public list<NegaraModel> searchNamaOrKode(String namaNegara, String kodeNegara); biasanya list

	// select * from T_NEGARA where namaNegara like '%%'
//	public List<NegaraModel> serachNamaLike(String namaNegara);

	// select * from T_NEGARA where kodeNegara like '%%'
//	public List<NegaraModel> serachKodeLike(String kodeNegara);

}
