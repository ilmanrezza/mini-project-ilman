package com.spring.cinema187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.cinema187.dao.NegaraDao;
import com.spring.cinema187.model.NegaraModel;
import com.spring.cinema187.service.NegaraService;

@Service
@Transactional
public class NegaraServiceImpl implements NegaraService{ //mengambil seluruh judul yg ada di service
	@Autowired
	private NegaraDao negaraDao;

	@Override
	public void create(NegaraModel negaraModel) {
		// TODO Auto-generated method stub
		this.negaraDao.create(negaraModel);  //manggil daonya
	}

	@Override
	public List<NegaraModel> searchAll() {
		// TODO Auto-generated method stub
		return this.negaraDao.searchAll();
	}
	
	public NegaraModel searchKode(String kodeNegara) {
		return this.negaraDao.searchKode(kodeNegara);
	}

	@Override
	public void update(NegaraModel negaraModel) {
		// TODO Auto-generated method stub
		this.negaraDao.update(negaraModel); 
	}

	@Override
	public void delete(NegaraModel negaraModel) {
		// TODO Auto-generated method stub
		this.negaraDao.delete(negaraModel);
	}

	@Override
	public List<NegaraModel> searchKodeLike(String kodeNegara) {
		return this.negaraDao.searchKodeLike(kodeNegara);
	}

//	@Override
//	public NegaraModel searchNama(String namaNegara) {
//		// TODO Auto-generated method stub
//		return this.negaraDao.searchNama(namaNegara);
//	}

	@Override
	public List<NegaraModel> searchNamaLike(String namaNegara) {
		// TODO Auto-generated method stub
		return this.negaraDao.searchNamaLike(namaNegara);
	}

	@Override
	public List<NegaraModel> searchKodeOrNama(String kodeNegara, String namaNegara) {
		// TODO Auto-generated method stub
		return this.negaraDao.searchKodeOrNama(kodeNegara, namaNegara);
	}

	@Override
	public Long countKode() {
		// TODO Auto-generated method stub
		return this.negaraDao.countKode();
	}
	
}
