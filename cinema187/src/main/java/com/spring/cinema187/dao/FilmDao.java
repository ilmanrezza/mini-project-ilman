package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.FilmModel;

public interface FilmDao {

	public void create(FilmModel filmModel);

	public List<FilmModel> searchAll();

	public FilmModel searchKode(String kodeFilm);

	public void update(FilmModel filmModel);

	public void delete(FilmModel filmModel);

}
