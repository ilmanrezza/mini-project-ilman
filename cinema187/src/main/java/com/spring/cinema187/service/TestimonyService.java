package com.spring.cinema187.service;

import java.util.List;
import com.spring.cinema187.model.TestimonyModel;

public interface TestimonyService {
	
	public void create(TestimonyModel testimonyModel);
	public void update(TestimonyModel testimonyModel);
	public List<TestimonyModel> searchAll();
	public TestimonyModel searchId(Integer id);
	public void deleteTemporary(TestimonyModel testimonyModel);
	public List<TestimonyModel> searchTitle(String title);
}
