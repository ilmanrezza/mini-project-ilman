<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${negaraModelList}" var="negaraModel" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${negaraModel.kodeNegara}</td>
		<td>${negaraModel.namaNegara}</td>
		<td>
			<button type="button" class="btn btn-warning" value="${negaraModel.kodeNegara}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${negaraModel.kodeNegara}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${negaraModel.kodeNegara}" id="btn-delete">Hapus</button>
		</td>
	</tr>
</c:forEach>