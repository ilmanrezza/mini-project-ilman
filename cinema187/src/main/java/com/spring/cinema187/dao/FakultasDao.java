package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.FakultasModel;

public interface FakultasDao {
	
	public void create(FakultasModel fakultasModel);
	public void update(FakultasModel fakultasModel);
	public void delete(FakultasModel fakultasModel);
	public List<FakultasModel> search(String kodeRole);
	public FakultasModel searchId(Integer idFakultas);
	public List<FakultasModel> searchNama(String namaFakultas);
	public List<FakultasModel> searchKode(String kodeFakultas);
	public List<FakultasModel> searchKodeOrNama(String kodeFakultas, String namaFakultas);
	public List<FakultasModel> searchKodeEqual(String kodeFakultas);
	public List<FakultasModel> searchNamaEqual(String namaFakultas);
	

}
