package com.spring.cinema187.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.cinema187.model.BootcamptesModel;
import com.spring.cinema187.model.TestimonyModel;
import com.spring.cinema187.service.TestimonyService;

@Controller
public class TestimonyController extends UserController {
	
	@Autowired
	TestimonyService testimonyService;
	
	@RequestMapping(value="testimony")
	public String testimony() {
		String jsp = "testimony/testimony";
		return jsp;
	}
	
	@RequestMapping(value="testimony/add")
	public String testimonyAdd() {
		String jsp = "testimony/add";
		return jsp;
	}
	
	@RequestMapping(value="testimony/create")
	public String testimonyCreate(HttpServletRequest request) {
		
		String title = request.getParameter("title"); 
		TestimonyModel testimonyModel = new TestimonyModel();
		
		Integer idCreatedBy = this.userSearch().getIdUser();
		//dari user
		testimonyModel.setIdCreatedBy(idCreatedBy);
		testimonyModel.setCreatedOn(new Date());
		//set data
		testimonyModel.setTitle(title);
		testimonyModel.setContent(request.getParameter("content"));
		testimonyModel.setIsDelete(false);
		this.testimonyService.create(testimonyModel);
		String jsp = "testimony/testimony";
		return jsp;
	}
	
	@RequestMapping(value ="testimony/list")
	public String testimonyList(Model model) {
		List<TestimonyModel> testimonyModel = new ArrayList<TestimonyModel>();
		//Integer id = this.userSearch().getIdUser();
		testimonyModel = this.testimonyService.searchAll();
		model.addAttribute("testimonyModel", testimonyModel);
		
		String jsp = "testimony/list";
		return jsp;
	}
	
	@RequestMapping(value="testimony/edit")
	public String testimonyEdit(Model model, HttpServletRequest request) {
		String id = request.getParameter("id");
		TestimonyModel testimonyModel = new TestimonyModel();
		testimonyModel = this.testimonyService.searchId(Integer.valueOf(id));
		
		model.addAttribute("testimonyModel", testimonyModel); //backend ke frontend, taro di edit.
		String jsp = "testimony/edit";
		return jsp;
	}
	
	@RequestMapping(value = "testimony/update")
	public String testimonyUpdate(Model model, HttpServletRequest request)
	{	String id = request.getParameter("id");
		String title = request.getParameter("title"); 
		
		TestimonyModel testimonyModel = new TestimonyModel();
		testimonyModel = this.testimonyService.searchId(Integer.valueOf(id));
		//save
			Integer idupdatedBy = this.userSearch().getIdUser();
			testimonyModel.setIdModifiedBy(idupdatedBy);
			testimonyModel.setModifiedOn(new Date());
			
			testimonyModel.setTitle(title);
			testimonyModel.setContent(request.getParameter("content"));
		testimonyModel.setIsDelete(false);
		this.testimonyService.update(testimonyModel);
		String jsp = "testimony/testimony";
		return jsp;
	}
	
	@RequestMapping(value="testimony/search")
	public String testimonySearchTitle(HttpServletRequest request, Model model) {
		String title = request.getParameter("title"); 
		List<TestimonyModel> testimonyModel = new ArrayList<TestimonyModel>();
		testimonyModel = this.testimonyService.searchTitle(title);
		
		model.addAttribute("testimonyModel", testimonyModel);
		String jsp = "testimony/list";
		return jsp;
	}
	
	@RequestMapping(value="testimony/remove")
	public String testimonyRemove(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		TestimonyModel testimonyModel = new TestimonyModel();
		testimonyModel = this.testimonyService.searchId(Integer.valueOf(id));
		
		model.addAttribute("testimonyModel", testimonyModel); //backend ke frontend, taro di edit.
		String jsp ="testimony/remove";
		return jsp;
	}
	
	@RequestMapping(value="testimony/delete")
	public String testimonyDelete(HttpServletRequest request) {
		String id = request.getParameter("id");
		TestimonyModel testimonyModel = new TestimonyModel();
		testimonyModel = this.testimonyService.searchId(Integer.valueOf(id));
		
		testimonyModel.setIsDelete(true);
		
		
		Integer iddeletedBy = this.userSearch().getIdUser();
		testimonyModel.setIdDeletedBy(iddeletedBy);
		testimonyModel.setDeletedOn(new Date());
		
		
		this.testimonyService.deleteTemporary(testimonyModel);
		String jsp = "testimony/testimony";
		return jsp;
	}
	
}
