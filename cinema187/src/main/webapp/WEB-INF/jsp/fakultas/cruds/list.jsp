<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${fakultasModelList}" var="fakultasModel" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${fakultasModel.kodeFakultas}</td>
		<td>${fakultasModel.namaFakultas}</td>
		<td>
			<button type="button" class="btn btn-warning" value="${fakultasModel.idFakultas}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${fakultasModel.idFakultas}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${fakultasModel.idFakultas}" id="btn-delete">Hapus</button>
		</td>
	</tr>
</c:forEach>