package com.spring.cinema187.dao;

import com.spring.cinema187.model.UserModel;

public interface UserDao {

	public UserModel searchUsernamePassword(String username, String password);
}
