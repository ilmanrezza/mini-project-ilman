<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Anggota</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah
		Anggota</button>

	<div>
		<table class="table" id="tbl-anggota">
			<tr>
				<td>No</td>
				<td>ID Anggota</td>
				<td>Kode Anggota</td>
				<td>Nama Anggota</td>
				<td>Usia</td>
			</tr>
			<tbody id="list-anggota">

			</tbody>
		</table>
	</div>


</div>

<!-- syntax popup -->
<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Anggota</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	loadListAnggota();

	function loadListAnggota() {
		$.ajax({
			url : 'anggota/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-anggota').html(result);
			}
		});
	}

	//document ready itu setelah halamannya dipanggil
	$(document).ready(function() {

		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'anggota/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah

		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit', '#form-anggota-add', function() {
			$.ajax({
				url : 'anggota/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {

					$('#modal-input').modal('hide');
					alert("data telah ditambah!");

				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah
	});
	//akhir dokumen ready
</script>