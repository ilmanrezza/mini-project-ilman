<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${categoryModel}" var="categoryModel" varStatus="number">
	<tr>
		<td>${categoryModel.code}</td>
		<td>${categoryModel.name}</td>
		<td>

			
			<div class="btn-group">
    			<a type="button" href="#" class="glyphicon glyphicon-menu-hamburger" data-toggle="dropdown" style="color:black;"> </a>
   			 	<div class="dropdown-menu">
      			<button class="dropdown-item" value="${categoryModel.code}" id="btn-edit" style="border:none; background-color : white;">Edit</button><br>
     			<button class="dropdown-item" value="${categoryModel.code}" id="btn-delete" style="border:none; background-color : white;">Delete</button>
    			</div>
  			</div>
		</td>
		
	</tr>
</c:forEach>