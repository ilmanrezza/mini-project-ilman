package com.spring.cinema187.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_ANGGOTA")
public class AnggotaModel {
	private Integer idAnggota;
	private String kodeAnggota;
	private String namaAnggota;
	private Integer usia;
	
	@Id
	@Column(name = "ID_ANGGOTA")
	public Integer getIdAnggota() {
		return idAnggota;
	}
	public void setIdAnggota(Integer idAnggota) {
		this.idAnggota = idAnggota;
	}

	@Column(name = "KD_ANGGOTA")
	public String getKodeAnggota() {
		return kodeAnggota;
	}
	public void setKodeAnggota(String kodeAnggota) {
		this.kodeAnggota = kodeAnggota;
	}

	@Column(name = "NM_ANGGOTA")
	public String getNamaAnggota() {
		return namaAnggota;
	}
	public void setNamaAnggota(String namaAnggota) {
		this.namaAnggota = namaAnggota;
	}

	@Column(name = "USIA")
	public Integer getUsia() {
		return usia;
	}
	public void setUsia(Integer usia) {
		this.usia = usia;
	}
	
	
}
