<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<!-- taglib atas yaitu prefix c utk looping data -->

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!-- taglib atas yaitu prefix fmt utk format data -->

<c:forEach items="${mahasiswaModelList}" var="mahasiswaModel" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${mahasiswaModel.nimMahasiswa}</td>
		<td>${mahasiswaModel.namaMahasiswa}</td>
		<td>
			<button type="button" class="btn btn-success" value="${mahasiswaModel.idMahasiswa}" id="btn-detil">Detil</button>
			<button type="button" class="btn btn-warning" value="${mahasiswaModel.idMahasiswa}" id="btn-ubah">Ubah</button>
			<button type="button" class="btn btn-danger" value="${mahasiswaModel.idMahasiswa}" id="btn-hapus">Hapus</button>
		</td>
	</tr>
</c:forEach>