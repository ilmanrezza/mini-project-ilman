<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${artisModelList}" var="artisModel" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${artisModel.kodeArtis}</td>
		<td>${artisModel.namaArtis}</td>
		<td>${artisModel.jenisKelamin}</td>
		<td>${artisModel.bayaran}</td>
		<td>${artisModel.award}</td>
		<td>${artisModel.negara}</td>
				
		<td>
			<button type="button" class="btn btn-warning" value="${artisModel.kodeArtis}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${artisModel.kodeArtis}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${artisModel.kodeArtis}" id="btn-delete">Hapus</button>
		</td>
	</tr>
</c:forEach>