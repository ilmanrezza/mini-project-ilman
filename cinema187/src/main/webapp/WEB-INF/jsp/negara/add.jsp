<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Tambah Negara</h1>
	<form action="#" method="get" id="form-negara-add">
		<table>
			<tr>
				<td>Kode Negara</td>
				<td><input type="text" id="kodeNegara" name="kodeNegara" value = "${kodeNegaraGenerator}"/></td>
			</tr>
			<tr>
				<td>Nama Negara</td>
				<td><input type="text" id="namaNegara"name="namaNegara"/></td>
			</tr>
			<tr>
				<td>Genre</td>
				<td><select id = "kodeGenre" name = "kodeGenre">
					<c:forEach items="${genreModelList}" var="genreModel" varStatus="number">
						<option value = "${genreModel.kodeGenre}">${genreModel.namaGenre}</option>
					</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td>Bahasa</td>
				<td><select id = "idBahasa" name = "idBahasa">
					<c:forEach items="${bahasaModelList}" var="bahasaModel" varStatus="number">
						<option value = "${bahasaModel.idBahasa}">${bahasaModel.namaBahasa}</option>
					</c:forEach>
				</select></td>
			</tr>
			
		</table>
		<button type="submit" onclick="validasi();">Simpan</button> 
	</form>
</div>

<script type = "text/javascript">
	function validasi() {
		var kodeNegara = document.getElementById("kodeNegara");
		if (kodeNegara.value == "") {
			kodeNegara.style.borderColor = "red";
		} else {
			kodeNegara.style.borderColor = "gray";
		}
		
		var namaNegara = document.getElementById("namaNegara");
		if (namaNegara.value == "") {
			namaNegara.style.borderColor = "red";
		} else {
			namaNegara.style.borderColor = "gray";
		}
	}
</script>