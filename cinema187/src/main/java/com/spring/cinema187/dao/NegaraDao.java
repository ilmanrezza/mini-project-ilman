package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.NegaraModel;


public interface NegaraDao {
	
	public void create(NegaraModel negaraModel);
	
	public List<NegaraModel> searchAll();

	public NegaraModel searchKode(String kodeNegara);
	
	public void update(NegaraModel negaraModel);
	
	public void delete(NegaraModel negaraModel);
	
	public List<NegaraModel> searchKodeLike(String kodeNegara);
	
	public List<NegaraModel> searchNamaLike(String namaNegara);

	public List<NegaraModel> searchKodeOrNama(String kodeNegara, String namaNegara);
	
	public Long countKode();

	
}
