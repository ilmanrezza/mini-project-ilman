package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.QuestionModel;

public interface QuestionDao {
	public void create(QuestionModel questionModel);
	public void update(QuestionModel questionModel);
	public List<QuestionModel> searchAll();
	public QuestionModel searchIdQuestion(Integer idQuestion);
	public List<QuestionModel> searchQuestion(String question);
}
