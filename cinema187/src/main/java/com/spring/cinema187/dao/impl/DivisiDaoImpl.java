package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.DivisiDao;
import com.spring.cinema187.model.DivisiModel;

@Repository
public class DivisiDaoImpl implements DivisiDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(DivisiModel divisiModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(divisiModel);
	}

	@Override
	public List<DivisiModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<DivisiModel> divisiModelList = new ArrayList<DivisiModel>();
		String query = "from DivisiModel";
		String kondisi = " ";
		divisiModelList = session.createQuery(query + kondisi).list(); // hql: select from kl di sql
		return divisiModelList;
	}

	@Override
	public DivisiModel searchKode(String idDivisi) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //set start query
		
		DivisiModel divisiModel = new DivisiModel();
		String query = "from DivisiModel";
		String kondisi = " where idDivisi = '"+idDivisi+"' "; //query	
		divisiModel = (DivisiModel) session.createQuery(query + kondisi).uniqueResult(); //hql: select from kl di sql
		return divisiModel;
	}

	@Override
	public void update(DivisiModel divisiModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(divisiModel);		
		
	}
	
}
