package com.spring.cinema187.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.AuditDao;
import com.spring.cinema187.model.AuditModel;

@Repository
public class AuditDaoImpl implements AuditDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(AuditModel auditModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(auditModel);
	}

	@Override
	public void update(AuditModel auditModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(auditModel);
	}

	@Override
	public void delete(AuditModel auditModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(auditModel);
	}

	@Override
	public List<AuditModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		return null;
	}


}
