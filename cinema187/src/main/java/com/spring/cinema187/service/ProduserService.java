package com.spring.cinema187.service;

import java.util.List;

import com.spring.cinema187.model.ProduserModel;

public interface ProduserService {
	
	public void create(ProduserModel produserModel);

	public List<ProduserModel> searchAll();

	public ProduserModel searchKode(String kodeProduser);

	public void update(ProduserModel produserModel);

	public void delete(ProduserModel produserModel);

}
