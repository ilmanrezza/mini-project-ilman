package com.spring.cinema187.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "T_DIVISI")
public class DivisiModel {
	private Integer idDivisi;
	private String kodeDivisi;
	private String namaDivisi;
	
	@Id
	@Column(name="ID_DIVISI")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "ID_DIVISI")
	@SequenceGenerator(name="ID_DIVISI", initialValue = 1, allocationSize = 1)
	public Integer getIdDivisi() {
		return idDivisi;
	}
	public void setIdDivisi(Integer idDivisi) {
		this.idDivisi = idDivisi;
	}

	@Column(name="KD_DIVISI")
	public String getKodeDivisi() {
		return kodeDivisi;
	}
	public void setKodeDivisi(String kodeDivisi) {
		this.kodeDivisi = kodeDivisi;
	}

	@Column(name="NM_DIVISI")
	public String getNamaDivisi() {
		return namaDivisi;
	}
	public void setNamaDivisi(String namaDivisi) {
		this.namaDivisi = namaDivisi;
	}

	

}
