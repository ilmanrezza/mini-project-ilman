<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${produserModelList}" var="produserModel" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${produserModel.kodeProduser}</td>
		<td>${produserModel.namaProduser}</td>
		<td>${produserModel.international}</td>
		<td>
			<button type="button" class="btn btn-warning" value="${produserModel.kodeProduser}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${produserModel.kodeProduser}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${produserModel.kodeProduser}" id="btn-delete">Hapus</button>
		</td>
	</tr>
</c:forEach>