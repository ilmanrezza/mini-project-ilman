package com.spring.cinema187.service;

import java.util.List;

import com.spring.cinema187.model.GenreModel;


public interface GenreService {

	public void create(GenreModel genreModel);

	public List<GenreModel> searchAll();

	public GenreModel searchKode(String kodeGenre);

	public void update(GenreModel genreModel);

	public void delete(GenreModel genreModel);

	public List<GenreModel> searchKodeLike(String kodeGenre);

	public List<GenreModel> searchNamaLike(Object namaGenre);
	
	
}
