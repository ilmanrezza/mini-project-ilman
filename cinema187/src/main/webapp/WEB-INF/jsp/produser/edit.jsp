<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Edit Produser</h1>

	<form action="#" method="get" id="form-produser-edit">
		<table>
			<tr>
				<td>Kode Produser</td>
				<td><input type="text" id="kodeProduser" name="kodeProduser" value = "${produserModel.kodeProduser}"/></td>
			</tr>
			<tr>
				<td>Nama Produser</td>
				<td><input type="text" id="namaProduser"name="namaProduser" value = "${produserModel.namaProduser}"/></td>
			</tr>
			<tr>
				<td>International</td>
				<td><input type="text" id="international"name="international" value = "${produserModel.international}"/></td>
			</tr>
		</table>
		<button type="submit" onclick="validasi();">Simpan</button> 
	</form>
</div>

<script type = "text/javascript">
	function validasi() {
		var kodeProduser = document.getElementById("kodeProduser");
		if (kodeProduser.value == "") {
			kodeProduser.style.borderColor = "red";
		} else {
			kodeProduser.style.borderColor = "gray";
		}
		
		var namaProduser = document.getElementById("namaProduser");
		if (namaProduser.value == "") {
			namaProduser.style.borderColor = "red";
		} else {
			namaProduser.style.borderColor = "gray";
		}

		var international = document.getElementById("international");
		if (international.value == "") {
			international.style.borderColor = "red";
		} else {
			international.style.borderColor = "gray";
		}

	}
</script>