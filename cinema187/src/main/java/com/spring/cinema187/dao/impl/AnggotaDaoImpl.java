package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.AnggotaDao;
import com.spring.cinema187.model.AnggotaModel;

@Repository
public class AnggotaDaoImpl implements AnggotaDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(AnggotaModel anggotaModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(anggotaModel);		
	}

	@Override
	public List<AnggotaModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<AnggotaModel> anggotaModelList = new ArrayList<AnggotaModel>();
		String query = "from AnggotaModel";
		String kondisi = " ";
		anggotaModelList = session.createQuery(query + kondisi).list(); // hql: select from kl di sql
		return anggotaModelList;
	}
	
	
}
