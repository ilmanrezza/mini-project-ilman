package com.spring.cinema187.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_BAHASA")
public class BahasaModel {
	private Integer idBahasa;
	private String namaBahasa;
	
	@Id
	@Column(name="ID_BAHASA")
	public Integer getIdBahasa() {
		return idBahasa;
	}
	public void setIdBahasa(Integer idBahasa) {
		this.idBahasa = idBahasa;
	}

	@Column(name="NM_BAHASA")
	public String getNamaBahasa() {
		return namaBahasa;
	}
	public void setNamaBahasa(String namaBahasa) {
		this.namaBahasa = namaBahasa;
	}
	
	
}
