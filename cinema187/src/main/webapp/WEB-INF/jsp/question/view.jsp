<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<form action="#" method="get" id="form-testimony-edit">
		
		<div class="form-group">
      		<div class="col-md-12">
      		 <label for="choice">Title:</label>
     		 <input type="text" class="form-control" id="choice" name="question_type" value="${questionModel.question_type}" disabled="disabled">
     		 </div>
    	</div>
	
		<div class="form-group">
				<div class="col-md-12">
 		 	<label for="comment">Content:</label>
 		 	<textarea class="form-control" rows="5" id="comment" name="question" placeholder="${questionModel.question}" disabled="disabled"></textarea>
 		 	</div>
		</div>
		
		
			<div class="form-group">
				<div class="col-md-12">
 		 	<label for="comment">Content:</label>
 		 	<textarea class="form-control" rows="5" id="comment" name="imageUrl"  src="${questionModel.image_url}" disabled="disabled"></textarea>
 		 	</div>
		</div>
		
		
			<button type="reset" data-dismiss="modal" class="btn btn-warning">Cancel </button> 
			<button type="submit" class="btn btn-warning">Save</button>
	</form>
</div>