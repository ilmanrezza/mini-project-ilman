<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${bootcamptesModel}" var="bootcamptesModel" varStatus="number">
	<tr>
		<td>${bootcamptesModel.name}</td>
		<td>${bootcamptesModel.createdBy.username}</td>
		<td>

			
			<div class="btn-group ">
    			<a type="button" href="#" class="glyphicon glyphicon-menu-hamburger" data-toggle="dropdown" style="color:black;"> </a>
   			 	<div class="dropdown-menu">
      			<button class="dropdown-item" value="${bootcamptesModel.id}" id="btn-edit" style=" background-color : white; border:none;">Edit</button><br>
     			<button class="dropdown-item" value="${bootcamptesModel.id}" id="btn-delete" style="border:none; background-color : white;">Delete</button>
    			</div>
  			</div>
		</td>
		
	</tr>
</c:forEach>