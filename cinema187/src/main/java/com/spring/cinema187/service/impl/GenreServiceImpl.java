package com.spring.cinema187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.cinema187.dao.GenreDao;
import com.spring.cinema187.model.GenreModel;
import com.spring.cinema187.service.GenreService;

@Service
@Transactional
public class GenreServiceImpl implements GenreService {

	@Autowired
	private GenreDao genreDao;

	@Override
	public void create(GenreModel genreModel) {
		this.genreDao.create(genreModel);
	}

	@Override
	public List<GenreModel> searchAll() {
		// TODO Auto-generated method stub
		return this.genreDao.searchAll();
	}

	@Override
	public GenreModel searchKode(String kodeGenre) {
		// TODO Auto-generated method stub
		return this.genreDao.searchKode(kodeGenre);
	}

	@Override
	public void update(GenreModel genreModel) {
		// TODO Auto-generated method stub
		this.genreDao.update(genreModel);

	}

	@Override
	public void delete(GenreModel genreModel) {
		// TODO Auto-generated method stub
		this.genreDao.delete(genreModel);
	
	}

	@Override
	public List<GenreModel> searchKodeLike(String kodeGenre) {
		// TODO Auto-generated method stub
		return this.genreDao.searchKodeLike(kodeGenre);
	}

	@Override
	public List<GenreModel> searchNamaLike(Object namaGenre) {
		// TODO Auto-generated method stub
		return this.genreDao.searchNamaLike(namaGenre);
	}


}
