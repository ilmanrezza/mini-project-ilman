package com.spring.cinema187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.cinema187.dao.FilmDao;
import com.spring.cinema187.model.FilmModel;
import com.spring.cinema187.service.FilmService;

@Service
@Transactional
public class FilmServiceImpl implements FilmService{

	@Autowired
	private FilmDao filmDao;
	
	@Override
	public void create(FilmModel filmModel) {
		// TODO Auto-generated method stub
		this.filmDao.create(filmModel);
	}

	@Override
	public List<FilmModel> searchAll() {
		// TODO Auto-generated method stub
		return this.filmDao.searchAll();
	}

	@Override
	public FilmModel searchKode(String kodeFilm) {
		// TODO Auto-generated method stub
		return this.filmDao.searchKode(kodeFilm);
	}

	@Override
	public void update(FilmModel filmModel) {
		// TODO Auto-generated method stub
		this.filmDao.update(filmModel);
	}

	@Override
	public void delete(FilmModel filmModel) {
		// TODO Auto-generated method stub
		this.filmDao.delete(filmModel);		
	}

}
