package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.ProduserDao;
import com.spring.cinema187.model.ProduserModel;

@Repository
public class ProduserDaoImpl implements ProduserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(ProduserModel produserModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(produserModel);
	}

	@Override
	public List<ProduserModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<ProduserModel> produserModelList = new ArrayList<ProduserModel>();
		String query = "from ProduserModel";
		String kondisi = " ";
		produserModelList = session.createQuery(query + kondisi).list(); // hql: select from kl di sql
		return produserModelList;
	}

	@Override
	public ProduserModel searchKode(String kodeProduser) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); // set start query

		ProduserModel produserModel = new ProduserModel();
		String query = "from ProduserModel";
		String kondisi = " where kodeProduser = '" + kodeProduser + "' "; // query
		produserModel = (ProduserModel) session.createQuery(query + kondisi).uniqueResult(); // hql: select from kl di
																								// sql
		return produserModel;
	}

	@Override
	public void update(ProduserModel produserModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(produserModel);

	}

	@Override
	public void delete(ProduserModel produserModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(produserModel);
		
	}
}
