package com.spring.cinema187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.cinema187.dao.ArtisDao;
import com.spring.cinema187.model.ArtisModel;
import com.spring.cinema187.service.ArtisService;

@Service
@Transactional
public class ArtisServiceImpl implements ArtisService{

	@Autowired
	private ArtisDao artisDao;

	@Override
	public void create(ArtisModel artisModel) {
		// TODO Auto-generated method stub
		this.artisDao.create(artisModel);
	}

	@Override
	public List<ArtisModel> searchAll() {
		// TODO Auto-generated method stub
		return this.artisDao.searchAll();
	}

	@Override
	public ArtisModel searchKode(String kodeArtis) {
		// TODO Auto-generated method stub
		return this.artisDao.searchKode(kodeArtis);
	}

	@Override
	public void update(ArtisModel artisModel) {
		// TODO Auto-generated method stub
		this.artisDao.update(artisModel);
	
	}

	@Override
	public void delete(ArtisModel artisModel) {
		// TODO Auto-generated method stub
		this.artisDao.delete(artisModel);
		
	}

	

}
