package com.spring.cinema187.service;

import java.util.List;

import com.spring.cinema187.model.ArtisModel;


public interface ArtisService {

	public void create(ArtisModel artisModel); // fungsi menyimpan data ke database

	public List<ArtisModel> searchAll();

	public ArtisModel searchKode(String kodeArtis);

	public void update(ArtisModel artisModel);

	public void delete(ArtisModel artisModel);

}