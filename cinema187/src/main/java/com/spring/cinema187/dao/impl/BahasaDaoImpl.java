package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.BahasaDao;
import com.spring.cinema187.model.BahasaModel;
import com.spring.cinema187.model.NegaraModel;

@Repository


public class BahasaDaoImpl implements BahasaDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<BahasaModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<BahasaModel> bahasaModelList = new ArrayList<BahasaModel>();
		String query = "from BahasaModel";
		String kondisi = " ";
		bahasaModelList = session.createQuery(query + kondisi).list(); //hql: select from kl di sql
		return bahasaModelList;
	}
	
	
	
}
