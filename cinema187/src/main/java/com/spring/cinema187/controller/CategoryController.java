package com.spring.cinema187.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.cinema187.model.CategoryModel;
import com.spring.cinema187.service.CategoryService;

@Controller
public class CategoryController extends UserController{
	
	@Autowired CategoryService categoryService;
	
	
	@RequestMapping(value = "category")
	public String category() {
		String jsp = "category/category";
		return jsp;
	}
	
	@RequestMapping(value="category/add") //url
	public String categoryAdd(Model model) {
		
		String kodecategoryGenerator = ""; //lempar backend ke front end
		Long jumlahKode = 0L;
		jumlahKode=this.categoryService.countKode() + 1;
		kodecategoryGenerator = "C000"+jumlahKode;
		model.addAttribute("kodecategoryGenerator", kodecategoryGenerator); //lempar backend ke front end
		
		String jsp="category/add";
		return jsp;
	}
		
	@RequestMapping(value="category/create")
	public String categoryCreate(HttpServletRequest request)
	{	//String id = request.getParameter(String.valueOf("id"));
		String code = request.getParameter("code"); 
		String name = request.getParameter("name");
		String deskription = request.getParameter("deskription");
		
		//dapatkan data dari front end
		CategoryModel categoryModel = new CategoryModel();
		//save
				Integer idCreatedBy = this.userSearch().getIdUser();
				categoryModel.setIdCreatedBy(idCreatedBy);
				categoryModel.setCreatedOn(new Date());
				
		
				categoryModel.setCode(code);
				categoryModel.setName(name);
				categoryModel.setDeskription(deskription);
				categoryModel.setIsDelete(false);
		this.categoryService.create(categoryModel);
		
		
		String jsp = "category/category";
		return jsp;
	}
	
	@RequestMapping(value ="category/list")
	public String bootcamptesList(Model model) {
		List<CategoryModel> categoryModel = new ArrayList<CategoryModel>();
		categoryModel = this.categoryService.searchAll();
		model.addAttribute("categoryModel", categoryModel);
		
		String jsp = "category/list";
		return jsp;
	}
	
	@RequestMapping(value="category/edit")
	public String categoryEdit(Model model, HttpServletRequest request) {
		String code = request.getParameter("code");
		CategoryModel categoryModel = new CategoryModel();
		categoryModel = this.categoryService.searchCode(code);
		model.addAttribute("categoryModel", categoryModel);
		String jsp = "category/edit";
		return jsp;
	}
	
	@RequestMapping(value = "category/update")
	public String categoryUpdate(Model model, HttpServletRequest request) { 
		String code = request.getParameter("code");
		String name = request.getParameter("name");
		String deskription = request.getParameter("description");
		
		CategoryModel categoryModel = new CategoryModel();
		categoryModel= this.categoryService.searchCode(code);
			
			//from user
			Integer idModifiedBy = this.userSearch().getIdUser();
			categoryModel.setIdModifiedBy(idModifiedBy);
			categoryModel.setModifiedOn(new Date());
		
		categoryModel.setName(name);
		categoryModel.setDeskription(deskription);
		this.categoryService.update(categoryModel);
				String jsp = "category/category";
				return jsp;
	}
	
	
	@RequestMapping(value="category/search")
	public String categorySearch(HttpServletRequest request, Model model) {
		String code = request.getParameter("codeOrname");
		String name = request.getParameter("codeOrname");
		List<CategoryModel> categoryModel = new ArrayList<CategoryModel>();
		categoryModel = this.categoryService.searchCodeOrName(code, name);
		model.addAttribute("categoryModel", categoryModel);
		String jsp = "category/list";
		return jsp;
	}
	
	@RequestMapping(value="category/remove")
	public String categoryRemove(Model model, HttpServletRequest request) {
		String code = request.getParameter("code");
		CategoryModel categoryModel = new CategoryModel();
		categoryModel = this.categoryService.searchCode(code);
		model.addAttribute("categoryModel", categoryModel);
		String jsp = "category/remove";
		return jsp;
	}
	
	@RequestMapping(value = "category/delete")
	public String categoryDelete(HttpServletRequest request) { 
		String code = request.getParameter("code");
		
		CategoryModel categoryModel = new CategoryModel();
		categoryModel= this.categoryService.searchCode(code);
			
			//from user
			Integer idDeleteBy = this.userSearch().getIdUser();
			categoryModel.setIdDeletedBy(idDeleteBy);
			categoryModel.setDeletedOn(new Date());
		
			categoryModel.setIsDelete(true);
			
		this.categoryService.deleteTemporary(categoryModel);
				String jsp = "category/category";
				return jsp;
	}
}
