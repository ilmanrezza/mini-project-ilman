package com.spring.cinema187.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity //menandakan entitas
@Table(name = "T_ARTIS") //harusnya M atau T_Artis utk tabel master
public class ArtisModel {
	private String kodeArtis; //nama variabel pada framework/ciri-ciri (penyamaan penulisan pada codingan)
	private String namaArtis;
	private String jenisKelamin;
	private Integer bayaran;
	private Integer award;
	private String negara;
	
	@Id //unique
	@Column(name="KD_ARTIS")
	public String getKodeArtis() {
		return kodeArtis;
	}
	public void setKodeArtis(String kodeArtis) {
		this.kodeArtis = kodeArtis;
	}
	
	@Column(name="NM_ARTIS")
	public String getNamaArtis() {
		return namaArtis;
	}
	public void setNamaArtis(String namaArtis) {
		this.namaArtis = namaArtis;
	}
	
	@Column(name="JK")
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	
	@Column(name="BAYARAN")
	public Integer getBayaran() {
		return bayaran;
	}
	public void setBayaran(Integer bayaran) {
		this.bayaran = bayaran;
	}
	
	@Column(name="AWARD")
	public Integer getAward() {
		return award;
	}
	public void setAward(Integer award) {
		this.award = award;
	}
	
	@Column(name="NEGARA")
	public String getNegara() {
		return negara;
	}
	public void setNegara(String negara) {
		this.negara = negara;
	}
	
	
}
