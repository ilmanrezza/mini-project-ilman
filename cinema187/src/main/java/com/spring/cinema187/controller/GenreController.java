package com.spring.cinema187.controller;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.cinema187.model.GenreModel;
import com.spring.cinema187.model.NegaraModel;
import com.spring.cinema187.service.GenreService;
import com.spring.cinema187.service.NegaraService;


@Controller
public class GenreController {
	
	@Autowired
	private GenreService genreService;

	@Autowired
	private NegaraService negaraService;
	
	@RequestMapping(value = "genre")
	public String genre() {
		String jsp = "genre/genre";
		return jsp;
	}
	
	@RequestMapping(value="genre/add") //url
	public String genreAdd(Model model) {
		List<NegaraModel> negaraModelList = new ArrayList<NegaraModel>();
		negaraModelList = this.negaraService.searchAll();
		model.addAttribute("negaraModelList", negaraModelList);
		
		String jsp="genre/add";
		return jsp;
	}
	
	@RequestMapping(value="genre/create")
	public String genreCreate(HttpServletRequest request) {
		String kodeGenre = request.getParameter("kodeGenre");
		String namaGenre = request.getParameter("namaGenre");
		
		GenreModel genreModel = new GenreModel();
		genreModel.setKodeGenre(kodeGenre);
		genreModel.setNamaGenre(namaGenre);
		this.genreService.create(genreModel);
		
		String jsp = "genre/genre";
		return jsp;
		
	}
	
	@RequestMapping(value="genre/list")
	public String genreList(Model model) {
		List<GenreModel> genreModelList = new ArrayList<GenreModel>();
		genreModelList = this.genreService.searchAll();
		model.addAttribute("genreModelList", genreModelList);
		String jsp = "genre/list";
		return jsp;
	}

	@RequestMapping(value="genre/edit")
	public String genreEdit(Model model, HttpServletRequest request) {
		String kodeGenre = request.getParameter("kodeGenre");
		GenreModel genreModel = new GenreModel();
		genreModel = this.genreService.searchKode(kodeGenre);
		
		model.addAttribute("genreModel", genreModel); 			//backend ke frontend, taro di edit.
		String jsp = "genre/edit";
		return jsp;
	}
	
	@RequestMapping(value="genre/update")
	public String genreUpdate(Model model, HttpServletRequest request) {
		String kodeGenre = request.getParameter("kodeGenre");
		String namaGenre = request.getParameter("namaGenre");
		
		GenreModel genreModel = new GenreModel();
		genreModel = this.genreService.searchKode(kodeGenre);
		genreModel.setNamaGenre(namaGenre); //set
		
		this.genreService.update(genreModel);
		
		String jsp = "genre/genre";
		return jsp;
	}
	
	@RequestMapping(value="genre/detail")
	public String genreDetail(Model model, HttpServletRequest request) {
		String kodeGenre = request.getParameter("kodeGenre");
		GenreModel genreModel = new GenreModel();
		genreModel = this.genreService.searchKode(kodeGenre);
		model.addAttribute("genreModel", genreModel);
		
		String jsp = "genre/detail";
		return jsp;
	}
	
	@RequestMapping(value="genre/remove")
	public String genreRemove(Model model, HttpServletRequest request) {
		String kodeGenre = request.getParameter("kodeGenre");
		GenreModel genreModel = new GenreModel();
		genreModel = this.genreService.searchKode(kodeGenre);
		model.addAttribute("genreModel", genreModel);
		
		String jsp = "genre/remove";
		return jsp;
	}

	@RequestMapping(value="genre/delete")
	public String genreDelete(Model model, HttpServletRequest request) {
		String kodeGenre = request.getParameter("kodeGenre");
		String namaGenre = request.getParameter("namaGenre");
		
		GenreModel genreModel = new GenreModel();
		genreModel = this.genreService.searchKode(kodeGenre);
		
		this.genreService.delete(genreModel);
		
		String jsp = "genre/genre";
		return jsp;
	}

	@RequestMapping(value="genre/search/kode")
	public String genreSearchKode(Model model, HttpServletRequest request) {
		String kodeGenre = request.getParameter("kodeGenre");
		List<GenreModel> genreModelList = new ArrayList<GenreModel>();
		genreModelList = this.genreService.searchKodeLike(kodeGenre);
		model.addAttribute("genreModelList", genreModelList);
		
		String jsp = "genre/list";
		return jsp;
	}

	@RequestMapping(value="genre/search/nama")
	public String genreSearchNama(Model model, HttpServletRequest request) {
		String namaGenre = request.getParameter("namaGenre");
		List<GenreModel> genreModelList = new ArrayList<GenreModel>();
		genreModelList = this.genreService.searchNamaLike(namaGenre);
		model.addAttribute("genreModelList", genreModelList);
		
		String jsp = "genre/list";
		return jsp;
	}

	
}
