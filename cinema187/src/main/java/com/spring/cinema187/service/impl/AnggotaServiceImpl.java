package com.spring.cinema187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.cinema187.dao.AnggotaDao;
import com.spring.cinema187.model.AnggotaModel;
import com.spring.cinema187.service.AnggotaService;

@Service
@Transactional
public class AnggotaServiceImpl implements AnggotaService{

	@Autowired
	private AnggotaDao anggotaDao;

	@Override
	public void create(AnggotaModel anggotaModel) {
		// TODO Auto-generated method stub
		this.anggotaDao.create(anggotaModel);
	}

	@Override
	public List<AnggotaModel> searchAll() {
		// TODO Auto-generated method stub
		return this.anggotaDao.searchAll();
	}
	
}
