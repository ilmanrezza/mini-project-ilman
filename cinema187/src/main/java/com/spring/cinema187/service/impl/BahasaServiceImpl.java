package com.spring.cinema187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.cinema187.dao.BahasaDao;
import com.spring.cinema187.model.BahasaModel;
import com.spring.cinema187.service.BahasaService;

@Service
@Transactional
public class BahasaServiceImpl implements BahasaService {

	@Autowired
	private BahasaDao bahasaDao;
	
	@Override
	public List<BahasaModel> searchAll() {
		// TODO Auto-generated method stub
		return this.bahasaDao.searchAll();
	}

}
