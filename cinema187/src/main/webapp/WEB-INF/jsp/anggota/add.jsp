<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Tambah Anggota</h1>
	<form action="#" method="get" id="form-anggota-add">
		<table>
			<tr>
				<td>ID Anggota</td>
				<td><input type="text" id="idAnggota" name="idAnggota"/></td>
			</tr>
			<tr>
				<td>Kode Anggota</td>
				<td><input type="text" id="kodeAnggota" name="kodeAnggota"/></td>
			</tr>
			<tr>
				<td>Nama Anggota</td>
				<td><input type="text" id="namaAnggota" name="namaAnggota"/></td>
			</tr>
			<tr>
				<td>Usia</td>
				<td><input type="text" id="usia" name="usia"/></td>
			</tr>
			<tr>
		</table>
		<button type="submit" onclick="validasi();">Simpan</button> 
	</form>
</div>

<script type = "text/javascript">
	function validasi() {
		var idAnggota = document.getElementById("idAnggota");
		if (idAnggota.value == "") {
			idAnggota.style.borderColor = "red";
		} else {
			idAnggota.style.borderColor = "gray";
		}
		
		var kodeAnggota = document.getElementById("kodeAnggota");
		if (kodeAnggota.value == "") {
			kodeAnggota.style.borderColor = "red";
		} else {
			kodeAnggota.style.borderColor = "gray";
		}
		
		var namaAnggota = document.getElementById("namaAnggota");
		if (namaAnggota.value == "") {
			namaAnggota.style.borderColor = "red";
		} else {
			namaAnggota.style.borderColor = "gray";
		}
		
		var usia = document.getElementById("usia");
		if (usia.value == "") {
			usia.style.borderColor = "red";
		} else {
			usia.style.borderColor = "gray";
		}
		

	}
</script>