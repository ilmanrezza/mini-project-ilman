<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Category</h1>

	<div>
		<table class="table" id="tbl-category">
			<tr>
				<td><form action="#" method="get" id="form-category-search">
						<input type="text" id="codeOrname" name="codeOrname" placeholder="Search by code/name" style="height:30px;width:300px;"/>
						<button type="submit" class="btn btn-warning">
						<span class="glyphicon glyphicon-search"></span> Search
						</button>
						</form>
						</td> 
				<td></td> 
				<td><button type="button"  id="btn-add" style="border:5; background-color : orange;">
				<span class="glyphicon glyphicon-plus"></span></button>
				<td></td>
	
			</tr>
			<tr>
				<td style="font-weight: bold;">CODE</td>
				<td style="font-weight: bold;">NAME</td>
				<td> <span class="glyphicon glyphicon-th-list"></span></td>
			</tr>
			<tbody id="list-category">

			</tbody>
		</table>
	</div>

</div>

<!-- syntax popup add -->
<div id="modal-input-add" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color : orange;">
				
				<h1  style="font-weight: bold;">Add Category</h1>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
			<div>
			
			</div>
			
		</div>
	</div>
</div>

<!-- syntax popup edit -->
<div id="modal-input-edit" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color : orange;">
				
				<h1  style="font-weight: bold;">Edit Category</h1>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
			<div>
			
			</div>
			
		</div>
	</div>
</div>


<!-- syntax popup remove -->
<div id="modal-input-remove" class="modal fade">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="background-color : orange;">
				
				<h1  style="font-weight: bold;">Delete</h1>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
			<div>
			
			</div>
			
		</div>
	</div>
</div>

<script type="text/javascript">

loadListCategory();

function loadListCategory() {
	$.ajax({
		url : 'category/list.html',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#list-category').html(result);
		}
	});
}

//fungsi ajax untuk popup tambah
$('#btn-add').on('click', function() {
	$.ajax({
		url : 'category/add.html',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#modal-input-add').find('.modal-body').html(result);
			$('#modal-input-add').modal('show');
		}
	});
});
//akhir fungsi ajax untuk popup tambah

	//fungsi ajax untuk create tambah
	$('#modal-input-add').on('submit', '#form-category-add', function() {
		$.ajax({
			url : 'category/create.json',
			type : 'get',
			dataType : 'json',
			data : $(this).serialize(),
			success : function(result) {
				
				$('#modal-input-add').modal('hide');
				alert("data telah ditambah!");
				loadListCategory();
			}
		});
		return false;
	});
	//akhir fungsi ajax untuk create tambah
	
		//fungsi ajax untuk popup edit
		$('#list-category').on('click','#btn-edit',function(){
			var code = $(this).val();
			$.ajax({
				url : 'category/edit.html',
				type : 'get',
				dataType : 'html',
				data:{code:code},
				success : function(result) {
					$('#modal-input-edit').find('.modal-body').html(result);
					$('#modal-input-edit').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit
		
		
		//fungsi ajax untuk update
		$('#modal-input-edit').on('submit','#form-category-edit',function(){
			
			$.ajax({
				url:'category/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input-edit').modal('hide');
					alert("data telah diubah!");
					loadListCategory();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
		
				//fungsi ajax untuk search 
		$('#form-category-search').on('submit',function(){
			$.ajax({
				url:'category/search.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data berhasil dicari !");
					$('#list-category').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search 
		
				//fungsi ajax untuk hapus
		$('#list-category').on('click','#btn-delete',function(){
			var code = $(this).val();
			$.ajax({
				url:'category/remove.html',
				type:'get',
				dataType:'html',
				data:{code:code},
				success: function(result){
					$('#modal-input-remove').find('.modal-body').html(result);
					$('#modal-input-remove').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk hapus
		
		//fungsi ajax untuk delete
		$('#modal-input-remove').on('submit','#form-category-delete',function(){
			
			$.ajax({
				url:'category/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input-remove').modal('hide');
					alert("data telah dihapus!");
					loadListCategory();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
		
</script>