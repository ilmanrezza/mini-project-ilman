package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.ArtisModel;


public interface ArtisDao {

	public void create(ArtisModel artisModel);

	public List<ArtisModel> searchAll();

	public ArtisModel searchKode(String kodeArtis);

	public void update(ArtisModel artisModel);
	
	public void delete(ArtisModel artisModel);


}
