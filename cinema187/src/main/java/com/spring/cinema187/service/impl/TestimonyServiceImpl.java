package com.spring.cinema187.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.cinema187.dao.TestimonyDao;
import com.spring.cinema187.model.TestimonyModel;
import com.spring.cinema187.service.TestimonyService;

@Service
@Transactional
public class TestimonyServiceImpl implements TestimonyService{

	@Autowired
	private TestimonyDao testimonyDao;
	
	@Override
	public void create(TestimonyModel testimonyModel) {
		// TODO Auto-generated method stub
		this.testimonyDao.create(testimonyModel);
	}

	@Override
	public void update(TestimonyModel testimonyModel) {
		// TODO Auto-generated method stub
		this.testimonyDao.update(testimonyModel);
	}

	@Override
	public List<TestimonyModel> searchAll() {
		// TODO Auto-generated method stub
		return this.testimonyDao.searchAll();
	}

	@Override
	public TestimonyModel searchId(Integer id) {
		// TODO Auto-generated method stub
		return this.testimonyDao.searchId(id);
	}

	@Override
	public void deleteTemporary(TestimonyModel testimonyModel) {
		// TODO Auto-generated method stub
		this.testimonyDao.update(testimonyModel);
	}

	@Override
	public List<TestimonyModel> searchTitle(String title) {
		// TODO Auto-generated method stub
		return this.testimonyDao.searchTitle(title);
	}

}
