<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Genre</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah
		Genre</button>

	<div>
		<table class="table" id="tbl-genre">
			<tr>
				<td>1</td>
				<td>
					<form action="#" method="get" id="form-genre-search-kode">
						<input type="text" id="kodeNegara" name="kodeGenre"/>
						<button type="submit">Cari Kode Genre</button>
					</form></td> 
				<td>
					<form action="#" method="get" id="form-genre-search-nama">
						<input type="text" id="namaGenre" name="namaGenre"/>
						<button type="submit">Cari Nama Genre</button>
					</form></td>
				<td>4</td>
	
			</tr>
			<tr>
				<td>No</td>
				<td>Kode Genre</td>
				<td>Nama Genre</td>
				<td>Aksi</td>
			</tr>
			<tbody id="list-genre">

			</tbody>
		</table>
	</div>

</div>


<!-- syntax popup -->
<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" >
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Genre</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	loadListGenre();

	function loadListGenre() {
		$.ajax({
			url : 'genre/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-genre').html(result);
			}
		});
	}

	//document ready itu setelah halamannya dipanggil
	$(document).ready(function() {

		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'genre/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah

		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit', '#form-genre-add', function() {
			$.ajax({
				url : 'genre/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {

					$('#modal-input').modal('hide');
					alert("data telah ditambah!");
					
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah

		//fungsi ajax untuk popup edit
		$('#list-genre').on('click','#btn-edit',function(){
			var kodeGenre = $(this).val();
			$.ajax({
				url : 'genre/edit.html',
				type : 'get',
				dataType : 'html',
				data:{kodeGenre:kodeGenre},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit

		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-genre-edit',function(){
			
			$.ajax({
				url:'genre/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah diubah!");
					loadListGenre();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update

		//fungsi ajax untuk popup lihat
		$('#list-genre').on('click','#btn-detail',function(){
			var kodeGenre = $(this).val();
			$.ajax({
				url : 'genre/detail.html',
				type : 'get',
				dataType : 'html',
				data:{kodeGenre:kodeGenre},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup lihat

		//fungsi ajax untuk popup hapus
		$('#list-genre').on('click','#btn-delete',function(){
			var kodeGenre = $(this).val();
			$.ajax({
				url : 'genre/remove.html',
				type : 'get',
				dataType : 'html',
				data:{kodeGenre:kodeGenre},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup hapus
		
		//fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-genre-delete',function(){
			
			$.ajax({
				url:'genre/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihapus!");
					loadListGenre();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete

		//fungsi ajax untuk search kode
		$('#form-genre-search-kode').on('submit',function(){
			$.ajax({
				url:'genre/search/kode.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode!");
					$('#list-genre').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search kode

		//fungsi ajax untuk search nama
		$('#form-genre-search-nama').on('submit',function(){
			$.ajax({
				url:'genre/search/nama.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan nama!");
					$('#list-genre').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search nama

		
	});
	//akhir dokumen ready
</script>

