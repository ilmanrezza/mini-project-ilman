<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Lihat Negara</h1>
	<form action="#" method="get" id="form-negara-detail">
		<table>
			<tr>
				<td>Kode Negara</td>
				<td><input type="text" id="kodeNegara" name="kodeNegara" value = "${negaraModel.kodeNegara}"/></td>
			</tr>
			<tr>
				<td>Nama Negara</td>
				<td><input type="text" id="namaNegara"name="namaNegara" value = "${negaraModel.namaNegara}"/></td>
			</tr>
			<tr>
				<td>Nama Genre</td>
				<td><input type="text" id="kodeGenre"name="kodeGenre" value = "${negaraModel.genreModel.namaGenre}"/></td>
			</tr>
			<tr>
				<td>Nama Bahasa</td>
				<td><input type="text" id="idBahasa"name="idBahasa" value = "${negaraModel.bahasaModel.namaBahasa}"/></td>
			</tr>

		</table>
	</form>
</div>

<script type = "text/javascript">

</script>