package com.spring.cinema187.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.cinema187.model.MahasiswaModel;
import com.spring.cinema187.service.MahasiswaService;
import com.spring.cinema187.service.SequenceService;

@Controller
public class MahasiswaController {
	
	//Koneksi ke service
	@Autowired
	private MahasiswaService mahasiswaService;
	
	@Autowired
	private SequenceService sequenceService;
	
	@RequestMapping(value="mahasiswa") // uri action
	public String mahasiswa() throws Exception { // mahasiswa() --> nama methodnya
		
		String jsp = "mahasiswa";
		return jsp; // panggil jspnya
	}
	
	@RequestMapping(value="mahasiswa/tambah") 
	public String mahasiswaTambah(Model model) throws Exception {
		String xsis = "Bootcamp itu ibadah";
		model.addAttribute("xsis", xsis);
		
		String nimMahasiswaOtomatis = null;
		nimMahasiswaOtomatis = this.nimMahasiswaGenerator();
		model.addAttribute("nimMahasiswaOtomatis", nimMahasiswaOtomatis);
		
		String jsp = "mahasiswa/add";
		return jsp; 
	}
	
	@RequestMapping(value="mahasiswa/save_add") 
	public String mahasiswaSaveAdd(HttpServletRequest request, Model model) throws Exception { 
		// HttpServletRequest request -> ambil nilai dari jsp (frontEnd)
		String nimMahasiswa = request.getParameter("nimMahasiswa");
		String namaMahasiswa = request.getParameter("namaMahasiswa");
		String teleponMahasiswa = request.getParameter("teleponMahasiswa");
		String genderMahasiswa = request.getParameter("genderMahasiswa");
		String alamatMahasiswa = request.getParameter("alamatMahasiswa");
		Integer tahunLahirMahasiswa = Integer.valueOf(request.getParameter("tahunLahirMahasiswa"));
		
		//encapsulate nilai ke modelnya
		MahasiswaModel mahasiswaModel = new MahasiswaModel(); // instance
		mahasiswaModel.setNimMahasiswa(nimMahasiswa);
		mahasiswaModel.setNamaMahasiswa(namaMahasiswa);
		mahasiswaModel.setTeleponMahasiswa(teleponMahasiswa);
		mahasiswaModel.setGenderMahasiswa(genderMahasiswa);
		mahasiswaModel.setAlamatMahasiswa(alamatMahasiswa);
		mahasiswaModel.setTahunLahirMahasiswa(tahunLahirMahasiswa);
		
		//cekNoTelepon dahulu
		// 1 Jika data single
		MahasiswaModel mahasiswaModelDB = new MahasiswaModel();
		mahasiswaModelDB = this.cekTeleponDataSingle(teleponMahasiswa);
		String teleponSama = "tidak";
		if (mahasiswaModelDB!=null) {
			//jika tidak nul berarti ada noTelepon yg sama di DB
			//artinya boleh create data
			teleponSama = "ya";
			
		}else {
			// ini boleh create data krna g da notelepon yg sama di DB
			// proses insert datanya
			teleponSama = "tidak";
			this.mahasiswaService.create(mahasiswaModel);
			
		}
		
		model.addAttribute("teleponSama", teleponSama);
		model.addAttribute("teleponMahasiswa", teleponMahasiswa);
		
		String programmer = "Sir William";
		model.addAttribute("mahasiswaModel", mahasiswaModel);
		model.addAttribute("programmer", programmer);
		String jsp = "mahasiswa";
		return jsp; 
	}
	
	
	@RequestMapping(value="mahasiswa/list") 
	public String mahasiswaList(Model model) throws Exception { 
		// Model model itu utk send data dari backend ke frontend
		List<MahasiswaModel> mahasiswaModelList = new ArrayList<MahasiswaModel>();
		mahasiswaModelList = this.mahasiswaService.list();
		model.addAttribute("mahasiswaModelList", mahasiswaModelList);		
		String jsp = "mahasiswa/list";
		return jsp; 
	}
	
	@RequestMapping(value="mahasiswa/detil") 
	public String mahasiswaDetil(HttpServletRequest request, Model model) throws Exception{ 
		Integer idMahasiswa = Integer.valueOf(request.getParameter("idMahasiswa"));
		MahasiswaModel mahasiswaModel = new MahasiswaModel();
		mahasiswaModel = this.mahasiswaService.searchById(idMahasiswa);
		model.addAttribute("mahasiswaModel", mahasiswaModel);
		String jsp = "mahasiswa/detail";
		return jsp; 
	}
	
	
	@RequestMapping(value="mahasiswa/hapus") 
	public String mahasiswaHapus(HttpServletRequest request, Model model) throws Exception{ 
		Integer idMahasiswa = Integer.valueOf(request.getParameter("idMahasiswa"));
		MahasiswaModel mahasiswaModel = new MahasiswaModel();
		mahasiswaModel = this.mahasiswaService.searchById(idMahasiswa);
		model.addAttribute("mahasiswaModel", mahasiswaModel);
		String jsp = "mahasiswa/delete";
		return jsp; 
	}
	
	@RequestMapping(value="mahasiswa/save_delete") 
	public String mahasiswaSaveDelete(HttpServletRequest request) throws Exception { 
		// HttpServletRequest request -> ambil nilai dari jsp (frontEnd)
		Integer idMahasiswa = Integer.valueOf(request.getParameter("idMahasiswa"));
		
		//cari data modelnya yg akan dihapus dari Database
		MahasiswaModel mahasiswaModel = new MahasiswaModel(); // instance
		mahasiswaModel = this.mahasiswaService.searchById(idMahasiswa);
		
		// proses delete datanya
		this.mahasiswaService.delete(mahasiswaModel);
		
		String jsp = "mahasiswa";
		return jsp; 
	}
	
	@RequestMapping(value="mahasiswa/ubah") 
	public String mahasiswaUbah(HttpServletRequest request, Model model) throws Exception{ 
		Integer idMahasiswa = Integer.valueOf(request.getParameter("idMahasiswa"));
		MahasiswaModel mahasiswaModel = new MahasiswaModel();
		mahasiswaModel = this.mahasiswaService.searchById(idMahasiswa);
		model.addAttribute("mahasiswaModel", mahasiswaModel);
		String jsp = "mahasiswa/edit";
		return jsp; 
	}
	
	@RequestMapping(value="mahasiswa/save_edit") 
	public String mahasiswaSaveEdit(HttpServletRequest request) throws Exception { 
		// HttpServletRequest request -> ambil nilai dari jsp (frontEnd)
		Integer idMahasiswa = Integer.valueOf(request.getParameter("idMahasiswa"));
		
		String nimMahasiswa = request.getParameter("nimMahasiswa");
		String namaMahasiswa = request.getParameter("namaMahasiswa");
		String teleponMahasiswa = request.getParameter("teleponMahasiswa");
		String genderMahasiswa = request.getParameter("genderMahasiswa");
		String alamatMahasiswa = request.getParameter("alamatMahasiswa");
		Integer tahunLahirMahasiswa = Integer.valueOf(request.getParameter("tahunLahirMahasiswa"));
		
		//cari data modelnya dari database berdasarkan idMahasiswa
		MahasiswaModel mahasiswaModel = new MahasiswaModel(); // instance
		mahasiswaModel = this.mahasiswaService.searchById(idMahasiswa); //cari data ubah

		mahasiswaModel.setNimMahasiswa(nimMahasiswa);
		mahasiswaModel.setNamaMahasiswa(namaMahasiswa);
		mahasiswaModel.setTeleponMahasiswa(teleponMahasiswa);
		mahasiswaModel.setGenderMahasiswa(genderMahasiswa);
		mahasiswaModel.setAlamatMahasiswa(alamatMahasiswa);
		mahasiswaModel.setTahunLahirMahasiswa(tahunLahirMahasiswa);
		
		// proses update datanya
		this.mahasiswaService.update(mahasiswaModel);
		
		String jsp = "mahasiswa";
		return jsp; 
	}
	
	@RequestMapping(value="mahasiswa/search_nim") 
	public String mahasiswaSearchNim(HttpServletRequest request, Model model) throws Exception { 
		// HttpServletRequest request -> ambil nilai dari jsp (frontEnd)
		String nimMahasiswa = request.getParameter("keywordNim");
		
		//cari data modelnya dari database berdasarkan nimMahasiswa
		List<MahasiswaModel> mahasiswaModelList = new ArrayList<MahasiswaModel>();
		mahasiswaModelList = this.mahasiswaService.searchByNim(nimMahasiswa);
		model.addAttribute("mahasiswaModelList", mahasiswaModelList);
		String jsp = "mahasiswa/list";
		return jsp; 
	}
	
	public String nimMahasiswaGenerator() throws Exception{
		Integer idMahasiswaSequence = 0;
		String sequenceName = "M_MAHASISWA_ID";
		idMahasiswaSequence = this.sequenceService.searchForIdMahasiswaSeq(sequenceName);
		
		String nimMahasiswaOtomatis = null;
		if (idMahasiswaSequence < 10 ) {
			nimMahasiswaOtomatis = "NIM000" + idMahasiswaSequence;
			
		}else if (idMahasiswaSequence >= 10 && idMahasiswaSequence < 100) {
			nimMahasiswaOtomatis = "NIM00" + idMahasiswaSequence;
			
		}else if (idMahasiswaSequence >= 100 && idMahasiswaSequence < 1000) {
			nimMahasiswaOtomatis = "NIM0" + idMahasiswaSequence;
			
		}else if (idMahasiswaSequence >= 1000) {
			nimMahasiswaOtomatis = "NIM" + idMahasiswaSequence;
			
		}else {
			
		}
		
		return nimMahasiswaOtomatis;
	}
	
	public MahasiswaModel cekTeleponDataSingle(String teleponMahasiswa) {
		MahasiswaModel mahasiswaModel = new MahasiswaModel();
		mahasiswaModel = this.mahasiswaService.searchByTeleponSingle(teleponMahasiswa);
		return mahasiswaModel;
	}
	
	public List<MahasiswaModel> cekTeleponDataList(String teleponMahasiswa) {
		List<MahasiswaModel> mahasiswaModelList = new ArrayList<MahasiswaModel>();
		mahasiswaModelList = this.mahasiswaService.searchByTeleponList(teleponMahasiswa);
		return mahasiswaModelList;
	}
}
