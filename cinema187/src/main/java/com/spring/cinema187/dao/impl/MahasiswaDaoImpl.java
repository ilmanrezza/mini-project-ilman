package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.MahasiswaDao;
import com.spring.cinema187.model.MahasiswaModel;

@Repository
public class MahasiswaDaoImpl implements MahasiswaDao {

	//koneksi ke applicatioContext (Modelnya)
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(MahasiswaModel mahasiswaModel) {
		// TODO Auto-generated method stub
		
		Session session = this.sessionFactory.getCurrentSession(); // start compile query
		session.save(mahasiswaModel);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MahasiswaModel> list() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MahasiswaModel> mahasiswaModelList = new ArrayList<MahasiswaModel>();
		mahasiswaModelList = session.createQuery(" from MahasiswaModel ").list();
		return mahasiswaModelList;
	}

	@Override
	public MahasiswaModel searchById(Integer idMahasiswa) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		MahasiswaModel mahasiswaModel = new MahasiswaModel();
		mahasiswaModel = (MahasiswaModel) session.createQuery(" from MahasiswaModel where idMahasiswa="+idMahasiswa+" ").getSingleResult();
		
		return mahasiswaModel;
	}

	@Override
	public void delete(MahasiswaModel mahasiswaModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(mahasiswaModel);
	}

	@Override
	public void update(MahasiswaModel mahasiswaModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(mahasiswaModel);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MahasiswaModel> searchByNim(String nimMahasiswa) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MahasiswaModel> mahasiswaModelList = new ArrayList<MahasiswaModel>();
		mahasiswaModelList = session.createQuery(" from MahasiswaModel where nimMahasiswa like '%"+nimMahasiswa+"%' ").list();
		return mahasiswaModelList;
	}

	@Override
	public MahasiswaModel searchByNamaPasti(String namaMahasiswa) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		MahasiswaModel mahasiswaModel = new MahasiswaModel();
		mahasiswaModel = (MahasiswaModel) session.createQuery(" from MahasiswaModel where namaMahasiswa='"+namaMahasiswa+"' ").getSingleResult();
		return mahasiswaModel;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MahasiswaModel> searchByNamaLikeDepan(String namaMahasiswa) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MahasiswaModel> mahasiswaModelList = new ArrayList<MahasiswaModel>();
		mahasiswaModelList =  session.createQuery(" from MahasiswaModel where namaMahasiswa like '"+namaMahasiswa+"%' ").list();
		return mahasiswaModelList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MahasiswaModel> searchByNamaLikeAkhir(String namaMahasiswa) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MahasiswaModel> mahasiswaModelList = new ArrayList<MahasiswaModel>();
		mahasiswaModelList =  session.createQuery(" from MahasiswaModel where namaMahasiswa like '%"+namaMahasiswa+"' ").list();
		return mahasiswaModelList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MahasiswaModel> searchByNamaLikeOrTahunLahir(String namaMahasiswa, Integer tahunLahirMahasiswa) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MahasiswaModel> mahasiswaModelList = new ArrayList<MahasiswaModel>();
		mahasiswaModelList =  session.createQuery(" from MahasiswaModel "
												+ " where namaMahasiswa like '%"+namaMahasiswa+"%' "
												+ " or tahunLahirMahasiswa="+tahunLahirMahasiswa+" ").list();
		return mahasiswaModelList;
	}

	@Override
	public MahasiswaModel searchByTeleponSingle(String teleponMahasiswa) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		MahasiswaModel mahasiswaModel = new MahasiswaModel();
		try {
			mahasiswaModel = (MahasiswaModel) session.createQuery(" from MahasiswaModel where teleponMahasiswa='"+teleponMahasiswa+"' ").getSingleResult();
			
		} catch (Exception e) {
			// TODO: handle exception
			mahasiswaModel = null;
		}
		
		
		return mahasiswaModel;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MahasiswaModel> searchByTeleponList(String teleponMahasiswa) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MahasiswaModel> mahasiswaModelList = new ArrayList<MahasiswaModel>();
		mahasiswaModelList = session.createQuery(" from MahasiswaModel where teleponMahasiswa='"+teleponMahasiswa+"' ").list();
		return mahasiswaModelList;
	}

}
