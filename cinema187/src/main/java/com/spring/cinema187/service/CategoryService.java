package com.spring.cinema187.service;

import java.util.List;

import com.spring.cinema187.model.CategoryModel;


public interface CategoryService {
	public void create(CategoryModel categoryModel);
	public List<CategoryModel> searchAll();
	public Long countKode();
	public void update(CategoryModel categoryModel);
	public CategoryModel searchCode(String code);
	
	public List<CategoryModel> searchCodeOrName(String code, String name);
	public void deleteTemporary(CategoryModel categoryModel);
}
