package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.DivisiModel;

public interface DivisiDao {

	void create(DivisiModel divisiModel);
	
	public List<DivisiModel> searchAll();

	public DivisiModel searchKode(String idDivisi);

	public void update(DivisiModel divisiModel);




}
