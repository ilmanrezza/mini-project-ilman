package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.FakultasDao;
import com.spring.cinema187.model.FakultasModel;

@Repository
public class FakultasDaoImpl implements FakultasDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(FakultasModel fakultasModel) {
		// TODO Auto-generated method stub
		
		Session session = this.sessionFactory.getCurrentSession();
		session.save(fakultasModel);
		
	}

	@Override
	public void update(FakultasModel fakultasModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(fakultasModel);
		
	}

	@Override
	public void delete(FakultasModel fakultasModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(fakultasModel);
	}

	@Override
	public List<FakultasModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		String query = " from FakultasModel ";
		String kondisi = " where isDelete != 1 ";
		
		if (kodeRole.equals("ROLE_ADMIN")) {
			kondisi = " ";
		} else {

		}
		
		fakultasModelList = session.createQuery(query+kondisi).list();
		
		return fakultasModelList;
	}

	@Override
	public FakultasModel searchId(Integer idFakultas) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		FakultasModel fakultasModel = new FakultasModel();
		String query = " from FakultasModel where idFakultas="+idFakultas+" ";
		fakultasModel =  (FakultasModel) session.createQuery(query).uniqueResult();
		
		return fakultasModel;
	}

	@Override
	public List<FakultasModel> searchNama(String namaFakultas) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //sintaks start queri
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>(); //instance return
		String query = " from FakultasModel where namaFakultas like '%"+namaFakultas+"%'   "; //queri
		fakultasModelList = session.createQuery(query).list(); //hibernate
	
		return fakultasModelList;
	}

	@Override
	public List<FakultasModel> searchKode(String kodeFakultas) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //sintaks start queri
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>(); //instance return
		String query = " from FakultasModel where kodeFakultas like '%"+kodeFakultas+"%'   "; //queri
		fakultasModelList = session.createQuery(query).list(); //hibernate
	
		return fakultasModelList;
	}

	@Override
	public List<FakultasModel> searchKodeOrNama(String kodeFakultas, String namaFakultas) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		String query = " from FakultasModel "
				     + " where kodeFakultas like '%"+kodeFakultas+"%' "
				     + " or namaFakultas like '%"+namaFakultas+"%' ";
		fakultasModelList = session.createQuery(query).list();
		return fakultasModelList;
	}

	@Override
	public List<FakultasModel> searchKodeEqual(String kodeFakultas) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //sintaks start queri
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>(); //instance return
		String query = " from FakultasModel where kodeFakultas='"+kodeFakultas+"' "; //queri
		fakultasModelList = session.createQuery(query).list(); //hibernate
	
		return fakultasModelList;
	}

	@Override
	public List<FakultasModel> searchNamaEqual(String namaFakultas) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //sintaks start queri
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>(); //instance return
		String query = " from FakultasModel where namaFakultas='"+namaFakultas+"' "; //queri
		fakultasModelList = session.createQuery(query).list(); //hibernate
	
		return fakultasModelList;
	}	

}
