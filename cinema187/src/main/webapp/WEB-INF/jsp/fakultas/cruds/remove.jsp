<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h1>Hapus Fakultas</h1>
	
	<form action="#" method="get" id="form-fakultas-delete">
		<input type="hidden" id="idFakultas" name="idFakultas" value="${fakultasModel.idFakultas}" />
		<table>
			<tr>
				<td>Kode Fakultas</td>
				<td><input type="text" id="kodeFakultas" name="kodeFakultas" value="${fakultasModel.kodeFakultas}"/></td>
			</tr>
			<tr>
				<td>Nama Fakultas</td>
				<td><input type="text" id="namaFakultas" name="namaFakultas" value="${fakultasModel.namaFakultas}"/></td>
			</tr>
			<tr>
				<td>Lokasi Fakultas</td>
				<td>
					<select id="idLokasi" name="idLokasi">
						<c:forEach items="${lokasiModelList}" var="lokasiModel">
							<option value="${lokasiModel.idLokasi}" 
								${lokasiModel.idLokasi==fakultasModel.idLokasi ? 'selected="true"':''}>
								${lokasiModel.namaLokasi}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><button type="submit">Delete</button></td>
			</tr>
		</table>
	</form>
</div>