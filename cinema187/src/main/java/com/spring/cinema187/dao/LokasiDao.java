package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.LokasiModel;

public interface LokasiDao {
	
	public List<LokasiModel> search();
}
