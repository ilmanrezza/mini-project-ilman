package com.spring.cinema187.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.cinema187.model.BahasaModel;
import com.spring.cinema187.model.GenreModel;
import com.spring.cinema187.model.NegaraModel;
import com.spring.cinema187.service.BahasaService;
import com.spring.cinema187.service.GenreService;
import com.spring.cinema187.service.NegaraService;

@Controller

public class NegaraController {
	@Autowired
	private NegaraService negaraService;

	@Autowired
	private GenreService genreService;
	
	@Autowired
	private BahasaService bahasaService;
	
	@RequestMapping(value = "negara") // url
	public String negara() {
		String jsp = "negara/negara";
		return jsp;
	}

	@RequestMapping(value = "negara/add") // url
	public String negaraAdd(Model model) {
		String kodeNegaraGenerator = ""; //lempar backend ke front end
		Long jumlahKode = 0L;
		jumlahKode=this.negaraService.countKode() + 1;
		kodeNegaraGenerator = "NGR000"+jumlahKode;
		model.addAttribute("kodeNegaraGenerator", kodeNegaraGenerator); //lempar backend ke front end
		
		List<GenreModel> genreModelList = new ArrayList<GenreModel>();
		genreModelList = this.genreService.searchAll();
		model.addAttribute("genreModelList", genreModelList);

		List<BahasaModel> bahasaModelList = new ArrayList<BahasaModel>();
		bahasaModelList = this.bahasaService.searchAll();
		model.addAttribute("bahasaModelList", bahasaModelList);
		
		String jsp = "negara/add";
		return jsp;
	}

	@RequestMapping(value = "negara/create")
	public String negaraCreate(HttpServletRequest request) {
		String kodeNegara = request.getParameter("kodeNegara");
		String namaNegara = request.getParameter("namaNegara");
		
		String kodeGenre = request.getParameter("kodeGenre");
		
		String idBahasa = request.getParameter(String.valueOf("idBahasa"));		
		
		NegaraModel negaraModel = new NegaraModel(); //kantong / tempatnya
		negaraModel.setKodeNegara(kodeNegara);
		negaraModel.setNamaNegara(namaNegara);
		
		negaraModel.setKodeGenre(kodeGenre);

		negaraModel.setIdBahasa(Integer.valueOf(idBahasa));
		
		this.negaraService.create(negaraModel);
		
		String jsp = "negara/negara";
		return jsp;
	}
	
	@RequestMapping(value="negara/list")
	public String negaraList(Model model) {
		List<NegaraModel> negaraModelList = new ArrayList<NegaraModel>();
		negaraModelList = this.negaraService.searchAll();
		model.addAttribute("negaraModelList", negaraModelList); 
		String jsp = "negara/list";
		return jsp;
	}
	
	@RequestMapping(value="negara/edit")
	public String negaraEdit(Model model, HttpServletRequest request) {
		String kodeNegara = request.getParameter("kodeNegara");
		NegaraModel negaraModel = new NegaraModel();
		negaraModel = this.negaraService.searchKode(kodeNegara);
		model.addAttribute("negaraModel", negaraModel); 			//backend ke frontend, taro di edit.
		String kota = "Jakarta";
		Integer usia = 22;
		model.addAttribute("kota", kota);
		model.addAttribute("usia", usia);
		
		String jsp = "negara/edit";
		return jsp;
	}
	
	@RequestMapping(value="negara/detail")
	public String negaraDetail(Model model, HttpServletRequest request) {
		String kodeNegara = request.getParameter("kodeNegara");
		NegaraModel negaraModel = new NegaraModel();
		negaraModel = this.negaraService.searchKode(kodeNegara);
		model.addAttribute("negaraModel", negaraModel);
		
		String jsp = "negara/detail";
		return jsp;
	}
	
	@RequestMapping(value="negara/remove")
	public String negaraRemove(Model model, HttpServletRequest request) {
		String kodeNegara = request.getParameter("kodeNegara");
		NegaraModel negaraModel = new NegaraModel();
		negaraModel = this.negaraService.searchKode(kodeNegara);
		model.addAttribute("negaraModel", negaraModel);
		
		String jsp = "negara/remove";
		return jsp;
	}
	
	@RequestMapping(value="negara/update")
	public String negaraUpdate(Model model, HttpServletRequest request) {
		String kodeNegara = request.getParameter("kodeNegara");
		String namaNegara = request.getParameter("namaNegara");
		
		NegaraModel negaraModel = new NegaraModel();
		negaraModel = this.negaraService.searchKode(kodeNegara);
		negaraModel.setNamaNegara(namaNegara); //set
		
		this.negaraService.update(negaraModel);
		
		String jsp = "negara/negara";
		return jsp;
	}
	
	@RequestMapping(value="negara/delete")
	public String negaraDelete(Model model, HttpServletRequest request) {
		String kodeNegara = request.getParameter("kodeNegara");
		String namaNegara = request.getParameter("namaNegara");
		
		NegaraModel negaraModel = new NegaraModel();
		negaraModel = this.negaraService.searchKode(kodeNegara);
		
		this.negaraService.delete(negaraModel);
		
		String jsp = "negara/negara";
		return jsp;
	}

	@RequestMapping(value="negara/search/kode")
	public String negaraSearchKode(Model model, HttpServletRequest request) {
		String kodeNegara = request.getParameter("kodeNegara");
		List<NegaraModel> negaraModelList = new ArrayList<NegaraModel>();
		negaraModelList = this.negaraService.searchKodeLike(kodeNegara);
		model.addAttribute("negaraModelList", negaraModelList);
		
		String jsp = "negara/list";
		return jsp;
	}

	@RequestMapping(value="negara/search/nama")
	public String negaraSearchNama(Model model, HttpServletRequest request) {
		String namaNegara = request.getParameter("namaNegara");
		List<NegaraModel> negaraModelList = new ArrayList<NegaraModel>();
		negaraModelList = this.negaraService.searchNamaLike(namaNegara);
		model.addAttribute("negaraModelList", negaraModelList);
		
		String jsp = "negara/list";
		return jsp;
	}

	@RequestMapping(value="negara/search")
	public String negaraSearch(HttpServletRequest request, Model model) {
		String kodeNegara = request.getParameter("keyword");
		String namaNegara = request.getParameter("keyword");
		List<NegaraModel> negaraModelList = new ArrayList<NegaraModel>();
		negaraModelList = this.negaraService.searchKodeOrNama(kodeNegara, namaNegara);
		
		model.addAttribute("negaraModelList", negaraModelList);
		String jsp ="negara/list";
		return jsp;
	}

	
}
