package com.spring.cinema187.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "T_QUESTION")
public class QuestionModel {
	private Integer idQuestion;
	private String question;
	private String question_type;
	private String option_a;
	private String option_b;
	private String option_c;
	private String option_d;
	private String option_e;
	private String image_url;
	private String image_a;
	private String image_b;
	private String image_c;
	private String image_d;
	private String image_e;
	
	// VARIABLE audit log
		private Integer idCreatedBy;
		private UserModel createdBy;
		private Date createdOn;
		
		private Integer idModifiedBy;
		private UserModel modifiedBy;
		private Date modifiedOn;
		
		private Integer idDeletedBy;
		private UserModel deletedBy;
		private Date deletedOn;
		private Boolean isDelete;
	 // VARIABLE audit log
		
		
@Id
@Column(name="IDQUESTION",length=11,nullable=false) //nama kolom
			@GeneratedValue(strategy=GenerationType.TABLE, generator="T_QUESTION") // utk buat nilai sequential
			@TableGenerator(name="T_QUESTION", table="M_SEQUENCE",
							pkColumnName="SEQUENCE_NAME", pkColumnValue="IDQUESTION",
							valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
		public Integer getIdQuestion() {
			return idQuestion;
		}
		public void setIdQuestion(Integer idQuestion) {
			this.idQuestion = idQuestion;
		}
		
@Column(name="QUESTION",length=255,nullable=false)
		public String getQuestion() {
			return question;
		}
		public void setQuestion(String question) {
			this.question = question;
		}
		
@Column(name="QUESTION_TYPE",length=5,nullable=false)
		public String getQuestion_type() {
			return question_type;
		}
		public void setQuestion_type(String question_type) {
			this.question_type = question_type;
		}
		
@Column(name="OPTION_A",length=255)
		public String getOption_a() {
			return option_a;
		}
		public void setOption_a(String option_a) {
			this.option_a = option_a;
		}
		
@Column(name="OPTION_B",length=255)
		public String getOption_b() {
			return option_b;
		}
		public void setOption_b(String option_b) {
			this.option_b = option_b;
		}
		
@Column(name="OPTION_C",length=255)
		public String getOption_c() {
			return option_c;
		}
		public void setOption_c(String option_c) {
			this.option_c = option_c;
		}
		
@Column(name="OPTION_D",length=255)
		public String getOption_d() {
			return option_d;
		}
		public void setOption_d(String option_d) {
			this.option_d = option_d;
		}
		
@Column(name="OPTION_E",length=255)
		public String getOption_e() {
			return option_e;
		}
		public void setOption_e(String option_e) {
			this.option_e = option_e;
		}
@Column(name="IMAGE_URL",length=255)
		public String getImage_url() {
			return image_url;
		}
		public void setImage_url(String image_url) {
			this.image_url = image_url;
		}
		
@Column(name="IMAGE_A",length=255)
		public String getImage_a() {
			return image_a;
		}
		public void setImage_a(String image_a) {
			this.image_a = image_a;
		}
@Column(name="IMAGE_B",length=255)
		public String getImage_b() {
			return image_b;
		}
		public void setImage_b(String image_b) {
			this.image_b = image_b;
		}
@Column(name="IMAGE_C",length=255)
		public String getImage_c() {
			return image_c;
		}
		public void setImage_c(String image_c) {
			this.image_c = image_c;
		}
		
@Column(name="IMAGE_D",length=255)
		public String getImage_d() {
			return image_d;
		}
		public void setImage_d(String image_d) {
			this.image_d = image_d;
		}
		
@Column(name="IMAGE_E",length=255)
		public String getImage_e() {
			return image_e;
		}
		public void setImage_e(String image_e) {
			this.image_e = image_e;
		}
		
		
@Column(name="CREATED_BY",length=11,nullable=false)
		public Integer getIdCreatedBy() {
			return idCreatedBy;
		}
		public void setIdCreatedBy(Integer idCreatedBy) {
			this.idCreatedBy = idCreatedBy;
		}
		
@ManyToOne
@JoinColumn(name="CREATED_BY", nullable=false, updatable=false, insertable=false)
		public UserModel getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(UserModel createdBy) {
			this.createdBy = createdBy;
		}
		
@Column(name="CREATED_ON",nullable=false)
		public Date getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}
		
@Column(name="MODIFIED_BY",length=11,nullable=true)
		public Integer getIdModifiedBy() {
			return idModifiedBy;
		}
		public void setIdModifiedBy(Integer idModifiedBy) {
			this.idModifiedBy = idModifiedBy;
		}
		
@ManyToOne
@JoinColumn(name="MODIFIED_BY", nullable=true, updatable=false, insertable=false)
		public UserModel getModifiedBy() {
			return modifiedBy;
		}
		public void setModifiedBy(UserModel modifiedBy) {
			this.modifiedBy = modifiedBy;
		}
		
@Column(name="MODIFIED_ON",nullable=true)
		public Date getModifiedOn() {
			return modifiedOn;
		}
		public void setModifiedOn(Date modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
		
@Column(name="DELETED_BY",length=11,nullable=true)	
		public Integer getIdDeletedBy() {
			return idDeletedBy;
		}
		public void setIdDeletedBy(Integer idDeletedBy) {
			this.idDeletedBy = idDeletedBy;
		}
		
@ManyToOne
@JoinColumn(name="DELETED_BY", nullable=true, updatable=false, insertable=false)
		public UserModel getDeletedBy() {
			return deletedBy;
		}
		public void setDeletedBy(UserModel deletedBy) {
			this.deletedBy = deletedBy;
		}
		
@Column(name="DELETED_ON",nullable=true)
		public Date getDeletedOn() {
			return deletedOn;
		}
		public void setDeletedOn(Date deletedOn) {
			this.deletedOn = deletedOn;
		}
		
@Column(name="IS_DELETED",nullable=false)
		public Boolean getIsDelete() {
			return isDelete;
		}
		public void setIsDelete(Boolean isDelete) {
			this.isDelete = isDelete;
		}
	
		

}
