package com.spring.cinema187.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.cinema187.model.AnggotaModel;
import com.spring.cinema187.model.FilmModel;
import com.spring.cinema187.service.AnggotaService;

@Controller
public class AnggotaController {

	@Autowired
	private AnggotaService anggotaService;
	
	@RequestMapping(value = "anggota")
	public String anggota() {
		String jsp = "anggota/anggota";
		return jsp;
	}

	@RequestMapping(value = "anggota/add")
	public String anggotaAdd() {
		String jsp = "anggota/add";
		return jsp;
	}
	
	@RequestMapping(value = "anggota/create")
	public String anggotaCreate(HttpServletRequest request) {
		String idAnggota = String.valueOf(request.getParameter("idAnggota"));
		String kodeAnggota = request.getParameter("kodeAnggota");
		String namaAnggota = request.getParameter("namaAnggota");
		String usia = String.valueOf(request.getParameter("usia"));

		AnggotaModel anggotaModel = new AnggotaModel(); // kantong / tempatnya
		anggotaModel.setIdAnggota(Integer.valueOf(idAnggota));
		anggotaModel.setKodeAnggota(kodeAnggota);
		anggotaModel.setNamaAnggota(namaAnggota);
		anggotaModel.setUsia(Integer.valueOf(usia));
		
		this.anggotaService.create(anggotaModel);

		String jsp = "anggota/anggota";
		return jsp;
	}
	
	@RequestMapping(value = "anggota/list")
	public String anggotaList(Model model) {
		List<AnggotaModel> anggotaModelList = new ArrayList<AnggotaModel>();
		anggotaModelList = this.anggotaService.searchAll();
		model.addAttribute("anggotaModelList", anggotaModelList);
		String jsp = "anggota/list";
		return jsp;

	}


}
