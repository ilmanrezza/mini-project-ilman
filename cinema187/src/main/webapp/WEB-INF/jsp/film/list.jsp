<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${filmModelList}" var="filmModel" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${filmModel.kodeFilm}</td>
		<td>${filmModel.namaFilm}</td>
		<td>${filmModel.genre}</td>
		<td>${filmModel.artis}</td>
		<td>${filmModel.produser}</td>
		<td>${filmModel.pendapatan}</td>
		<td>${filmModel.nominasi}</td>
				
		<td>
			<button type="button" class="btn btn-warning" value="${filmModel.kodeFilm}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${filmModel.kodeFilm}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${filmModel.kodeFilm}" id="btn-delete">Hapus</button>
		</td>
	</tr>
</c:forEach>