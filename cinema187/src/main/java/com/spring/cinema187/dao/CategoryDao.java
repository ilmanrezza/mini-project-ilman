package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.CategoryModel;

public interface CategoryDao {
	public void create(CategoryModel categoryModel);
	public List<CategoryModel> searchAll();
	public Long countKode();
	public void update(CategoryModel categoryModel);
	public CategoryModel searchCode(String code);
	
	public List<CategoryModel> searchCodeOrName(String code, String name);
}
