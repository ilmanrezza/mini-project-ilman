<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Lihat Film</h1>
	<form action="#" method="get" id="form-film-detail">
		<table>
			<tr>
				<td>Kode Film</td>
				<td><input type="text" id="kodeFilm" name="kodeFilm" value = "${filmModel.kodeFilm}"/></td>
			</tr>
			<tr>
				<td>Nama Film</td>
				<td><input type="text" id="namaFilm"name="namaFilm" value = "${filmModel.namaFilm}"/></td>
			</tr>
			<tr>
				<td>Genre</td>
				<td><input type="text" id="genre"name="genre" value = "${filmModel.genre}"/></td>
			</tr>
			<tr>
				<td>Artis</td>
				<td><input type="text" id="artis"name="artis" value = "${filmModel.artis}"/></td>
			</tr>
			<tr>
				<td>Produser</td>
				<td><input type="text" id="produser"name="produser" value = "${filmModel.produser}"/></td>
			</tr>
			<tr>
				<td>Pendapatan</td>
				<td><input type="text" id="pendapatan"name="pendapatan" value = "${filmModel.pendapatan}"/></td>
			</tr>
			<tr>
				<td>Nominasi</td>
				<td><input type="text" id="nominasi"name="nominasi" value = "${filmModel.nominasi}"/></td>
			</tr>
		</table>
	</form>
</div>

