<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Lihat Artis</h1>
	<form action="#" method="get" id="form-artis-detail">
		<table>
			<tr>
				<td>Kode Artis</td>
				<td><input type="text" id="kodeArtis" name="kodeArtis" value = "${artisModel.kodeArtis}"/></td>
			</tr>
			<tr>
				<td>Nama Artis</td>
				<td><input type="text" id="namaArtis" name="namaArtis" value = "${artisModel.namaArtis}"/></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td><input type="text" id="jenisKelamin" name="jenisKelamin" value = "${artisModel.jenisKelamin}"/></td>
			</tr>
			<tr>
				<td>Bayaran</td>
				<td><input type="text" id="bayaran" name="bayaran" value = "${artisModel.bayaran}"/></td>
			</tr>
			<tr>
				<td>Award</td>
				<td><input type="text" id="award" name="award" value = "${artisModel.award}"/></td>
			</tr>
			<tr>
				<td>Negara</td>
				<td><input type="text" id="negara" name="negara" value = "${artisModel.negara}"/></td>
			</tr>
		</table>
	</form>
</div>

<script type = "text/javascript">

</script>