package com.spring.cinema187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.cinema187.dao.LokasiDao;
import com.spring.cinema187.model.LokasiModel;
import com.spring.cinema187.service.LokasiService;

@Service
@Transactional
public class LokasiServiceImpl implements LokasiService{
	
	@Autowired
	private LokasiDao lokasiDao;

	@Override
	public List<LokasiModel> search() {
		// TODO Auto-generated method stub
		return this.lokasiDao.search();
	}

}
