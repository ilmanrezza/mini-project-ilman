package com.spring.cinema187.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_FILM")

public class FilmModel {
	private String kodeFilm;
	private String namaFilm;
	private String genre;
	private String artis;
	private String produser;
	private Integer pendapatan;
	private Integer nominasi;
	
	@Id
	@Column(name="KD_FILM")
	public String getKodeFilm() {
		return kodeFilm;
	}
	public void setKodeFilm(String kodeFilm) {
		this.kodeFilm = kodeFilm;
	}
	
	@Column(name="NM_FILM")
	public String getNamaFilm() {
		return namaFilm;
	}
	public void setNamaFilm(String namaFilm) {
		this.namaFilm = namaFilm;
	}
	
	@Column(name="GENRE")
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	@Column(name="ARTIS")
	public String getArtis() {
		return artis;
	}
	public void setArtis(String artis) {
		this.artis = artis;
	}
	
	@Column(name="PRODUSER")
	public String getProduser() {
		return produser;
	}
	public void setProduser(String produser) {
		this.produser = produser;
	}
	
	@Column(name="PENDAPATAN")
	public Integer getPendapatan() {
		return pendapatan;
	}
	public void setPendapatan(Integer pendapatan) {
		this.pendapatan = pendapatan;
	}
	
	@Column(name="NOMINASI")
	public Integer getNominasi() {
		return nominasi;
	}
	public void setNominasi(Integer nominasi) {
		this.nominasi = nominasi;
	}


}
