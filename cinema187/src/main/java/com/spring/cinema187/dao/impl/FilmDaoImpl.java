package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.FilmDao;
import com.spring.cinema187.model.FilmModel;

@Repository
public class FilmDaoImpl implements FilmDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(FilmModel filmModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(filmModel);
	}

	@Override
	public List<FilmModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<FilmModel> filmModelList = new ArrayList<FilmModel>();
		String query = "from FilmModel";
		String kondisi = " ";
		filmModelList = session.createQuery(query + kondisi).list(); // hql: select from kl di sql
		return filmModelList;

	}

	@Override
	public FilmModel searchKode(String kodeFilm) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //set start query
		
		FilmModel filmModel = new FilmModel();
		String query = "from FilmModel";
		String kondisi = " where kodeFilm = '"+kodeFilm+"' "; //query	
		filmModel = (FilmModel) session.createQuery(query + kondisi).uniqueResult(); //hql: select from kl di sql
		return filmModel;
	}

	@Override
	public void update(FilmModel filmModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(filmModel);

	}

	@Override
	public void delete(FilmModel filmModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(filmModel);
		
	}

}
