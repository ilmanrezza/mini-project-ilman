<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Question</h1>
		
		<div class="form-group">
			<div class="col-md-12">
						<form action="#" method="get" id="form-question-search">
						<input type="text" id="questionSearch" name="questionSearch" placeholder="Search by Question" style="height:30px;width:300px;" required="required"/>
						<button type="submit" class="btn btn-warning">
						<span class="glyphicon glyphicon-search"></span> Search
						</button>
						</form>
			</div>
		</div>

	<div>
	
		<table class="table" id="tbl-question">
			<tr>
				<td></td> 
				<td></td> 
				<td><button type="button" id="btn-add" style="border:5; background-color : orange;">
				<span class="glyphicon glyphicon-plus"></span>
				</button></td>
				<td></td>
	
			</tr>
			<tr>
				<td style="width:300px; font-weight: bold;">Question</td>
				<td style="width:700px; font-weight: bold;">Type</td> 
				<td> <span class="glyphicon glyphicon-th-list"></span></td>
			</tr>
			<tbody id="list-question">

			</tbody>
		</table>
	</div>

</div>


<!-- syntax popup add -->
<div id="modal-input-add" class="modal fade">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header" style="background-color : orange;">
				
				<h1  style="font-weight: bold;"> Add Question</h1>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
			<div>
			
			</div>
			
		</div>
	</div>
</div>

<!-- syntax popup edit -->
<div id="modal-input-edit" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color : orange;">
				
				<h1  style="font-weight: bold;">Question</h1>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
			<div>
			
			</div>
			
		</div>
	</div>
</div>


<!-- syntax popup remove -->
<div id="modal-input-remove" class="modal fade">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="background-color : orange;">
				
				<h1  style="font-weight: bold;">Question</h1>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
			<div>
			
			</div>
			
		</div>
	</div>
</div>


<script type="text/javascript">

loadListQuestion();

function loadListQuestion() {
	$.ajax({
		url : 'question/list.html',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#list-question').html(result);
		}
	});
}

//fungsi ajax untuk popup tambah
$('#btn-add').on('click', function() {
	$.ajax({
		url : 'question/add.html',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#modal-input-add').find('.modal-body').html(result);
			$('#modal-input-add').modal('show');
		}
	});
});
//akhir fungsi ajax untuk popup tambah

	//fungsi ajax untuk create tambah
	$('#modal-input-add').on('submit', '#form-question-add', function() {
		alert('tes');
		$.ajax({
			url : 'question/create.json',
			type : 'get',
			dataType : 'json',
			data : $(this).serialize(),
			success : function(result) {
				
				$('#modal-input-add').modal('hide');
				alert("data telah ditambah!");
				loadListQuestion();
			}
		});
		return false;
	});
	//akhir fungsi ajax untuk create tambah
	
		//fungsi ajax untuk popup edit
		$('#list-question').on('click','#btn-edit',function(){
			var idQuestion = $(this).val();
			$.ajax({
				url : 'question/edit.html',
				type : 'get',
				dataType : 'html',
				data:{idQuestion:idQuestion},
				success : function(result) {
					$('#modal-input-edit').find('.modal-body').html(result);
					$('#modal-input-edit').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit
		
		
		//fungsi ajax untuk update
		$('#modal-input-edit').on('submit','#form-question-edit',function(){
			
			$.ajax({
				url:'question/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input-edit').modal('hide');
					alert("data telah diubah!");
					loadListQuestion();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
		
				//fungsi ajax untuk search 
		$('#form-question-search').on('submit',function(){
			$.ajax({
				url:'question/search.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data berhasil dicari !");
					$('#list-question').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search 
		
				//fungsi ajax untuk hapus
		$('#list-question').on('click','#btn-delete',function(){
			var idQuestion = $(this).val();
			$.ajax({
				url:'question/remove.html',
				type:'get',
				dataType:'html',
				data:{idQuestion:idQuestion},
				success: function(result){
					$('#modal-input-remove').find('.modal-body').html(result);
					$('#modal-input-remove').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk hapus
		
		//fungsi ajax untuk delete
		$('#modal-input-remove').on('submit','#form-question-delete',function(){
			
			$.ajax({
				url:'question/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input-remove').modal('hide');
					alert("data telah dihapus!");
					loadListQuestion();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
		
</script>