<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">

	<form action="#" method="get" id="form-category-add">
		
		<div class="form-group">
      		<div class="col-md-12">
      		 <label for="usr">Name:</label>
      		 <input type="hidden" id="code" name="code"  value = "${kodecategoryGenerator}"/>
     		 <input type="text" class="form-control" id="usr" name="name" placeholder="Nama">
     		 </div>
    	</div>
	
		<div class="form-group">
				<div class="col-md-12">
 		 	<label for="comment">Deskripsi:</label>
 		 	<textarea class="form-control" rows="5" id="comment" name="deskription" placeholder="Deskription"></textarea>
 		 	</div>
		</div>
		
		<button type="reset" data-dismiss="modal" class="btn btn-warning">Cancel </button> 
		<button type="submit" class="btn btn-warning">Save</button>
	</form>
</div>

