package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.GenreDao;
import com.spring.cinema187.model.GenreModel;

@Repository
public class GenreDaoImpl implements GenreDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(GenreModel genreModel) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.getCurrentSession();
		session.save(genreModel);
		
	}

	@Override
	public List<GenreModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<GenreModel> genreModelList = new ArrayList<GenreModel>();
		String query = "from GenreModel";
		String kondisi = " ";
		genreModelList = session.createQuery(query + kondisi).list(); //hql: select from kl di sql
		return genreModelList;
	}

	@Override
	public GenreModel searchKode(String kodeGenre) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //set start query
		
		GenreModel genreModel = new GenreModel();
		String query = "from GenreModel";
		String kondisi = " where kodeGenre = '"+kodeGenre+"' "; //query	
		genreModel = (GenreModel) session.createQuery(query + kondisi).uniqueResult(); //hql: select from kl di sql
		return genreModel;
	}

	@Override
	public void update(GenreModel genreModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(genreModel);		
	}

	@Override
	public void delete(GenreModel genreModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(genreModel);
		
	}

	@Override
	public List<GenreModel> searchKodeLike(String kodeGenre) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //set start query
		List<GenreModel> genreModelList = new ArrayList<GenreModel>();
		String query = "from GenreModel";
		String kondisi = " where kodeGenre like '%"+kodeGenre+"%' ";
		//select * from T_NEGARA where kodeNegara like '%kodeNegara%'
		genreModelList = session.createQuery(query + kondisi).list(); //hql: select from kl di sql
		return genreModelList;
	}

	@Override
	public List<GenreModel> searchNamaLike(Object namaGenre) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //set start query
		List<GenreModel> genreModelList = new ArrayList<GenreModel>();
		String query = " from GenreModel ";
		String kondisi = " where namaGenre like '%"+namaGenre+"%' ";
		//select * from T_NEGARA where kodeNegara like '%kodeNegara%'
		genreModelList = session.createQuery(query + kondisi).list(); //hql: select from kl di sql
		return genreModelList;
	}


	
	
	
}
