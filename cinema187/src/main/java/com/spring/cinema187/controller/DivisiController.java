package com.spring.cinema187.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.cinema187.model.DivisiModel;
import com.spring.cinema187.service.DivisiService;

@Controller
public class DivisiController {

	@Autowired
	private DivisiService divisiService;
	
	@RequestMapping(value="divisi")
	public String divisi() {
		String jsp = "divisi/divisi";
		return jsp;
	}
	
	@RequestMapping(value="divisi/add") //url
	public String divisiAdd() {
		String jsp="divisi/add";
		return jsp;
	}

	@RequestMapping(value = "divisi/create")
	public String divisiCreate(HttpServletRequest request) {
	//	String idDivisi = String.valueOf(request.getParameter("idDivisi"));
		String kodeDivisi = request.getParameter("kodeDivisi");
		String namaDivisi = request.getParameter("namaDivisi");
		
		DivisiModel divisiModel = new DivisiModel(); //kantong / tempatnya
	//	divisiModel.setIdDivisi(Integer.valueOf(idDivisi));
		divisiModel.setKodeDivisi(kodeDivisi);
		divisiModel.setNamaDivisi(namaDivisi);
		
		this.divisiService.create(divisiModel);
		
		String jsp = "divisi/divisi";
		return jsp;
	}

	@RequestMapping(value="divisi/list")
	public String divisiList(Model model) {
		List<DivisiModel> divisiModelList = new ArrayList<DivisiModel>();
		divisiModelList = this.divisiService.searchAll();
		model.addAttribute("divisiModelList", divisiModelList); 
		String jsp = "divisi/list";
		return jsp;

	}

	@RequestMapping(value="divisi/edit")
	public String divisiEdit(Model model, HttpServletRequest request) {
		String idDivisi = String.valueOf(request.getParameter("idDivisi"));
		DivisiModel divisiModel = new DivisiModel();
		divisiModel = this.divisiService.searchKode(idDivisi);
		model.addAttribute("divisiModel", divisiModel); 			//backend ke frontend, taro di edit.
		String kota = "Jakarta";
		Integer usia = 22;
		model.addAttribute("kota", kota);
		model.addAttribute("usia", usia);
		
		String jsp = "divisi/edit";
		return jsp;
	}

	@RequestMapping(value="divisi/update")
	public String divisiUpdate(Model model, HttpServletRequest request) {
		String idDivisi = String.valueOf(request.getParameter("idDivisi"));
		String kodeDivisi = request.getParameter("kodeDivisi");
		String namaDivisi = request.getParameter("namaDivisi");

		DivisiModel divisiModel = new DivisiModel(); //kantong / tempatnya
		divisiModel.setIdDivisi(Integer.valueOf(idDivisi));
		divisiModel.setKodeDivisi(kodeDivisi);
		divisiModel.setNamaDivisi(namaDivisi);
		
		this.divisiService.create(divisiModel);
		
		String jsp = "divisi/divisi";
		return jsp;
	}

	
}
