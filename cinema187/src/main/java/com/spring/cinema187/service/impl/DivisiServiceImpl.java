package com.spring.cinema187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.cinema187.dao.DivisiDao;
import com.spring.cinema187.model.DivisiModel;
import com.spring.cinema187.service.DivisiService;

@Service
@Transactional
public class DivisiServiceImpl implements DivisiService{
	
	@Autowired
	private DivisiDao divisiDao;

	@Override
	public void create(DivisiModel divisiModel) {
		// TODO Auto-generated method stub
		this.divisiDao.create(divisiModel);
	}

	@Override
	public List<DivisiModel> searchAll() {
		// TODO Auto-generated method stub
		return this.divisiDao.searchAll();
	}

	@Override
	public DivisiModel searchKode(String idDivisi) {
		// TODO Auto-generated method stub
		return this.divisiDao.searchKode(idDivisi);
	}

	@Override
	public void update(DivisiModel divisiModel) {
		// TODO Auto-generated method stub
		this.divisiDao.update(divisiModel);
	}
	
}
