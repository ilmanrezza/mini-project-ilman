package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.QuestionDao;
import com.spring.cinema187.model.QuestionModel;

@Repository
public class QuestionDaoImpl implements QuestionDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(QuestionModel questionModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(questionModel);
	}

	@Override
	public void update(QuestionModel questionModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(questionModel);
	}

	@Override
	public List<QuestionModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<QuestionModel> questionModelList = new ArrayList<QuestionModel>();
		String query = "from QuestionModel";
		String kondisi = " where isDelete = false ORDER BY question ";
		questionModelList= session.createQuery(query + kondisi).list();
		return questionModelList;
	}

	@Override
	public QuestionModel searchIdQuestion(Integer idQuestion) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		QuestionModel questionModel = new QuestionModel();
		String query = "from QuestionModel";
		String kondisi = " where  idQuestion like '%"+idQuestion+"%'";
		questionModel = (QuestionModel) session.createQuery(query + kondisi).uniqueResult();
		return null;
	}

	@Override
	public List<QuestionModel> searchQuestion(String question) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<QuestionModel> questionModelList = new ArrayList<QuestionModel>();
		String query = "from QuestionModel";
		String kondisi = " where  question like '%"+question+"%' and isDelete = false ORDER BY question ";
		questionModelList= session.createQuery(query + kondisi).list();
		return questionModelList;
	}
	
	

}
