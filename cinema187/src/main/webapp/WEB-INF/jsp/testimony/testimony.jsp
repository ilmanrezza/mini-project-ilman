<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Testimony</h1>

	<div>
		<table class="table" id="tbl-testimony">
			<tr>
				<td><form action="#" method="get" id="form-testimony-search">
						<input type="text" id="title" name="title" placeholder="Search by Title" style="height:30px;width:300px;"/>
						<button type="submit" class="btn btn-warning">
						<span class="glyphicon glyphicon-search"></span> Search
						</button>
						</form>
						</td> 
				<td></td> 
				<td><button type="button" id="btn-add" style="border:5; background-color : orange;">
				<span class="glyphicon glyphicon-plus"></span>
				</button></td>
				<td></td>
	
			</tr>
			<tr>
				<td style="font-weight: bold;">Title</td>
				<td></td> 
				<td> <span class="glyphicon glyphicon-th-list"></span></td>
			</tr>
			<tbody id="list-testimony">

			</tbody>
		</table>
	</div>

</div>


<!-- syntax popup add -->
<div id="modal-input-add" class="modal fade">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header" style="background-color : orange;">
				
				<h1  style="font-weight: bold;">Testimony</h1>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
			<div>
			
			</div>
			
		</div>
	</div>
</div>

<!-- syntax popup edit -->
<div id="modal-input-edit" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color : orange;">
				
				<h1  style="font-weight: bold;">Testimony</h1>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
			<div>
			
			</div>
			
		</div>
	</div>
</div>


<!-- syntax popup remove -->
<div id="modal-input-remove" class="modal fade">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="background-color : orange;">
				
				<h1  style="font-weight: bold;">Testimony</h1>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
			<div>
			
			</div>
			
		</div>
	</div>
</div>


<script type="text/javascript">

loadListTestimony();

function loadListTestimony() {
	$.ajax({
		url : 'testimony/list.html',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#list-testimony').html(result);
		}
	});
}

//fungsi ajax untuk popup tambah
$('#btn-add').on('click', function() {
	$.ajax({
		url : 'testimony/add.html',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#modal-input-add').find('.modal-body').html(result);
			$('#modal-input-add').modal('show');
		}
	});
});
//akhir fungsi ajax untuk popup tambah

	//fungsi ajax untuk create tambah
	$('#modal-input-add').on('submit', '#form-testimony-add', function() {
		$.ajax({
			url : 'testimony/create.json',
			type : 'get',
			dataType : 'json',
			data : $(this).serialize(),
			success : function(result) {
				
				$('#modal-input-add').modal('hide');
				alert("data telah ditambah!");
				loadListTestimony();
			}
		});
		return false;
	});
	//akhir fungsi ajax untuk create tambah
	
		//fungsi ajax untuk popup edit
		$('#list-testimony').on('click','#btn-edit',function(){
			var id = $(this).val();
			$.ajax({
				url : 'testimony/edit.html',
				type : 'get',
				dataType : 'html',
				data:{id:id},
				success : function(result) {
					$('#modal-input-edit').find('.modal-body').html(result);
					$('#modal-input-edit').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit
		
		
		//fungsi ajax untuk update
		$('#modal-input-edit').on('submit','#form-testimony-edit',function(){
			
			$.ajax({
				url:'testimony/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input-edit').modal('hide');
					alert("data telah diubah!");
					loadListTestimony();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
		
				//fungsi ajax untuk search 
		$('#form-testimony-search').on('submit',function(){
			$.ajax({
				url:'testimony/search.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data berhasil dicari !");
					$('#list-testimony').html(result);
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search 
		
				//fungsi ajax untuk hapus
		$('#list-testimony').on('click','#btn-delete',function(){
			var id = $(this).val();
			$.ajax({
				url:'testimony/remove.html',
				type:'get',
				dataType:'html',
				data:{id:id},
				success: function(result){
					$('#modal-input-remove').find('.modal-body').html(result);
					$('#modal-input-remove').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk hapus
		
		//fungsi ajax untuk delete
		$('#modal-input-remove').on('submit','#form-testimony-delete',function(){
			
			$.ajax({
				url:'testimony/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input-remove').modal('hide');
					alert("data telah dihapus!");
					loadListTestimony();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
		
</script>