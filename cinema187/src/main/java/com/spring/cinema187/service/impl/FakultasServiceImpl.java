package com.spring.cinema187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.cinema187.dao.FakultasDao;
import com.spring.cinema187.model.FakultasModel;
import com.spring.cinema187.service.FakultasService;

@Service
@Transactional
public class FakultasServiceImpl implements FakultasService {

	@Autowired
	private FakultasDao fakultasDao;
	
	@Override
	public void create(FakultasModel fakultasModel) {
		// TODO Auto-generated method stub
		this.fakultasDao.create(fakultasModel);
	}

	@Override
	public void update(FakultasModel fakultasModel) {
		// TODO Auto-generated method stub
		this.fakultasDao.update(fakultasModel);
	}

	@Override
	public void delete(FakultasModel fakultasModel) {
		// TODO Auto-generated method stub
		this.fakultasDao.delete(fakultasModel);
	}

	@Override
	public List<FakultasModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		return this.fakultasDao.search(kodeRole);
	}

	@Override
	public FakultasModel searchId(Integer idFakultas) {
		// TODO Auto-generated method stub
		return this.fakultasDao.searchId(idFakultas);
	}

	@Override
	public List<FakultasModel> searchNama(String namaFakultas) {
		// TODO Auto-generated method stub
		return this.fakultasDao.searchNama(namaFakultas);
	}

	@Override
	public List<FakultasModel> searchKode(String kodeFakultas) {
		// TODO Auto-generated method stub
		return this.fakultasDao.searchKode(kodeFakultas);
	}

	@Override
	public List<FakultasModel> searchKodeOrNama(String kodeFakultas, String namaFakultas) {
		// TODO Auto-generated method stub
		return this.fakultasDao.searchKodeOrNama(kodeFakultas, namaFakultas);
	}

	@Override
	public List<FakultasModel> searchKodeEqual(String kodeFakultas) {
		// TODO Auto-generated method stub
		return this.fakultasDao.searchKodeEqual(kodeFakultas);
	}

	@Override
	public List<FakultasModel> searchNamaEqual(String namaFakultas) {
		// TODO Auto-generated method stub
		return this.fakultasDao.searchNamaEqual(namaFakultas);
	}

	@Override
	public void deleteTemporary(FakultasModel fakultasModel) {
		// TODO Auto-generated method stub
		this.fakultasDao.update(fakultasModel);
	}

	

}
