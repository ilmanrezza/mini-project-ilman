<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Edit Negara</h1>

	<form action="#" method="get" id="form-negara-edit">
		<table>
			<tr>
				<td>Kode Negara</td>
				<td><input type="text" id="kodeNegara" name="kodeNegara" value = "${negaraModel.kodeNegara}"/></td>
			</tr>
			<tr>
				<td>Nama Negara</td>
				<td><input type="text" id="namaNegara"name="namaNegara" value = "${negaraModel.namaNegara}"/></td>
			</tr>
		</table>
		<button type="submit" onclick="validasi();">Simpan</button> 
	</form>
</div>

<script type = "text/javascript">
	function validasi() {
		var kodeNegara = document.getElementById("kodeNegara");
		if (kodeNegara.value == "") {
			kodeNegara.style.borderColor = "red";
		} else {
			kodeNegara.style.borderColor = "gray";
		}
		
		var namaNegara = document.getElementById("namaNegara");
		if (namaNegara.value == "") {
			namaNegara.style.borderColor = "red";
		} else {
			namaNegara.style.borderColor = "gray";
		}
	}
</script>