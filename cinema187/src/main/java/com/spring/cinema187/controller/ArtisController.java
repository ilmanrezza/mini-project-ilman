package com.spring.cinema187.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.cinema187.model.ArtisModel;
import com.spring.cinema187.service.ArtisService;


@Controller
public class ArtisController {

	@Autowired
	private ArtisService artisService;

	@RequestMapping(value="artis")
		public String artis() {
		String jsp="artis/artis";
		return jsp;
	}
	
	@RequestMapping(value="artis/add") //url
	public String artisAdd() {
		String jsp="artis/add";
		return jsp;
	}

	@RequestMapping(value = "artis/create")
	public String artisCreate(HttpServletRequest request) {
		String kodeArtis = request.getParameter("kodeArtis");
		String namaArtis = request.getParameter("namaArtis");
		String jenisKelamin = request.getParameter("jenisKelamin");
		String bayaran = String.valueOf(request.getParameter("bayaran"));
		String award = String.valueOf(request.getParameter("award"));
		String negara = request.getParameter("negara");
		
		ArtisModel artisModel = new ArtisModel(); //kantong / tempatnya
		artisModel.setKodeArtis(kodeArtis);
		artisModel.setNamaArtis(namaArtis);
		artisModel.setJenisKelamin(jenisKelamin);
		artisModel.setBayaran(Integer.valueOf(bayaran));
		artisModel.setAward(Integer.valueOf(award));
		artisModel.setNegara(negara);
		
		this.artisService.create(artisModel);
		
		String jsp = "artis/artis";
		return jsp;
	}

	@RequestMapping(value="artis/list")
	public String artisList(Model model) {
		List<ArtisModel> artisModelList = new ArrayList<ArtisModel>();
		artisModelList = this.artisService.searchAll();
		model.addAttribute("artisModelList", artisModelList); 
		String jsp = "artis/list";
		return jsp;

	}
	
	@RequestMapping(value="artis/edit")
	public String artisEdit(Model model, HttpServletRequest request) {
		String kodeArtis = request.getParameter("kodeArtis");
		ArtisModel artisModel = new ArtisModel();
		artisModel = this.artisService.searchKode(kodeArtis);
		model.addAttribute("artisModel", artisModel); 			//backend ke frontend, taro di edit.
		String kota = "Jakarta";
		Integer usia = 22;
		model.addAttribute("kota", kota);
		model.addAttribute("usia", usia);
		
		String jsp = "artis/edit";
		return jsp;
	}

	@RequestMapping(value="artis/update")
	public String artisUpdate(Model model, HttpServletRequest request) {
		String kodeArtis = request.getParameter("kodeArtis");
		String namaArtis = request.getParameter("namaArtis");
		String jenisKelamin = request.getParameter("jenisKelamin");
		String bayaran = String.valueOf(request.getParameter("bayaran"));
		String award = String.valueOf(request.getParameter("award"));
		String negara = request.getParameter("negara");

		ArtisModel artisModel = new ArtisModel(); //kantong / tempatnya
		artisModel = this.artisService.searchKode(kodeArtis);
		artisModel.setNamaArtis(namaArtis);
		artisModel.setJenisKelamin(jenisKelamin);
		artisModel.setBayaran(Integer.valueOf(bayaran));
		artisModel.setAward(Integer.valueOf(award));
		artisModel.setNegara(negara);
		
		this.artisService.update(artisModel);
		
		String jsp = "artis/artis";
		return jsp;
	}

	@RequestMapping(value="artis/remove")
	public String artisRemove(Model model, HttpServletRequest request) {
		String kodeArtis = request.getParameter("kodeArtis");
		ArtisModel artisModel = new ArtisModel();
		artisModel = this.artisService.searchKode(kodeArtis);
		model.addAttribute("artisModel", artisModel);
		
		String jsp = "artis/remove";
		return jsp;
	}

	@RequestMapping(value="artis/delete")
	public String artisDelete(Model model, HttpServletRequest request) {
		String kodeArtis = request.getParameter("kodeArtis");
		String namaArtis = request.getParameter("namaArtis");
		String jenisKelamin = request.getParameter("jenisKelamin");
		String bayaran = String.valueOf(request.getParameter("bayaran"));
		String award = String.valueOf(request.getParameter("award"));
		String negara = request.getParameter("negara");
		
		ArtisModel artisModel = new ArtisModel();
		artisModel = this.artisService.searchKode(kodeArtis);
		
		this.artisService.delete(artisModel);
		
		String jsp = "artis/artis";
		return jsp;
	}

	@RequestMapping(value="artis/detail")
	public String artisDetail(Model model, HttpServletRequest request) {
		String kodeArtis = request.getParameter("kodeArtis");
		ArtisModel artisModel = new ArtisModel();
		artisModel = this.artisService.searchKode(kodeArtis);
		model.addAttribute("artisModel", artisModel);
		
		String jsp = "artis/detail";
		return jsp;
	}

	
}
