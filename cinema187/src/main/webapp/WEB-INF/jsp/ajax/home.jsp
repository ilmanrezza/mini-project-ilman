
<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Welcome to Ajax</h1>
	<button type="button" onclick="coba();" >Popup Function</button>
	<button type="button" id="tombol-popsatu">Popup Ajax</button>
	<br>
	<button type="button" onclick="kota();">Popup Kota</button>
	<button type="button" onclick="negara();" id="tombol-popnegara">Popup Negara</button>
	<br>
	<button type="button" id="popupsatu">Popup Satu</button>
	<button type="button" id="popupdua">Popup Dua</button>
	<br><br>
	saya suka <input type="text" id="vegetable"/>
	<br><br><br><br>
	<button type="button" id="biodata">Data</button>
	<br><br>
	<button type="button" id="mamin">Input Makan & Minum</button>
	<br><br>
	<div>
		<table class="table" id="tbl-genre">
			<tr>
				<td>Makan</td>
				<td>Minum</td>
			</tr>
			<tbody id="list-genre">

			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<div id="modal-input1" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	function coba() { //sintaks function javascript sederhana
		alert("ilman");
	}
	//$(namaId).on(fungsi,function(){}); sintaks jquery sederhana
	$("#tombol-popsatu").on('click',function(){
		alert("ILMAN");
	});
	
	function kota() {
		alert("BANDUNG");
	}
	
	$("#tombol-popnegara").on('click', function(){
		alert("Indonesia");
	});
	
	//$(namaId).on(fungsi,function(){ sintaks ini adalah fungsi ajax untuk connect ke controller
		//$.ajax({
			//url:namaurl,
			//type:pilihan get/post,
			//dataType:ekstensi html/json,
			//success:function(result){
				
			//}
			
		//});
	//}); 
	
	$("#popupsatu").on('click',function(){ 
		$.ajax({
			url: 'ajax/popsatu.html',
			type: 'get',
			dataType:'html',
			success:function(result){
				$('#modal-input').find('.modal-body').html(result);
				$('#modal-input').modal('show');
			}
			
		});
	}); 
	
	$("#popupdua").on('click',function(){ 
		$.ajax({
			url: 'ajax/popdua.html',
			type: 'get',
			dataType:'html',
			success:function(result){
				$('#modal-input').find('.modal-body').html(result);
				$('#modal-input').modal('show');
			}
			
		});
	}); 
	
	$("#modal-input").on('submit','#form-popsatu',function(){ 
		$.ajax({
			url: 'ajax/popsatu/simpan.json',
			type: 'get',
			dataType:'json',
			data : $(this).serialize(),
			success:function(result){
				alert(result.kirimsayur);
				$('#modal-input').modal('hide');
				$("#vegetable").val(result.kirimsayur); 
				
			}
			
		});
		return false;
	}); 


	$("#biodata").on('click',function(){ 
		$.ajax({
			url: 'ajax/biodata.html',
			type: 'get',
			dataType:'html',
			success:function(result){
				$('#modal-input').find('.modal-body').html(result);
				$('#modal-input').modal('show');
			}
			
		});
	}); 


	$('#modal-input').on('submit', '#form-biodata', function() {
		$.ajax({
			url : 'ajax/biodata/simpan.json',
			type : 'get',
			dataType : 'json',
			data : $(this).serialize(),
			success : function(result) {
				$('#modal-input').modal('hide');
				$('#bootcamp').html(result);
			}
		});
		return false;
	});

	
////////////////////////////////////////////////////////////////////////
	$("#form-bootcamp").on('click', '#bc', function(){ 
		$.ajax({
			url: 'ajax/bootcamp.html',
			type: 'get',
			dataType:'html',
			success:function(result){
				$('#modal-input1').find('.modal-body').html(result);
				$('#modal-input1').modal('show');
			}
			
		});
	}); 


	$("#modal-input1").on('submit','#form-bootcamp',function(){ 
		$.ajax({
			url: 'ajax/bootcamp/simpan.json',
			type: 'get',
			dataType:'json',
			data : $(this).serialize(),
			success:function(result){
				$('#modal-input1').modal('hide');
				$("#bc").val(result.kirimBootcamp); 
				$('#modal-input').modal('show');
			}
			
		});
		return false;
	}); 

	$("#mamin").on('click',function(){ 
		$.ajax({
			url: 'ajax/poptiga.html',
			type: 'get',
			dataType:'html',
			success:function(result){
				$('#modal-input').find('.modal-body').html(result);
				$('#modal-input').modal('show');
			}
			
		});
	}); 
	
</script>

