<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h1>Tambah Fakultas</h1>
	
	<form action="#" method="get" id="form-fakultas-add">
		<table>
			<tr>
				<td>Kode Fakultas</td>
				<td><input type="text" id="kodeFakultas" name="kodeFakultas" value="${kodeFakultasAuto}"/></td>
			</tr>
			<tr>
				<td>Nama Fakultas</td>
				<td><input type="text" id="namaFakultas" name="namaFakultas"/></td>
			</tr>
			<tr>
				<td>Lokasi Fakultas</td>
				<td>
					<select id="idLokasi" name="idLokasi">
						<c:forEach items="${lokasiModelList}" var="lokasiModel">
							<option value="${lokasiModel.idLokasi}">${lokasiModel.namaLokasi}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><button type="submit">Create</button></td>
			</tr>
		</table>
	</form>
</div>