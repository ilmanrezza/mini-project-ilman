package com.spring.cinema187.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Columns;

//membuat table otomatis menggunakan hibernate

@Entity  //menandakan entitas
@Table(name="T_KARYAWAN")
public class KaryawanModel {
	private String nik;
	private String nama;
	private String noTelepon;
	private Integer tinggiBadan;
	private Integer beratBadan;
	
	@Id // primary key
	@Column(name = "NIK")
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	
	@Column(name = "NAMA")
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	@Column(name = "NOMOR_TELEPON")
	public String getNoTelepon() {
		return noTelepon;
	}
	public void setNoTelepon(String noTelepon) {
		this.noTelepon = noTelepon;
	}
	
	@Column(name = "TINGGI_BADAN")
	public Integer getTinggiBadan() {
		return tinggiBadan;
	}
	public void setTinggiBadan(Integer tinggiBadan) {
		this.tinggiBadan = tinggiBadan;
	}
	
	@Column(name = "BERAT_BADAN")
	public Integer getBeratBadan() {
		return beratBadan;
	}
	public void setBeratBadan(Integer beratBadan) {
		this.beratBadan = beratBadan;
	}
	
}