<h4>Halaman Ubah Mahasiswa</h4>
<br/>
<div class="form-horizontal">
	<form action="#" method="get" id="form-mahasiswa-edit">
		
		<input type="hidden" name="idMahasiswa" id="idMahasiswa" value="${mahasiswaModel.idMahasiswa}">
		
		<div class="form-group">
			<label class="control-label col-md-3">NIM</label>
			<input type="text" class="form-input" name="nimMahasiswa_Disabled" id="nimMahasiswa_Disabled" value="${mahasiswaModel.nimMahasiswa}" disabled="disabled"/>
			<input type="text" name="nimMahasiswa" id="nimMahasiswa" value="${mahasiswaModel.nimMahasiswa}">
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama</label>
			<input type="text" class="form-input" name="namaMahasiswa" id="namaMahasiswa" value="${mahasiswaModel.namaMahasiswa}" />
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Telepon</label>
			<input type="text" class="form-input" name="teleponMahasiswa" id="teleponMahasiswa" value="${mahasiswaModel.teleponMahasiswa}" />
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Gender</label>
			<input type="radio" class="form-input" name="genderMahasiswa" id="pria" value="pria"/>Pria 
			<input type="radio" class="form-input" name="genderMahasiswa" id="wanita" value="wanita"/>Wanita
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Alamat</label>
			<textarea rows="5" cols="30" name="alamatMahasiswa" id="alamatMahasiswa" >${mahasiswaModel.alamatMahasiswa}</textarea>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Tahun Lahir</label>
			<input type="text" class="form-input" name="tahunLahirMahasiswa" id="tahunLahirMahasiswa" value="${mahasiswaModel.tahunLahirMahasiswa}" />
		</div>
		
		<div class="modal-footer">
			<button type="submit" class="btn btn-warning">Ubah</button>
		</div>
	</form>
</div>

