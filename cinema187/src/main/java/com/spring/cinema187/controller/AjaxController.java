package com.spring.cinema187.controller;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AjaxController {
	@RequestMapping(value = "ajax")
	public String ajax() {
		String jsp = "ajax/home";
		return jsp;
	}
	
	@RequestMapping(value="ajax/popsatu")
	public String ajaxPopSatu(Model model) {
		String buah = "Pisang";
		model.addAttribute("buah", buah);
		String jsp = "ajax/popsatu";
		return jsp;
	}
	
	@RequestMapping(value="ajax/popdua")
	public String ajaxPopDua() {
		String jsp = "ajax/popdua";
		return jsp;
	}
	
	@RequestMapping(value="ajax/popsatu/simpan")
	public String simpan(Model model, HttpServletRequest request) {
		String sayur = request.getParameter("sayur");
		System.out.println(sayur);
		model.addAttribute("kirimsayur", sayur);
		
		String jsp = "ajax/home";
		return jsp;
	}
	
	
	
	
	@RequestMapping(value="ajax/biodata")
	public String biodata() {
		String jsp = "ajax/biodata";
		return jsp;
	}
	
	@RequestMapping(value="ajax/biodata/simpan")
	public String biodataSimpan(HttpServletRequest request) {
		String nama = request.getParameter("nama");
		String kampus = request.getParameter("kampus");
		String gender = request.getParameter("gender");
		String alamat = request.getParameter("alamat");
		String bootcamp = request.getParameter("bc");
		System.out.println(nama);
		System.out.println(kampus);
		System.out.println(gender);
		System.out.println(bootcamp);
		System.out.println(alamat);
		
		String jsp = "ajax/home";
		return jsp;
	}
	
	
	@RequestMapping(value="ajax/bootcamp")
	public String bootcamp() {
		String jsp = "ajax/bootcamp";
		return jsp;
	}

	@RequestMapping(value="ajax/bootcamp/simpan")
	public String bootcampSimpan(Model model, HttpServletRequest request) {
		String bootcamp = request.getParameter("bootcamp");
		model.addAttribute("kirimBootcamp", bootcamp);
		String jsp = "ajax/biodata";
		return jsp;
	}
	
	@RequestMapping(value="ajax/poptiga")
	public String popTiga() {
		String jsp = "ajax/poptiga";
		return jsp;
	}
	
}
