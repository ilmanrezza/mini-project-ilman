<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Lihat Genre</h1>
	<form action="#" method="get" id="form-genre-detail">
		<table>
			<tr>
				<td>Kode Genre</td>
				<td><input type="text" id="kodeGenre" name="kodeGenre" value = "${genreModel.kodeGenre}"/></td>
			</tr>
			<tr>
				<td>Nama Genre</td>
				<td><input type="text" id="namaGenre"name="namaGenre" value = "${genreModel.namaGenre}"/></td>
			</tr>
		</table>
	</form>
</div>

<script type = "text/javascript">

</script>