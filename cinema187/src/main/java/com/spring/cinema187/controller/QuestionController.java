package com.spring.cinema187.controller;

import java.io.File;
import java.util.Date;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.spring.cinema187.model.QuestionModel;
import com.spring.cinema187.service.QuestionService;

@Controller
public class QuestionController extends UserController {
	
	
	@Autowired 
	QuestionService questionService;
	
	@RequestMapping(value = "question")
	public String question() {
		String jsp = "question/question";
		return jsp;
	}
	
	@RequestMapping(value = "question/add")
	public String questionAdd()
	{
		String jsp = "question/add";
		return jsp;
	}
	
	@RequestMapping(value = "question/create", method=RequestMethod.GET)
	public String questionCreate(@RequestParam("file") MultipartFile imageUrl, HttpServletRequest request) {
		String question_type = request.getParameter("question_type");
		String question = request.getParameter("question");
		
		//File imageUrl = request.getParameter("imageUrl");
		//String imageUrl = file.getName()+file.getContentType();
		String name = imageUrl.getName();
		String ori = imageUrl.getOriginalFilename();
		String ext = imageUrl.getContentType();
		QuestionModel questionModel = new QuestionModel();
		//save
			// dari user
			Integer idCreatedBy = this.userSearch().getIdUser();
			questionModel.setIdCreatedBy(idCreatedBy);
			questionModel.setCreatedOn(new Date());
			// set data ke table question
			questionModel.setQuestion_type(question_type);
			questionModel.setQuestion(question);
			//questionModel.setImage_url(imageUrl);
			questionModel.setIsDelete(false);
		this.questionService.create(questionModel);
			
		String jsp = "question/question";
		return jsp;
	}
	
	
}
