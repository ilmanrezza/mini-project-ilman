<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Divisi</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah
		Divisi</button>
	
	<div>
		<table class="table" id="tbl-divisi">
			<tr>
				<td>No</td>
				<td>ID Divisi</td>
				<td>Kode Divisi</td>
				<td>Nama Divisi</td>
			</tr>
			<tbody id="list-divisi">

			</tbody>
		</table>
	</div>
		
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Divisi</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	loadListDivisi();

	function loadListDivisi() {
		$.ajax({
			url : 'divisi/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-divisi').html(result);
			}
		});
	}

//document ready itu setelah halamannya dipanggil
$(document).ready(function() {

	//fungsi ajax untuk popup tambah
	$('#btn-add').on('click', function() {
		$.ajax({
			url : 'divisi/add.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#modal-input').find('.modal-body').html(result);
				$('#modal-input').modal('show');
				loadListDivisi();
			}
		});
	});
	//akhir fungsi ajax untuk popup tambah

	//fungsi ajax untuk create tambah
	$('#modal-input').on('submit', '#form-divisi-add', function() {
		$.ajax({
			url : 'divisi/create.json',
			type : 'get',
			dataType : 'json',
			data : $(this).serialize(),
			success : function(result) {

				$('#modal-input').modal('hide');
				alert("data telah ditambah!");
				loadListDivisi();
			}
		});
		return false;
	});
	//akhir fungsi ajax untuk create tambah

	//fungsi ajax untuk popup edit
	$('#list-divisi').on('click','#btn-edit',function(){
		var idDivisi = $(this).val();
		$.ajax({
			url : 'divisi/edit.html',
			type : 'get',
			dataType : 'html',
			data:{idDivisi:idDivisi},
			success : function(result) {
				$('#modal-input').find('.modal-body').html(result);
				$('#modal-input').modal('show');
			}
		});
	});
	//akhir fungsi ajax untuk popup edit

	//fungsi ajax untuk update
	$('#modal-input').on('submit','#form-divisi-edit',function(){
		
		$.ajax({
			url:'divisi/update.json',
			type:'get',
			dataType:'json',
			data:$(this).serialize(),
			success: function(result){
				$('#modal-input').modal('hide');
				alert("data telah diubah!");
				loadListDivisi();
			}
		});
		return false;
	});
	//akhir fungsi ajax untuk update



});
//akhir dokumen ready

</script>