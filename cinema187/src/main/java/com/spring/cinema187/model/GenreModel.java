package com.spring.cinema187.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_GENRE")
public class GenreModel {
	private String kodeGenre;
	private String namaGenre;
	
	@Id
	@Column(name="KD_GENRE")
	public String getKodeGenre() {
		return kodeGenre;
	}
	public void setKodeGenre(String kodeGenre) {
		this.kodeGenre = kodeGenre;
	}

	@Column(name="NM_GENRE")
	public String getNamaGenre() {
		return namaGenre;
	}
	public void setNamaGenre(String namaGenre) {
		this.namaGenre = namaGenre;
	}
	
	
	



}
