<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Lihat Produser</h1>
	<form action="#" method="get" id="form-produser-detail">
		<table>
			<tr>
				<td>Kode Produser</td>
				<td><input type="text" id="kodeProduser" name="kodeProduser" value = "${produserModel.kodeProduser}"/></td>
			</tr>
			<tr>
				<td>Nama Produser</td>
				<td><input type="text" id="namaProduser"name="namaProduser" value = "${produserModel.namaProduser}"/></td>
			</tr>
			<tr>
				<td>International</td>
				<td><input type="text" id="international"name="international" value = "${produserModel.international}"/></td>
			</tr>
		</table>
	</form>
</div>

<script type = "text/javascript">

</script>