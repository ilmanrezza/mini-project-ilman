package com.spring.cinema187.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.cinema187.model.FakultasModel;
import com.spring.cinema187.model.LokasiModel;
import com.spring.cinema187.service.FakultasService;
import com.spring.cinema187.service.LokasiService;

@Controller
public class FakultasController extends UserController{
	
	@Autowired
	private FakultasService fakultasService;
	
	@Autowired
	private LokasiService lokasiService;
	
	@RequestMapping(value="fakultas") // url action
	public String fakultas() { // method 
		String jsp ="fakultas/fakultas"; // target atau halaman
		return jsp;
	}
	
	@RequestMapping(value="fakultas/cruds/add")
	public String fakultasAdd(Model model) {
		String kodeFakultasAuto = this.generateKodeFakultas();
		model.addAttribute("kodeFakultasAuto", kodeFakultasAuto);
		
		//tampilkan lokasi untuk select option
		this.lokasiList(model);
		
		String jsp ="fakultas/cruds/add";
		return jsp;
	}
	
	@RequestMapping(value="fakultas/cruds/create")
	public String fakultasCreate(HttpServletRequest request, Model model) {
		
		// get nilai dari variable name di jsp
		String kodeFakultas = request.getParameter("kodeFakultas");
		String namaFakultas = request.getParameter("namaFakultas");
		//get join table
		//Integer idLokasi = Integer.valueOf(request.getParameter("idLokasi"));
		
		// set nilai ke variable di modelnya
		FakultasModel fakultasModel = new FakultasModel();
		fakultasModel.setKodeFakultas(kodeFakultas);
		fakultasModel.setNamaFakultas(namaFakultas);
		fakultasModel.setIsDelete(0); // 0 artinya tidak didelete
		
		//set join table
		//fakultasModel.setIdLokasi(idLokasi);
		
		//panggil sebuah method cek kode ada g di DB
		Integer jumlahDataFakultas = this.cekNamaFakultas(namaFakultas);
		
		//save audit trail untuk created
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		fakultasModel.setxIdCreatedBy(xIdCreatedBy);
		fakultasModel.setxCreatedDate(new Date());
		
		if (jumlahDataFakultas > 0) {
			model.addAttribute("jumlahDataFakultas", jumlahDataFakultas);
			model.addAttribute("namaFakultas", namaFakultas);
		} else {
			this.fakultasService.create(fakultasModel);
		}
		
		String jsp ="fakultas/fakultas";
		return jsp;
	}
	
	@RequestMapping(value="fakultas/cruds/list")
	public String fakultasList(Model model) {
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		fakultasModelList = this.fakultasService.search(kodeRole);
		model.addAttribute("fakultasModelList",fakultasModelList);
		String jsp ="fakultas/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="fakultas/cruds/detail")
	public String fakultasDetail(HttpServletRequest request, Model model) {
		Integer idFakultas =Integer.valueOf(request.getParameter("idFakultas"));
		
		FakultasModel fakultasModel = new FakultasModel();
		fakultasModel = this.fakultasService.searchId(idFakultas);
		
		model.addAttribute("fakultasModel", fakultasModel);
		
		String var1 = "JAKARTA";
		model.addAttribute("var2", var1);
		
		//tampilkan lokasi untuk select option
		this.lokasiList(model);
		
		String jsp ="fakultas/cruds/detail";
		return jsp;
	}
	
	@RequestMapping(value="fakultas/cruds/edit")
	public String fakultasEdit(HttpServletRequest request, Model model) {
		Integer idFakultas =Integer.valueOf(request.getParameter("idFakultas"));
		
		FakultasModel fakultasModel = new FakultasModel();
		fakultasModel = this.fakultasService.searchId(idFakultas);
		
		model.addAttribute("fakultasModel", fakultasModel);
		
		//tampilkan lokasi untuk select option
		this.lokasiList(model);
				
		String jsp ="fakultas/cruds/edit";
		return jsp;
	}
	
	@RequestMapping(value="fakultas/cruds/update")
	public String fakultasUpdate(HttpServletRequest request) {
		
		// get nilai dari variable name di jsp
		Integer idFakultas =Integer.valueOf(request.getParameter("idFakultas"));
		String kodeFakultas = request.getParameter("kodeFakultas");
		String namaFakultas = request.getParameter("namaFakultas");
		//get join table
		//Integer idLokasi = Integer.valueOf(request.getParameter("idLokasi"));
		
		String varBackEnd = request.getParameter("varJSP");
		System.out.println(varBackEnd);
		
		// set nilai ke variable di modelnya
		FakultasModel fakultasModel = new FakultasModel();
		
		fakultasModel = this.fakultasService.searchId(idFakultas);
		
		fakultasModel.setKodeFakultas(kodeFakultas);
		fakultasModel.setNamaFakultas(namaFakultas);
		//set join table
		//fakultasModel.setIdLokasi(idLokasi);
		
		//save audit trail untuk updated
		Integer xIdUpdatedBy = this.userSearch().getIdUser();
		fakultasModel.setxIdUpdatedBy(xIdUpdatedBy);
		fakultasModel.setxUpdatedDate(new Date());
		
		//simpan data
		this.fakultasService.update(fakultasModel);
		
		String jsp ="fakultas/fakultas";
		return jsp;
	}
	
	@RequestMapping(value="fakultas/cruds/remove")
	public String fakultasRemove(HttpServletRequest request, Model model) {
		Integer idFakultas =Integer.valueOf(request.getParameter("idFakultas"));
		
		FakultasModel fakultasModel = new FakultasModel();
		fakultasModel = this.fakultasService.searchId(idFakultas);
		
		model.addAttribute("fakultasModel", fakultasModel);
		
		//tampilkan lokasi untuk select option
		this.lokasiList(model);
		
		String jsp ="fakultas/cruds/remove";
		return jsp;
	}
	
	@RequestMapping(value="fakultas/cruds/delete")
	public String fakultasDelete(HttpServletRequest request) {
		
		// get nilai dari variable name di jsp
		Integer idFakultas =Integer.valueOf(request.getParameter("idFakultas"));
		
		// set nilai ke variable di modelnya
		FakultasModel fakultasModel = new FakultasModel();
		
		fakultasModel = this.fakultasService.searchId(idFakultas);
		
		fakultasModel.setIsDelete(1); // status 1 artinya didelete
		
		//save audit trail untuk deleted
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		fakultasModel.setxIdDeletedBy(xIdDeletedBy);
		fakultasModel.setxDeletedDate(new Date());
		
		//simpan data
		this.fakultasService.deleteTemporary(fakultasModel);
		
		String jsp ="fakultas/fakultas";
		return jsp;
	}
	
	@RequestMapping(value="fakultas/cruds/search/nama")
	public String fakultasSearchNama(HttpServletRequest request, Model model) {
		String namaFakultas = request.getParameter("namaFakultasKey");
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		fakultasModelList = this.fakultasService.searchNama(namaFakultas);
		model.addAttribute("fakultasModelList", fakultasModelList);
		String jsp ="fakultas/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="fakultas/cruds/search/kode")
	public String fakultasSearchKode(HttpServletRequest request, Model model) {
		String kodeFakultas = request.getParameter("kodeFakultasKey");
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		fakultasModelList = this.fakultasService.searchKode(kodeFakultas);
		model.addAttribute("fakultasModelList", fakultasModelList);
		String jsp ="fakultas/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="fakultas/cruds/search")
	public String fakultasSearch(HttpServletRequest request, Model model) {
		String kodeFakultas = request.getParameter("keyword");
		String namaFakultas = request.getParameter("keyword");
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		fakultasModelList = this.fakultasService.searchKodeOrNama(kodeFakultas, namaFakultas);
		
		model.addAttribute("fakultasModelList", fakultasModelList);
		String jsp ="fakultas/cruds/list";
		return jsp;
	}
	
	public Integer cekKodeFakultas(String kodeFakultas) {
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		fakultasModelList = this.fakultasService.searchKodeEqual(kodeFakultas);
		Integer jumlahDataFakultas = 0;
		if (fakultasModelList == null) {
			jumlahDataFakultas = 0;
		} else {
			jumlahDataFakultas = fakultasModelList.size();
		}
		return jumlahDataFakultas;
	}
	
	
	public Integer cekNamaFakultas(String namaFakultas) {
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		fakultasModelList = this.fakultasService.searchNamaEqual(namaFakultas);
		Integer jumlahDataFakultas = 0;
		if (fakultasModelList == null) {
			jumlahDataFakultas = 0;
		} else {
			jumlahDataFakultas = fakultasModelList.size();
		}
		return jumlahDataFakultas;
	}
	
	public String generateKodeFakultas() {
		String kodeFakultasAuto = "FK";
		
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		fakultasModelList = this.fakultasService.search(kodeRole);
		Integer jumlahDataFakultas = 0;
		if (fakultasModelList == null) {
			jumlahDataFakultas = 0;
		} else {
			jumlahDataFakultas = fakultasModelList.size();
		}
		
		kodeFakultasAuto = kodeFakultasAuto + "000" + jumlahDataFakultas;
		
		return kodeFakultasAuto;
	}
	
	public void lokasiList(Model model) {
		List<LokasiModel> lokasiModelList = new ArrayList<LokasiModel>();
		lokasiModelList = this.lokasiService.search();
		model.addAttribute("lokasiModelList", lokasiModelList);
	}

}
