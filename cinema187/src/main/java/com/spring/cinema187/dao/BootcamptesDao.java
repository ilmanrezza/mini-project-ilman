package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.BootcamptesModel;

public interface BootcamptesDao {
	public void create(BootcamptesModel bootcamptesModel);
	public void update(BootcamptesModel bootcamptesModel);
	public void delete(BootcamptesModel bootcamptesModel);
	public List<BootcamptesModel> searchAll();
	public BootcamptesModel searchId(Integer id);
	public List<BootcamptesModel> name(String name);
	/* public List<BootcamptesModel> search(String kodeRole); */
}
