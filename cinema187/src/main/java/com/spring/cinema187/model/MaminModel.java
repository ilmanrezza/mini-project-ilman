package com.spring.cinema187.model;

public class MaminModel {
	
	private String makan;
	private String minum;
	
	public String getMakan() {
		return makan;
	}
	public void setMakan(String makan) {
		this.makan = makan;
	}
	public String getMinum() {
		return minum;
	}
	public void setMinum(String minum) {
		this.minum = minum;
	}
}
