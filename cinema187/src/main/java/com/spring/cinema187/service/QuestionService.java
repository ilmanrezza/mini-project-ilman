package com.spring.cinema187.service;

import java.util.List;

import com.spring.cinema187.model.QuestionModel;

public interface QuestionService {

	public void create(QuestionModel questionModel);
	public void update(QuestionModel questionModel);
	public List<QuestionModel> searchAll();
	public QuestionModel searchIdQuestion(Integer idQuestion);
	public List<QuestionModel> searchQuestion(String question);
	public void deleteTemporary(QuestionModel questionModel);
}
