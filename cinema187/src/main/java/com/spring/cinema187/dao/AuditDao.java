package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.AuditModel;

public interface AuditDao {
	public void create(AuditModel auditModel);
	public void update(AuditModel auditModel);
	public void delete(AuditModel auditModel);
	public List<AuditModel> searchAll();
}
