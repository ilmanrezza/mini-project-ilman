package com.spring.cinema187.service;

import java.util.List;

import com.spring.cinema187.model.AuditModel;


public interface AuditService {

	public void create(AuditModel auditModel);
	public void update(AuditModel auditModel);
	public void delete(AuditModel auditModel);
	public List<AuditModel> searchAll();
	public void deleteTemporary(AuditModel auditModel);
}
