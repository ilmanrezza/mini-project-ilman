<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Tambah Genre</h1>

	<form action="#" method="get" id="form-genre-add">
		<table>
			<tr>
				<td>Kode Genre</td>
				<td><input type="text" id="kodeGenre" name="kodeGenre"/></td>
			</tr>
			<tr>
				<td>Nama Genre</td>
				<td><input type="text" id="namaGenre"name="namaGenre"/></td>
			</tr>
			<tr>
				<td>Negara</td>
				<td><select>
					<c:forEach items="${negaraModelList}" var="negaraModel" varStatus="number">
						<option>${negaraModel.namaNegara}</option>
					</c:forEach>
				</select></td>
			</tr>

		</table>
		<button type="submit">Simpan</button>
	</form>
</div>

