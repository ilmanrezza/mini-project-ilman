package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.AnggotaModel;

public interface AnggotaDao {

	public void create(AnggotaModel anggotaModel);

	public List<AnggotaModel> searchAll();

}
