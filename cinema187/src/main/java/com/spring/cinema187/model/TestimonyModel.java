package com.spring.cinema187.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_TESTIMONY")
public class TestimonyModel {
		private Integer id;
		private String title;
		private String content;
		
		// VARIABLE audit log
		private Integer idCreatedBy;
		private UserModel createdBy;
		private Date createdOn;
		
		private Integer idModifiedBy;
		private UserModel modifiedBy;
		private Date modifiedOn;
		
		private Integer idDeletedBy;
		private UserModel deletedBy;
		private Date deletedOn;
		private Boolean isDelete;
	 // VARIABLE audit log
		
		
		
		@Id
		@Column(name="ID",length=11,nullable=false) //nama kolom
			@GeneratedValue(strategy=GenerationType.TABLE, generator="T_TESTIMONY") // utk buat nilai sequential
			@TableGenerator(name="T_TESTIMONY", table="M_SEQUENCE",
							pkColumnName="SEQUENCE_NAME", pkColumnValue="ID",
							valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)

		
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		
		@Column(name="title",length=255,nullable=false)
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		
		@Column(name="content",length=5000,nullable=false)
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		@Column(name="CREATED_BY",length=11,nullable=false)
		public Integer getIdCreatedBy() {
			return idCreatedBy;
		}
		public void setIdCreatedBy(Integer idCreatedBy) {
			this.idCreatedBy = idCreatedBy;
		}

		
	@ManyToOne
	@JoinColumn(name="CREATED_BY", nullable=false, updatable=false, insertable=false)
		public UserModel getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(UserModel createdBy) {
			this.createdBy = createdBy;
		}
		
	@Column(name="CREATED_ON",nullable=false)
		public Date getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}
	@Column(name="MODIFIED_BY",length=11,nullable=true)
		public Integer getIdModifiedBy() {
			return idModifiedBy;
		}
		public void setIdModifiedBy(Integer idModifiedBy) {
			this.idModifiedBy = idModifiedBy;
		}
		
	@ManyToOne
	@JoinColumn(name="MODIFIED_BY", nullable=true, updatable=false, insertable=false)
		public UserModel getModifiedBy() {
			return modifiedBy;
		}
		public void setModifiedBy(UserModel modifiedBy) {
			this.modifiedBy = modifiedBy;
		}
	@Column(name="MODIFIED_ON",nullable=true)
		public Date getModifiedOn() {
			return modifiedOn;
		}
		public void setModifiedOn(Date modifiedOn) {
			this.modifiedOn = modifiedOn;
		}

	@Column(name="DELETED_BY",length=11,nullable=true)	
		public Integer getIdDeletedBy() {
			return idDeletedBy;
		}
		public void setIdDeletedBy(Integer idDeletedBy) {
			this.idDeletedBy = idDeletedBy;
		}
	@ManyToOne
	@JoinColumn(name="DELETED_BY", nullable=true, updatable=false, insertable=false)
		public UserModel getDeletedBy() {
			return deletedBy;
		}
		public void setDeletedBy(UserModel deletedBy) {
			this.deletedBy = deletedBy;
		}
	@Column(name="DELETED_ON",nullable=true)
		public Date getDeletedOn() {
			return deletedOn;
		}
		public void setDeletedOn(Date deletedOn) {
			this.deletedOn = deletedOn;
		}
	@Column(name="IS_DELETED",nullable=false)
		public Boolean getIsDelete() {
			return isDelete;
		}
		public void setIsDelete(Boolean isDelete) {
			this.isDelete = isDelete;
		}

		
		
		
}
