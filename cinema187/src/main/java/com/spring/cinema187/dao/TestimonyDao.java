package com.spring.cinema187.dao;

import java.util.List;

import com.spring.cinema187.model.TestimonyModel;

public interface TestimonyDao {
	
	public void create(TestimonyModel testimonyModel);
	public void update(TestimonyModel testimonyModel);
	public List<TestimonyModel> searchAll();
	public TestimonyModel searchId(Integer id);
	public List<TestimonyModel> searchTitle(String title);
}
