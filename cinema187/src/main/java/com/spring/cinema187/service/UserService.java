package com.spring.cinema187.service;

import com.spring.cinema187.model.UserModel;

public interface UserService {

	public UserModel searchUsernamePassword(String username, String password);
}
