<br/><br/>

<div class="panel" style="background: white;min-height: 800px;">
	<div class="box-header">
		<h1 class="box-title">Halaman Mahasiswa</h1> 
		<br/><br/>
		<button type="button" class="btn btn-info" id="btn-tambah">Tambah</button>
	</div>
	
	<!-- part table list mahasiswa -->
	<table class="table" id="tbl-mahasiswa">
		<thead>
			<tr>
				<td> </td>
				<td>
					<form action="#" method="get" id="form-mahasiswa-search-nim">
						<input type="text" name="keywordNim" id="keywordNim"/><br/>
						<button type="submit">Cari</button>
					</form>
				</td> 
				<td>
					<form action="#" method="get" id="form-mahasiswa-search-nim">
						<input type="text" name="keywordNim" id="keywordNim"/><br/>
						<button type="submit">Cari</button>
					</form>
				</td>
				<td>Action</td>
			</tr>
			<tr>
				<td>No.</td>
				<td>NIM</td>
				<td>Nama</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody id="list-data-mahasiswa">
			
		</tbody>
	</table>
	<!-- part table list mahasiswa -->
	
</div>

<!-- part modal -> show form -->
<div class="modal fade" id="modal-input">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4>Form Mahasiswa</h4>
			</div>
			
			<div class="modal-body">
				<!-- tempat popup jsp-nya -->
			</div>
		</div>
	</div>
</div>
<!-- part modal -> show form -->

<!-- part jquery -->
<script>

	function loadDataMahasiswaList() {
		$.ajax({
			url:'mahasiswa/list.html',
			type:'get',
			dataType:'html',
			success: function(result) {
				$('#list-data-mahasiswa').html(result);
			}
			
		});
	}
	
	//script otomatis dijalankan saat mahasiswa.jsp dishow
	loadDataMahasiswaList();
	
	$(document).ready(function() {
		
		$('#btn-tambah').on('click', function(){
			$.ajax({
				url:'mahasiswa/tambah.html',
				type:'get',
				dataType:'html',
				success: function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		
		$('#modal-input').on('submit', '#form-mahasiswa-add', function(){
			$.ajax({
				url:'mahasiswa/save_add.json',
				type:'get',
				dataType:'json', // untuk kirim data
				data:$(this).serialize(), //serialize akan ambil sluruh input di formnya
				success: function(result) {
					if (result.teleponSama =='ya') {
						alert("tidak bisa simpan karena telp "+result.teleponMahasiswa+" sudah ada di DB ");
					} else {
						$('#modal-input').modal('hide');
						alert("data mahasiswa dgn NIM "+result.mahasiswaModel.nimMahasiswa+" tersimpan, hacked by "+result.programmer+" ");
						loadDataMahasiswaList();
					}
					
					
				}
			});
			
			return false //agar halaman ga kerefresh
		});
		
		
		$('#list-data-mahasiswa').on('click', '#btn-detil',function(){
			var idMahasiswaBtnDetil = $(this).val(); 
			$.ajax({
				url:'mahasiswa/detil.html',
				type:'get',
				dataType:'html',
				data:{idMahasiswa: idMahasiswaBtnDetil},
				success: function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		
		
		$('#list-data-mahasiswa').on('click', '#btn-hapus',function(){
			var idMahasiswaBtnHapus = $(this).val(); 
			$.ajax({
				url:'mahasiswa/hapus.html',
				type:'get',
				dataType:'html',
				data:{idMahasiswa: idMahasiswaBtnHapus},
				success: function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		
		
		$('#modal-input').on('submit', '#form-mahasiswa-delete', function(){
			$.ajax({
				url:'mahasiswa/save_delete.json',
				type:'get',
				dataType:'json', // untuk kirim data
				data:$(this).serialize(), //serialize akan ambil sluruh input di formnya
				success: function(result) {
					$('#modal-input').modal('hide');
					alert("data mahasiswa terhapus, yahhh T_T #2019gantilaptop ");
					loadDataMahasiswaList();
				}
			});
			
			return false //agar halaman ga kerefresh
		});
		
		$('#list-data-mahasiswa').on('click', '#btn-ubah',function(){
			var idMahasiswaBtnUbah = $(this).val(); 
			$.ajax({
				url:'mahasiswa/ubah.html',
				type:'get',
				dataType:'html',
				data:{idMahasiswa: idMahasiswaBtnUbah},
				success: function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		
		$('#modal-input').on('submit', '#form-mahasiswa-edit', function(){
			$.ajax({
				url:'mahasiswa/save_edit.json',
				type:'get',
				dataType:'json', // untuk kirim data
				data:$(this).serialize(), //serialize akan ambil sluruh input di formnya
				success: function(result) {
					$('#modal-input').modal('hide');
					alert("data mahasiswa terkini, saatnya moveon ^_^ #xsiswinners ");
					loadDataMahasiswaList();
				}
			});
			
			return false //agar halaman ga kerefresh
		});
		
		$('#form-mahasiswa-search-nim').on('submit', function(){
			$.ajax({
				url:'mahasiswa/search_nim.html',
				type:'get',
				dataType:'html', // untuk kirim data
				data:$(this).serialize(), //serialize akan ambil sluruh input di formnya
				success: function(result) {
					//alert("data mahasiswa tercyduq karena oleh sebab NIM");
					$('#list-data-mahasiswa').html(result);
				}
			});
			
			return false; //agar halaman ga kerefresh
		});
		
	});
	
</script>
<!-- part jquery -->