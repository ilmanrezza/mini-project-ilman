<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h1>Lihat Fakultas</h1>
	
	<form action="#" method="get" id="form-fakultas-detail">
		<table>
			<tr>
				<td>Kode Fakultas</td>
				<td><input type="text" id="kodeFakultas" name="kodeFakultas" value="${fakultasModel.kodeFakultas}"/></td>
			</tr>
			<tr>
				<td>Nama Fakultas</td>
				<td><input type="text" id="namaFakultas" name="namaFakultas" value="${fakultasModel.namaFakultas}"/></td>
			</tr>
			<tr>
				<td>Kota</td>
				<td>${var2}</td>
			</tr>
			<tr>
				<td>Lokasi Fakultas</td>
				<td>
					<select id="idLokasi" name="idLokasi">
						<c:forEach items="${lokasiModelList}" var="lokasiModel">
							<option value="${lokasiModel.idLokasi}" 
								${lokasiModel.idLokasi==fakultasModel.idLokasi ? 'selected="true"':''}>
								${lokasiModel.namaLokasi}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td>Created By</td>
				<td>${fakultasModel.xCreatedBy.username}</td>
			</tr>
			<tr>
				<td>Created Date</td>
				<td>${fakultasModel.xCreatedDate}</td>
			</tr>
			<tr>
				<td>Updated By</td>
				<td>${fakultasModel.xUpdatedBy.username}</td>
			</tr>
			<tr>
				<td>Created Date</td>
				<td>${fakultasModel.xUpdatedDate}</td>
			</tr>
			<tr>
				<td>Deleted By</td>
				<td>${fakultasModel.xDeletedBy.username}</td>
			</tr>
			<tr>
				<td>Deleted Date</td>
				<td>${fakultasModel.xDeletedDate}</td>
			</tr>
		</table>
	</form>
</div>