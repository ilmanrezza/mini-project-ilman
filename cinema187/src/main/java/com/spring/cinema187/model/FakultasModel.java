package com.spring.cinema187.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_FAKULTAS")
public class FakultasModel {
	
	private Integer idFakultas; //PK
	private String kodeFakultas;
	private String namaFakultas;
	private Integer isDelete;
	
	//join table LokasiModel
	private Integer idLokasi; //FK dari LokasiModel
	private LokasiModel lokasiModel; 
	
	//Audit Trail
	private Integer xIdCreatedBy;
	private UserModel xCreatedBy;
	private Date xCreatedDate;
	
	private Integer xIdUpdatedBy;
	private UserModel xUpdatedBy;
	private Date xUpdatedDate;
	
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	//Audit Trail
	
	
	@Id
	@Column(name="ID_FAKULTAS") //nama kolom
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_FAKULTAS") // utk buat nilai sequential
	@TableGenerator(name="M_FAKULTAS", table="M_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_FAKULTAS",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdFakultas() {
		return idFakultas;
	}
	public void setIdFakultas(Integer idFakultas) {
		this.idFakultas = idFakultas;
	}
	
	@Column(name="KODE_FAKULTAS")
	public String getKodeFakultas() {
		return kodeFakultas;
	}
	public void setKodeFakultas(String kodeFakultas) {
		this.kodeFakultas = kodeFakultas;
	}
	
	@Column(name="NAMA_FAKULTAS")
	public String getNamaFakultas() {
		return namaFakultas;
	}
	public void setNamaFakultas(String namaFakultas) {
		this.namaFakultas = namaFakultas;
	}
	
	@Column(name="IS_DELETE")
	public Integer getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
	
	@Column(name="ID_LOKASI")
	public Integer getIdLokasi() {
		return idLokasi;
	}
	public void setIdLokasi(Integer idLokasi) {
		this.idLokasi = idLokasi;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_LOKASI", nullable=true, updatable=false, insertable=false)
	public LokasiModel getLokasiModel() {
		return lokasiModel;
	}
	public void setLokasiModel(LokasiModel lokasiModel) {
		this.lokasiModel = lokasiModel;
	}
	
	@Column(name="X_ID_CREATED")
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_CREATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name="X_CREATED_DATE")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name="X_ID_UPDATED")
	public Integer getxIdUpdatedBy() {
		return xIdUpdatedBy;
	}
	public void setxIdUpdatedBy(Integer xIdUpdatedBy) {
		this.xIdUpdatedBy = xIdUpdatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_UPDATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxUpdatedBy() {
		return xUpdatedBy;
	}
	public void setxUpdatedBy(UserModel xUpdatedBy) {
		this.xUpdatedBy = xUpdatedBy;
	}
	
	@Column(name="X_UPDATED_DATE")
	public Date getxUpdatedDate() {
		return xUpdatedDate;
	}
	public void setxUpdatedDate(Date xUpdatedDate) {
		this.xUpdatedDate = xUpdatedDate;
	}
	
	@Column(name="X_ID_DELETED")
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_DELETED", nullable=true, updatable=false, insertable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name="X_DELETED_DATE")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}
	
	

}
