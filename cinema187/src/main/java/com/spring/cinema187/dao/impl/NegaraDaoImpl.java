package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.NegaraDao;
import com.spring.cinema187.model.FakultasModel;
import com.spring.cinema187.model.NegaraModel;

@Repository
public class NegaraDaoImpl implements NegaraDao{
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(NegaraModel negaraModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //set start query
		session.save(negaraModel); //hibernate(framework) tabel (hql), kl di
		
	}

	@Override
	public List<NegaraModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<NegaraModel> negaraModelList = new ArrayList<NegaraModel>();
		String query = "from NegaraModel";
		String kondisi = " ";
		negaraModelList = session.createQuery(query + kondisi).list(); //hql: select from kl di sql
		return negaraModelList;
	}

	@Override
	public NegaraModel searchKode(String kodeNegara) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //set start query
		
		NegaraModel negaraModel = new NegaraModel();
		String query = "from NegaraModel";
		String kondisi = " where kodeNegara = '"+kodeNegara+"' "; //query	
		negaraModel = (NegaraModel) session.createQuery(query + kondisi).uniqueResult(); //hql: select from kl di sql
		return negaraModel;
	}

	@Override
	public void update(NegaraModel negaraModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(negaraModel);
		
	}

	@Override
	public void delete(NegaraModel negaraModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(negaraModel);
	}

	@Override
	public List<NegaraModel> searchKodeLike(String kodeNegara) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //set start query
		List<NegaraModel> negaraModelList = new ArrayList<NegaraModel>();
		String query = "from NegaraModel";
		String kondisi = " where kodeNegara like '%"+kodeNegara+"%' ";
		//select * from T_NEGARA where kodeNegara like '%kodeNegara%'
		negaraModelList = session.createQuery(query + kondisi).list(); //hql: select from kl di sql
		return negaraModelList;
	}

//	@Override
//	public NegaraModel searchNama(String namaNegara) {
//		// TODO Auto-generated method stub
//		//step 1. syntax start query
//		Session session = this.sessionFactory.getCurrentSession(); //set start query
//		
//		//step 2. instance berdasarkan output
//		// pola instance : ObjekModel objekModel = new ObjekModel(); 1 data
//		//pola instance : List<ObjekModel> objekModelList = new ArrayList<ObjekModel>(); > 1 data
//		NegaraModel negaraModel = new NegaraModel();
//		
//		//step 3. string query dan kondisi;
//		String query = "from NegaraModel";
//		String kondisi = " where namaNegara = '" +namaNegara+ "' ";
//		
//		//step 4. session hibernate nya;
//		//Insert into = session.save
//		//update = session.update
//		//delete = session.delete
//		//select = session.creatQuery
//		
//		//uniqueResult(); untuk satu data, ditambah casting objek
//		//list(); untuk > satu data, ditambah casting objek
//		negaraModel = (NegaraModel) session.createQuery(query + kondisi).uniqueResult();
//		
//		return negaraModel; // output negaraModel
//	}

	@Override
	public List<NegaraModel> searchNamaLike(String namaNegara) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //set start query
		List<NegaraModel> negaraModelList = new ArrayList<NegaraModel>();
		String query = " from NegaraModel ";
		String kondisi = " where namaNegara like '%"+namaNegara+"%' ";
		//select * from T_NEGARA where kodeNegara like '%kodeNegara%'
		negaraModelList = session.createQuery(query + kondisi).list(); //hql: select from kl di sql
		return negaraModelList;
	}
	
	@Override
	public List<NegaraModel> searchKodeOrNama(String kodeNegara, String namaNegara) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		List<NegaraModel> negaraModelList = new ArrayList<NegaraModel>();
		String query = " from NegaraModel "
				     + " where kodeNegara like '%"+kodeNegara+"%' "
				     + " or namaNegara like '%"+namaNegara+"%' ";
		negaraModelList = session.createQuery(query).list();
		return negaraModelList;
	}

	@Override
	public Long countKode() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Long jumlahKode = 0L;
		String query = " select count(kodeNegara) from NegaraModel ";
		String kondisi = "";
		jumlahKode = (Long) session.createQuery(query + kondisi).uniqueResult();
		return jumlahKode;
	}


}
