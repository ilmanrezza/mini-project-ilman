package com.spring.cinema187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.cinema187.dao.CategoryDao;
import com.spring.cinema187.model.CategoryModel;
import com.spring.cinema187.service.CategoryService;
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	private CategoryDao categoryDao;

	@Override
	public void create(CategoryModel categoryModel) {
		// TODO Auto-generated method stub
		this.categoryDao.create(categoryModel);
	}

	@Override
	public List<CategoryModel> searchAll() {
		// TODO Auto-generated method stub
		return this.categoryDao.searchAll();
	}

	@Override
	public Long countKode() {
		// TODO Auto-generated method stub
		return this.categoryDao.countKode();
	}

	@Override
	public CategoryModel searchCode(String code) {
		// TODO Auto-generated method stub
		return this.categoryDao.searchCode(code);
	}
	
	@Override
	public void update(CategoryModel categoryModel) {
		// TODO Auto-generated method stub
		this.categoryDao.update(categoryModel);
	}

	@Override
	public List<CategoryModel> searchCodeOrName(String code, String name) {
		// TODO Auto-generated method stub
		return this.categoryDao.searchCodeOrName(code, name);
	}

	@Override
	public void deleteTemporary(CategoryModel categoryModel) {
		// TODO Auto-generated method stub
		this.categoryDao.update(categoryModel);
	}

	
	
	

}
