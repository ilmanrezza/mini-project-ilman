package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.TestimonyDao;
import com.spring.cinema187.model.BootcamptesModel;
import com.spring.cinema187.model.TestimonyModel;

@Repository
public class TestimonyDaoImpl implements TestimonyDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(TestimonyModel testimonyModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(testimonyModel);
	}

	@Override
	public void update(TestimonyModel testimonyModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(testimonyModel);
	}

	@Override
	public List<TestimonyModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TestimonyModel> testimonyModel = new ArrayList<TestimonyModel>();
		String query = "from TestimonyModel";
		String kondisi = " where isDelete = false ORDER BY id ";
		testimonyModel = session.createQuery(query+kondisi).list();
		return testimonyModel;

		
	}

	@Override
	public TestimonyModel searchId(Integer id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		TestimonyModel testimonyModel = new TestimonyModel();
		String query = "from TestimonyModel";
		String kondisi = " where id = '"+id+"' "; //query
		testimonyModel = (TestimonyModel) session.createQuery(query + kondisi).uniqueResult();
		return testimonyModel;
	}

	@Override
	public List<TestimonyModel> searchTitle(String title) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TestimonyModel> testimonyModel = new ArrayList<TestimonyModel>();
		String query = "from TestimonyModel";
		String kondisi = " where title like '%"+title+"%' and isDelete = false "; //query
		testimonyModel = session.createQuery(query+kondisi).list();
		return testimonyModel;
	}
	
}
