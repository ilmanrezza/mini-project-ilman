<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Edit Genre</h1>
	<form action="#" method="get" id="form-genre-edit">
		<table>
			<tr>
				<td>Kode Genre</td>
				<td><input type="text" id="kodeGenre" name="kodeGenre" value = "${genreModel.kodeGenre}"/></td>
			</tr>
			<tr>
				<td>Nama Genre</td>
				<td><input type="text" id="namaGenre"name="namaGenre" value = "${genreModel.namaGenre}"/></td>
			</tr>
		</table>
		<button type="submit" onclick="validasi();">Simpan</button> 
	</form>
</div>

<script type = "text/javascript">
	function validasi() {
		var kodeGenre = document.getElementById("kodeGenre");
		if (kodeGenre.value == "") {
			kodeGenre.style.borderColor = "red";
		} else {
			kodeGenre.style.borderColor = "gray";
		}
		
		var namaGenre = document.getElementById("namaGenre");
		if (namaGenre.value == "") {
			namaGenre.style.borderColor = "red";
		} else {
			namaGenre.style.borderColor = "gray";
		}
	}
</script>