<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">

	<form action="#" method="get" id="form-question-add" enctype="multipart/form-data">
		
		<div class="form-group">
		<div class="col-md-12">
			<div class="custom-select" style="width:300px;">
			<select name="question_type">
				<option>Choose Question Type :</option>
				<option value="MC">Multiple Choice</option>
				<option value="ES">Essay</option>
			</select>
			</div>
		</div>
		</div>
		
		<div class="form-group">
			<div class="col-md-12">
 		 	<label for="comment">Question:</label>
 		 	<textarea class="form-control" rows="5" id="comment" name="question" placeholder="question"></textarea>
 		 	</div>
		</div>

		 <div class="form-group">
      		<div class="col-md-12">
      		 <label for="upload">Upload Image</label>
     		 <input type="file" class="form-control" id="imageUrl"  name="imageUrl" accept="image/*">
     		 </div>
    	</div>
	
		<button type="reset" data-dismiss="modal" class="btn btn-warning">Cancel </button> 
		<button type="submit" class="btn btn-warning"  >Save</button>
	</form>
</div>


<script type="text/javascript">

 
 </script>
