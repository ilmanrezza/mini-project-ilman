package com.spring.cinema187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.cinema187.dao.CategoryDao;
import com.spring.cinema187.model.CategoryModel;
import com.spring.cinema187.model.NegaraModel;

@Repository
public class CategoryDaoImpl implements CategoryDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(CategoryModel categoryModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(categoryModel);
	}

	@Override
	public List<CategoryModel> searchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<CategoryModel> categoryModel = new ArrayList<CategoryModel>();
		String query = "from CategoryModel";
		String kondisi = " where isDelete = false ORDER BY name ";
		categoryModel = session.createQuery(query+kondisi).list();
		return categoryModel;
	}

	@Override
	public Long countKode() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Long jumlahKode = 0L;
		String query = " select count(code) from CategoryModel ";
		String kondisi = "";
		jumlahKode = (Long) session.createQuery(query + kondisi).uniqueResult();
		return jumlahKode;
	}

	@Override
	public void update(CategoryModel categoryModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(categoryModel);
	}

	@Override
	public CategoryModel searchCode(String code) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		CategoryModel categoryModel = new CategoryModel();
		String query = "from CategoryModel";
		String kondisi = " where CODE = '"+code+"' "; //query
		categoryModel = (CategoryModel) session.createQuery(query + kondisi).uniqueResult();
		return categoryModel;
	}

	@Override
	public List<CategoryModel> searchCodeOrName(String code, String name) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); 
		List<CategoryModel> categoryModel = new ArrayList<CategoryModel>();
		String query = " from CategoryModel "
				     + " where (code like '%"+code+"%' "
				     + " or name like '%"+name+"%' ) and  isDelete = false ";
		categoryModel = session.createQuery(query).list();
		return categoryModel;
	}



	
	
}
