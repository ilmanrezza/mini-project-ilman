<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${anggotaModelList}" var="anggotaModel" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${anggotaModel.idAnggota}</td>
		<td>${anggotaModel.kodeAnggota}</td>
		<td>${anggotaModel.namaAnggota}</td>
		<td>${anggotaModel.usia}</td>
				
		<td>
			<button type="button" class="btn btn-warning" value="${anggotaModel.idAnggota}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${anggotaModel.idAnggota}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${anggotaModel.idAnggota}" id="btn-delete">Hapus</button>
		</td>
	</tr>
</c:forEach>